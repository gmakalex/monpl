RouterController.loadHandler(true);

var BonusController = {
    init: function () {

        HintsController.extraDays(true);

        this.activeVideoIndex = 0;
        this.eventListener();

        $(this.selectors.phone.phone).val(CommonController.user.phone);
        mask(this.selectors.phone.phone);
        /*$(this.selectors.phone.phone).inputmask({
            mask: "+99999999999",
            definitions: {
                'X': {
                    validator: "[6-9]",
                }
            },
            oncomplete: this.onPhoneComplete.bind(this),
            onincomplete: this.onPhoneIncomplete.bind(this),
        }).val(CommonController.user.phone);*/

        $(this.selectors.phone.code).inputmask({
            mask: "99999",
            oncomplete: this.onCodeComplete.bind(this),
            onincomplete: this.onCodeIncomplete.bind(this),
        });

        if (!CommonController.user.is_phone_verified) {

            $(this.selectors.phone.container).show();
            this.onPhoneComplete();
        }

        if (!CommonController.user.is_questioned) {

            $(this.selectors.questions.container).show();
        }

        if (HintsController.isVideosDone) {
            const headers = $(this.selectors.videos.headers);
            headers.eq(0).hide();
            headers.eq(1).show();
        }
    },
    answers: {
        q0: false,
        q1: false,
        q2: false,
        q3: false,
        q4: false,
    },
    selectors: {
        phone: {
            container: '#phone-bonus',
            headers: '#phone-bonus h2',
            phone: '#phone',
            sendBtn: '#phone-bonus .btn:first',
            code: '#code',
            confirmBtn: '#phone-bonus .btn:last',
        },
        questions: {
            container: '#question-bonus',
            headers: '#question-bonus h2',
            texts: '#question-bonus .row:eq(1) .col',
            nextBtns: '#question-bonus .row:eq(2) .col .btn',
            radios: '#question-bonus input[type="radio"]',
            radiosGroup: function (question) {
                return `#question-bonus .col:eq(${question}) input[type="radio"]`;
            },
            radioChecked: function (question) {
                return `#question-bonus input[name='q${question}']:checked`;
            }
        },
        videos: {
            container: '#get-extra-days',
            headers: '#get-extra-days h2',
            videos: '#get-extra-days .extra-video',
        },
    },


    onPhoneComplete: function() {

        if (!CommonController.user.is_phone_verified) {


        }

        $(this.selectors.phone.sendBtn).removeClass('disabled');
    },

    onPhoneIncomplete: function () {
        $(this.selectors.phone.sendBtn).addClass('disabled');
    },

    onCodeComplete: function () {

        $(this.selectors.phone.confirmBtn).removeClass('disabled');
    },

    onCodeIncomplete: function () {
        $(this.selectors.phone.confirmBtn).addClass('disabled');
    },

    onSendCode: function (e) {

        e.preventDefault();

        const phone = $(this.selectors.phone.phone).val();

        if (phone) {

            $(this.selectors.phone.phone).addClass('disabled');
            $(this.selectors.phone.sendBtn).addClass('disabled');

            settings.ajax({
                method: 'POST',
                url: `/v1/sms`,
                data: {
                    id: localStorage.user_id,
                    phone: phone,
                },
                callback: function(result) {

                    if (result) {

                        $(BonusController.selectors.phone.code).css('visibility', 'visible');
                        $(BonusController.selectors.phone.confirmBtn).css('visibility', 'visible');

                        settings.responseHandler(true, result.message);

                        CommonController.user.phone = phone;
                    }

                },
                error: function(msg) {

                    settings.responseHandler(false, msg);
                },
                headers: function(xhr) {


                }
            }, true);
        }
    },

    onConfirmCode: function (e) {

        e.preventDefault();

        const code = $(this.selectors.phone.code).val();

        settings.ajax({
            method: 'PATCH',
            url: `/v1/sms/${localStorage.user_id}`,
            data: {
                code: code,
            },
            callback: function(result) {

                if (result) {

                    CommonController.user.is_phone_verified = true;

                    $(BonusController.selectors.phone.code).css('visibility', 'visible');
                    $(BonusController.selectors.phone.confirmBtn).css('visibility', 'visible');

                    settings.responseHandler(true, result.message);

                    setTimeout(() => {

                        $(BonusController.selectors.phone.container).hide();
                    }, 2000);

                    try {

                        gtag('event', 'send_form', {'event_category': 'registr', 'event_action': 'confirm_number'}); ym(69943126, 'reachGoal', 'registr_confirm_number');  fbq('trackCustom', 'registr_confirm_number');

                        Comagic.addOfflineRequest({
                            name: CommonController.user.name,
                            email: CommonController.user.email,
                            phone: CommonController.user.phone
                        });

                        VK.Goal('complete_registration');

                        settings.ajax({
                            method: 'GET',
                            url: `/v1/system-amo/${localStorage.user_id}`,
                            data: {},
                            callback: function(result) {

                            },
                            error: function(msg) {

                            },
                            headers: function(xhr) {

                            }
                        }, true);
                    }
                    catch (e){}
                }

            },
            error: function(msg) {

                settings.responseHandler(false, msg);
            },
            headers: function(xhr) {

            }
        }, true);
    },


    onQuestionResponseChange: function (e) {
        const id = $(e.currentTarget).attr('id');
        const index = parseInt(id[1]);

        $(this.selectors.questions.nextBtns)
            .eq(index).removeClass('disabled');
    },
    onQuestionNextClick: function (e) {

        e.preventDefault();

        var index = parseInt(e.currentTarget.dataset['index']);
        const value = $(this.selectors.questions.radioChecked(index)).val();
        const label = $(this.selectors.questions.radioChecked(index)).parent().find('label').text();

        $(this.selectors.questions.nextBtns)
            .eq(index).addClass('disabled');

        this.answers[$(this.selectors.questions.radioChecked(index)).attr('name')] = label;

        if (index == 0) {

            if (value == 'no') {

                this.sendAnswers();

                return true;
            }
            else if (value == 'plan') {

                $('#question-bonus .question').eq(1).find('h3').text('Когда вы планируете выйти на Маркетплэйс?');

                $('#question-bonus .question').eq(1).find('label').eq(0).text('В рамках месяца');
                $('#question-bonus .question').eq(1).find('label').eq(1).text('В рамках двух месяцев');
                $('#question-bonus .question').eq(1).find('label').eq(2).text('Не знаю');

                $('#question-bonus .question').eq(2).find('h3').text('Какой бюджет у вас запланирован на закупку товара?');

                $('#question-bonus .question').eq(2).find('label').eq(0).text('Менее 100 000 рублей');
                $('#question-bonus .question').eq(2).find('label').eq(1).text('Более 100 000 рублей');
                $('#question-bonus .question').eq(2).find('label').eq(2).text('Более 300 000 рублей');
            }
        }

        if (index == 0) {

            gtag('event', 'oprosnik', {'event_category': 'question', 'event_label': 'q1'}); ym(69943126, 'reachGoal', 'oprosnik_question_q1');
        }
        else if (index == 1) {

            gtag('event', 'oprosnik', {'event_category': 'question', 'event_label': 'q2'}); ym(69943126, 'reachGoal', 'oprosnik_question_q2');
        }

        $(this.selectors.questions.radiosGroup(index))
            .attr('disabled', 'true');

        if (index < 2) {
            $(this.selectors.questions.texts)
                .eq(index + 1).css('visibility', 'visible');
            $(this.selectors.questions.nextBtns)
                .eq(index + 1).css('visibility', 'visible');
        }

        if (index === 2) {

            gtag('event', 'oprosnik', {'event_category': 'question', 'event_label': 'q3'}); ym(69943126, 'reachGoal', 'oprosnik_question_q3');

            this.sendAnswers();
        }
    },
    sendAnswers: function() {

        settings.ajax({
            method: 'PATCH',
            url: `/v1/user/${localStorage.user_id}`,
            data: {
                questions: BonusController.answers,
            },
            callback: function(result) {

                if (result) {

                    CommonController.user.is_questioned = true;

                    settings.responseHandler(true, 'Спасибо за ответы. Вы получили +1 день premium доступа');

                    gtag('event', 'oprosnik', {'event_category': 'question', 'event_label': 'bonus'}); ym(69943126, 'reachGoal', 'oprosnik_question_bonus');

                    setTimeout(() => {

                        $(BonusController.selectors.questions.container).hide();
                    }, 2000);
                }

            },
            error: function(msg) {

                settings.responseHandler(false, msg);
            },
            headers: function(xhr) {

            }
        }, true);
    },
    getData: function () {
    },

    eventListener: function () {
        $(this.selectors.phone.sendBtn).on('click', this.onSendCode.bind(this));
        $(this.selectors.phone.confirmBtn).on('click', this.onConfirmCode.bind(this));

        const onQuestionResponseChange = this.onQuestionResponseChange.bind(this);
        $(this.selectors.questions.radios).each(function () {
            $(this).on('change', onQuestionResponseChange);
        });

        const onQuestionNextClick = this.onQuestionNextClick.bind(this);
        $(this.selectors.questions.nextBtns).each(function () {
            $(this).on('click', onQuestionNextClick);
        });
    },
}

$(function () {
    BonusController.init();
});