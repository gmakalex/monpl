<html><head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=PROJECT_NAME?> | Регистрация </title>

    <link href="/css/libs/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/css/libs/font-awesome/font-awesome.css" rel="stylesheet">
    <link href="/js/libs/toastr/toastr.min.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">
    <link href="/css/libs/animate/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/mobile.css?id=1" rel="stylesheet">


<body class="gray-bg" data-new-gr-c-s-check-loaded="14.1050.0" data-gr-ext-installed="">

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name"><?=PROJECT_NAME?></h1>

        </div>
        <h3>Регистрация. Попробовать бесплатно на 7 дней</h3>
        <form class="m-t" role="form" action="login.html">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Имя" name="name" required="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="email" required="">
                <p style="margin-bottom: -8px;font-size: 12px;">Пароль мы вышлем на электронную почту</p>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Повторите E-mail" name="email_repeat" required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Телефон" id="phone" name="phone" required="">
                <p style="margin-bottom: 0px;font-size: 12px;">Введите номер телефона. Для завершения регистрации будет <strong>необходимо</strong> ввести последние 4 цифры входящего звонка</p>
            </div>
            <div class="form-group">
                <div class="checkbox i-checks"><label> <div class="icheckbox_square-green checked" style="position: relative;"><input checked="" type="checkbox" style="position: absolute; opacity: 0;">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div><i></i>
                    Согласие с <a href="<?=PROJECT_URL?>/politika-konfidencialnosti/" target="_blank">пользовательским соглашением</a></label></div>
            </div>
            <input type="hidden" name="id_ref" id="partner" value="123588">
            <input type="hidden" name="tid" id="tid">
            <input type="hidden" name="subid1" id="subid1">
            <input type="hidden" name="utm_s" id="utm_s">
            <input type="hidden" name="id_yandex" value="13">
            <input type="hidden" name="id_fingerprint" value="BKfodi5kwyCZRCFfpcUb">
            <input type="hidden" name="id_localstorage" value="">
            <input type="hidden" name="type" value="registration">
            <input type="hidden" name="browserName" value="Chrome">
            <input type="hidden" name="ip" value="81.198.152.120">
            <input type="hidden" name="browserVersion" value="98.0.4758">
            <input type="hidden" name="os" value="Windows">
            <input type="hidden" name="mcheck" value="test">
            <input type="hidden" name="platform" value="Other">
            <input type="hidden" name="osVersion" value="10">
            <button type="submit" class="btn btn-primary block full-width m-b">Регистрация</button>

            <p class="text-muted text-center"><small>Уже есть аккаунт?</small></p>
            <a class="btn btn-sm btn-white btn-block" onclick="fbq('track', 'Lead')" href="/">Вход</a>
        </form>
    </div>
</div>
<!--
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-977719-7Ssal"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-977719-7Ssal" style="position:fixed; left:-999px;" alt=""/></noscript>

<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '245291500264702');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=245291500264702&ev=PageView&noscript=1"
/></noscript>


<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-1002893-hs8Hj"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-1002893-hs8Hj" style="position:fixed; left:-999px;" alt=""/></noscript>


<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-163314472-10"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-163314472-10');
    gtag('config', 'G-RVC65QY67H');
</script>



<script type="text/javascript">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(69943126, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/69943126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


  -->

<div class="modal inmodal" data-keyboard="false" data-backdrop="static" id="modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Подтверждение телефона</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Введите последние 4 цифры входящего звонка для завершения регистрации, либо код, который продиктует робот.
                        Если звонок не поступил - обратитесь в чат поддержки!</label>
                    <input type="text" class="form-control" value="" id="code" name="code" placeholder="код">
                    <input type="hidden" name="id_user" id="id_user">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-data">Применить</button>
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<!--<script src="//cdn.jsdelivr.net/npm/@fingerprintjs/fingerprintjs@2/dist/fingerprint2.min.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/bowser@2.5.3/es5.min.js"></script>
<script src="/js/libs/jquery/jquery-3.1.1.min.js"></script>
<script src="/js/libs/popper/popper.min.js"></script>
<script src="/js/libs/bootstrap/bootstrap.js"></script>
<script src="/js/libs/inputmask/inputmask.js"></script>
<script src="/js/libs/inputmask/phones.js?id=2"></script>
<!-- iCheck -->
<script src="/js/libs/iCheck/icheck.min.js"></script>
<script src="/js/libs/toastr/toastr.min.js"></script>
<script src="/js/common.js?id=5"></script>
<script>
    $(function() {

        if (settings.allGetParams().utm_s) {

            settings.setCookie('utm_s', settings.allGetParams().utm_s, 30, true);
        }

        /*Inputmask({
            mask: "+99999999999",
            definitions: {
                'X': {
                    validator: "[6-9]",
                }
            }
        }).mask('#phone');*/

        mask('#phone');

        if (settings.getCookie('id_ref')) {

            $('#partner').val(settings.getCookie('id_ref'));
        }

        if (settings.getCookie('tid')) {

            $('#tid').val(settings.getCookie('tid'));
        }

        if (settings.getCookie('subid1')) {

            $('#subid1').val(settings.getCookie('subid1'));
        }

        if (settings.getCookie('utm_s')) {

            $('#utm_s').val(settings.getCookie('utm_s'));
        }

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('form').on('submit', function(e) {

            e.preventDefault();

            if ($('input[name="email"]').val() != $('input[name="email_repeat"]').val()) {

                settings.responseHandler(false, 'Email не совпдают!');

                return  false;
            }

            $.ajax({
                type: 'post',
                url: 'https://auth.moneyplace.io---/api-client-v1/users',
                data: $(this).serialize(),
                dataType: 'json',
                success: function (data) {

                    $('#id_user').val(data.id);

                    $('#modal').modal('show');

                    var user = data;

                    try {
                        gtag('event', 'send_form', {'event_category': 'registr'}); ym(69943126, 'reachGoal', 'registr_send_form');

                    }
                    catch (e){}

                    $('#save-data').click(function() {

                        if (!$('#code').val()) {

                            settings.responseHandler(false, 'Поле кода обязательно для заполнения');

                            return false;
                        }

                        $.ajax({
                            type: 'post',
                            url: 'https://auth.moneyplace.io---/api-client-v1/users/sms',
                            data: {
                                id_user: $('#id_user').val(),
                                code: $('#code').val()
                            },
                            dataType: 'json',
                            success: function (data) {

                                if (data) {

                                    settings.responseHandler(true, 'Вы успешно зарегистрировались. Пароль выслан на указанную почту.');

                                    try {
                                        gtag('event', 'send_form', {'event_category': 'registr', 'event_action': 'confirm_number'}); ym(69943126, 'reachGoal', 'registr_confirm_number');  fbq('trackCustom', 'registr_confirm_number');

                                        if (user.status == 'active') {

                                            Comagic.addOfflineRequest({
                                                name: user.name,
                                                email: user.email,
                                                phone: user.phone
                                            });
                                        }

                                        VK.Goal('complete_registration');
                                    }
                                    catch (e){}

                                    setTimeout(function() {

                                        location.replace('/thankyou.html');
                                    }, 2000);
                                }
                                else {

                                    settings.responseHandler(false, 'Неверный код!');
                                }
                            },
                            error: function(error) {

                                settings.responseHandler(false, 'Неверный код!');
                            }
                        });
                    });

                    fbq('track', 'Lead');
                },
                error: function(error) {

                    settings.responseHandler(false, error.responseJSON[0].message);
                }
            });
        });

        var data = settings.getTrackData();

        data.type = 'registration';
        data.mcheck = 'test';

        setTimeout(function() {

            for (var i in data) {

                $(`input[name="${i}"]`).val(data[i]);
            }

        }, 1000);

        setTimeout(function () {

            FingerprintJS.load({token: 'Xim59tBea1X3c902IRGc'})
                .then(fp => fp.get({ extendedResult: true }))
                .then(function(result) {

                    console.log(result);

                    $('input[name="id_fingerprint"]').val(result.visitorId);
                    $('input[name="browserName"]').val(result.browserName);
                    $('input[name="browserVersion"]').val(result.browserVersion);
                    $('input[name="os"]').val(result.os);
                    $('input[name="osVersion"]').val(result.osVersion);
                    $('input[name="ip"]').val(result.ip);
                    $('input[name="platform"]').val(result.device);
                    $('input[name="id_yandex"]').val(13);
                });
        }, 1500);
    });


</script>


</div>

</body>
</html>