<?php

class USERData
{
    private $db;

//Constructor
    function __construct($DBcon)
    {
        $this->db = $DBcon;
    }

//Login 
    function login($name, $pass, $table)
    {
        try {

            $stmt = $this->db->prepare("SELECT * from ".DB_TABLE_PREFIX.$table." WHERE name=:uname");
            $stmt->execute(array(':uname' => $name));
            $data_row = $stmt->fetch(PDO::FETCH_ASSOC);


            if ($stmt->rowCount() > 0) {
                if (password_verify($pass, $data_row['password'])) {

                    return $data_row['id'];
                }

            }
        } catch (PDOException $e) {
            echo '<pre>';
            print_r(array($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()));
            echo '</pre>';
        }
    }

//Login
    function loginById($name, $id, $table)
    {
        try {

            $stmt = $this->db->prepare("SELECT * from ".DB_TABLE_PREFIX.$table." WHERE id=:id");
            $stmt->execute(array(':id' => $id));
            $data_row = $stmt->fetch(PDO::FETCH_ASSOC);

            if (($data_row > 0)&&($name === $data_row['name'])) {
                    return $data_row;
            }
        } catch (PDOException $e) {
            echo '<pre>';
            print_r(array($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()));
            echo '</pre>';
        }
    }

    //Restore
    function restore($email, $table)
    {
            try {

            $stmt = $this->db->prepare("SELECT * from ".DB_TABLE_PREFIX.$table." WHERE mail=:mail");
            $stmt->execute(array(':mail' => trim($email)));
            $data_row = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {

                    return $data_row['id'];

            }
        } catch (PDOException $e) {
                echo '<pre>';
                print_r(array($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()));
                echo '</pre>';
            }
        }

//Update password
    function passwordUpdate($email, $table, $password)
    {
        try {

            $stmt = $this->db->prepare("SELECT * from ".DB_TABLE_PREFIX.$table." WHERE mail=:mail");
            $stmt->execute(array(':mail' => $email));
            $data_row = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                $query = "UPDATE " . DB_TABLE_PREFIX .$table . " SET password = '" . $password . "' where mail = '" . $email."'";
                $this->db->query($query);
            }
        } catch (PDOException $e) {
            echo '<pre>';
            print_r(array($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()));
            echo '</pre>';
        }
    }		
	
 

//Signup
    function signup($name, $password, $ip, $mobile, $email, $table)
    {
        try {
			
            $new_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $stmt = $this->db->prepare("INSERT into ".DB_TABLE_PREFIX.$table." (name,password,ip,language,mail,last_login,mobile,status) 
            VALUES(:name, :password, :ip, :language, :mail, :last_login, :mobile, :status)");

            if ($stmt->execute(array(':name' => $name, ':password' => $new_password, ':ip' => $ip, ':language' => 1, ':mail' => $email, ':last_login' => time(), ':status' => 0, ':mobile' => $mobile))) {
                return $stmt;
            }

        } catch (PDOException $e) {
            echo '<pre>';
            print_r(array($e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode()));
            echo '</pre>';
        }

    }

}
