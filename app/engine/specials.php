<?php
$UtfIcon['rabbit'] = '🐇';

//Active urls
$aLink['bonus'] = $aLink['dashboard'] = $aLink['products'] = $aLink['categories'] = $aLink['sellers'] = $aLink['brands'] = $aLink['keywords'] = $aLink['analytic'] = $aLink['lists']  = $aLink['faq'] = $aLink['chrome'] = '';
$aClass = 'class="active"';


if (str_contains($requestUri, "?")){
    list($requestUri) = explode("?", $requestUri);
}


if (str_contains($requestUri, "client/")){
list($none, $nameUrl) = explode("client/", $requestUri);
}elseif (str_contains($requestUri, "register")){
    $nameUrl = 'register';
}elseif (str_contains($requestUri, "login")){
    $nameUrl = 'login';
}elseif (str_contains($requestUri, "reset")){
    $nameUrl = 'reset';
}else{
    $nameUrl = 'index';
}


$nameUrlBack = $nameUrl;


if (str_contains($requestUri, "client/".$nameUrl)){
    switch (trim($nameUrl)) {
        case 'seller':
        case 'brand':
        case 'keyword':
        case 'product':
            $nameUrl = $nameUrl.'s';
            break;
        case 'category':
            $nameUrl = 'categories';
            break;
    }
    $aLink[''.$nameUrl.''] = $aClass;
}



$aLinkTitle['login']    =  'Логин';
$aLinkTitle['register'] =  'Регистрация';
$aLinkTitle['reset']    =  'Востанавления пароля';
$aLinkTitle['index']    =  'Главная';


$aLinkTitle['bonus']      =  'Бонусы';
$aLinkTitle['dashboard']  =  'Главная';
$aLinkTitle['products']   =  'Товары';
$aLinkTitle['categories'] =  'Категории';
$aLinkTitle['sellers']    =  'Продавцы';
$aLinkTitle['brands']     =  'Бренды';
$aLinkTitle['keywords']   =  'Ключевики';
$aLinkTitle['analytic']   =  'Анализ ниш';
$aLinkTitle['lists']      =  'Отслеживание';
$aLinkTitle['faq']        =  'Обучение';
$aLinkTitle['chrome']     =  'Плагин для Chrome';
$aLinkTitle['404']        =  '404 Ошибка!';


$aLinkTitle['seller']    =  'Продавец';
$aLinkTitle['brand']     =  'Бренд';
$aLinkTitle['keyword']   =  'Ключевик';
$aLinkTitle['product']   =  'Товар';
$aLinkTitle['category']  =  'Категория';

$aLinkTitle['rates']     =  'Тарифы';

if(!empty($aLinkTitle[$nameUrlBack])){
    if (str_contains($requestUri, "?")){
        $nameTitle = 'Детализазия';
    }else{
        $nameTitle = $aLinkTitle[$nameUrlBack];
    }
}else{
    if (str_contains($requestUri, "?")){
        $nameTitle = 'Детализазия';
    }
}

$nameUrl = $nameUrlBack;



///////////////////////////////////////////////////////////////////
/////////    //********* LOCAL API ***********//    ///////////////
///////////////////////////////////////////////////////////////////
$linksGENERATOR = array(
    'v1/track',
    'v1/sms',
    'v1/user',
    'v1/system-amo',
    'v1/user-schedule',
    'api-client-v1/users/reset-password',
    'api-client-v1/users/sms',
    'api-client-v1/users',
    'auth');
if (!empty($_SERVER['REQUEST_URI'])) {
    foreach($linksGENERATOR as $linkData):
        if (strpos($_SERVER['REQUEST_URI'], $linkData) !== false) {
            $apiActivatedSwitches = 1;
            include $thisDir . '/app/local_api/index.php';
            die();
        }
    endforeach;
}

///////////////////////////////////////////////////////////////////
/////////    //********* JS SCRIPTS ***********//    //////////////
///////////////////////////////////////////////////////////////////
$linksJS = array(
    'VideoController_id_36',
    'SearchController_id_26',
    'RouterController_id_7',
    'ProductTableController_id_5',
    'PermissionController_id_23',
    'MyListController_id_8',
    'HintsController_id_45',
    'CommonController_id_55',
    'CustomVideoController_id_7',
    'ChartsController_id_12',
    'AnalyticsController_id_5',

    'pagination_id_3',
    'positions_id_22',
    'serializer_id_5',

    'analytic',
    'bonus',
    'brand',
    'brands',
    'categories',
    'category',
    'dashboard',
    'faq',
    'keyword',
    'keywords',
    'lists',
    'product',
    'products',
    'rates',
    'seller',
    'sellers',

    'common',
    'login',
    'register'

    );
if (!empty($_SERVER['REQUEST_URI'])) {
    $tagDIE = false;
    foreach($linksJS as $linkDataJS):
        if (strpos($_SERVER['REQUEST_URI'], 'n/'.$linkDataJS.'.js') !== false) {
            $javascriptActivatedSwitches = 1;
            $tagDIE = true;
            include $thisDir . '/app/local_javascript/index.php';
        }elseif (strpos($_SERVER['REQUEST_URI'], 's/'.$linkDataJS.'.js') !== false) {
            $javascriptActivatedSwitches = 1;
            $tagDIE = true;
            include $thisDir . '/app/local_javascript/index.php';
        }
    endforeach;
        if($tagDIE){
            die();
        }
}