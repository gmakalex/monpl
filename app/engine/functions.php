<?php


if(empty($ajaxPosition)) {
include($thisDir . "/engine/vendor/autoload.php");

if (empty($noHacks)) {
    die('NO ACCESS');
}

if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {

    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    switch ($lang) {
        case "ru":
            include($thisDir . "/arrays/languages/ru_lang.php");
            $languagefor = 'ru';
            break;
        case "en":
            include($thisDir . "/arrays/languages/en_lang.php");
            $languagefor = 'en';
            break;
        default:
            include($thisDir . "/arrays/languages/en_lang.php");
            $languagefor = 'en';
            break;
    }
} else {
    include($cpath . "/arrays/languages/en_lang.php");
    $languagefor = 'en';
}

}


if (!function_exists('str_contains')) {
    function str_contains(string $haystack, string $needle): bool
    {
        return '' === $needle || false !== strpos($haystack, $needle);
    }
}


if (!function_exists('activeUrls')) {
    function activeUrls($thisUrl, $status, $requestUri)
    {
        $statusDiv = '';
        if (str_contains($requestUri, $thisUrl)) {
            $statusDiv = $status;
        }
        return $statusDiv;
    }
}

if (!function_exists('isCookie')) {
function isCookie($CookieName)
{
    $check = false;
    if (isset($_COOKIE[$CookieName])) {
        $check = true;
    }
    return $check;
}
}

if (!function_exists('getRefer')) {
function getRefer()
{
    $check = false;

    if (isset($_SERVER['HTTP_REFERER'])) {
        $check = security($_SERVER['HTTP_REFERER'], "u");
    }

    if (!$check) {
        $check = security($_SERVER['REQUEST_URI'], "u");
    }

    return $check;
}
}

if (!function_exists('widget_redirect')) {
//redirect(TELEGRAM_URL, '301');
function widget_redirect($url, $redirectCode)
{
    header("Location: " . $url, true, $redirectCode);
    exit();
}
}


if (!function_exists('sessionFunction')) {
function sessionFunction($name, $value, $days)
{
    setcookie($name, $value, strtotime('+' . $days . ' days'), "/");
}
}

function qy($query){
    return  $array = tryPDO(false,false,false,false,false,false,$query);
}


if (!function_exists('pre')) {
    function pre($var)
    {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }
}

if (!function_exists('injectionsArray')) {
    function injectionsArray()
    {
        return array("collate", "group by", ".shell", "xp_regread", "xp_cmdshell",
            " delay", " +", "+ ", " +", "/", " declare", "drop ", " --",
            " union ", " union all ", "%", " like", " where ", "insert ",
            "select ", "update ", " update", " and 1 ", " 1=1 ", " 1=2 ", " 2=2 ", " or ");
    }
}

if (!function_exists('security')) {
    function security($string, $t = false)
    {
        if ($t === "d") {
            $string = str_replace(array("https://", "http://"), "", $string);
        }
        $string = str_replace("\'", "", $string);
        if ($t !== "m") {
            $string = htmlspecialchars($string, ENT_QUOTES);
        }
        $string = str_replace("--", "", $string);
        if ($t !== "u") {
            $urlEnable = array("/", "=");
            $string = str_replace($urlEnable, "", $string);
        }

        $injects = injectionsArray();

        return str_replace($injects, "", $string);
    }
}

if (!function_exists('requestReport')) {
    function requestReport($getArray)
    {
        $dataArray = [];

        foreach ($getArray as $get => $getValue) {

            if (!empty($_GET[$get])) {
                $thisRequest = $_GET[$get];
            } elseif (!empty($_POST[$get])) {
                $thisRequest = $_POST[$get];
            }

            if (!empty($thisRequest)) {
                $dataArray[$get] = security($thisRequest);

                $injects = injectionsArray();

                foreach ($injects as $injection) {
                    if (str_contains(strtolower(trim($thisRequest)), $injection)) {
                        $dataArray['INJECTION'] = "Security alert! Injection detected! " . $thisRequest;
                        //die($dataArray['INJECTION']);
                    }
                }
            }
        }

        return $dataArray;
    }
}


function uniqueHash($length = 10)
{
    return substr(md5(uniqid('', true) . microtime()), 0, $length);
}


function uniqueHashRandomLetters($length = 10)
{
    $str = substr(md5(uniqid('', true) . microtime()), 0, $length);

    $str = str_split(strtolower($str));
    foreach ($str as &$char) {
        if (mt_rand(0, 1)) {
            $char = strtoupper($char);
        }
    }
    return implode('', $str);
}


function sentToMail($toThisMail, $sentInformation)
{

    $error = false;

    $content = $sentInformation;

    $mail = new PHPMailer;

    $hostName = $_SERVER['HTTP_HOST'];

    $subject = "Report [" . $hostName . "]";

    $from = "report@" . $hostName;

    if (MAIL_SMTP) {
        $mail->IsSMTP();
        $mail->Host = MAIL_SMTP_HOST;
        $mail->Port = MAIL_SMTP_PORT;
        if (MAIL_SMTP_USER !== "" && MAIL_SMTP_PASSWORD !== "") {
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_SMTP_USER;
            $mail->Password = MAIL_SMTP_PASSWORD;
        }

        if (MAIL_SMTP_SECURE) {
            $mail->SMTPSecure = MAIL_SMTP_SECURE;
        }
        $from = MAIL_SMTP_USER;
    }

    $mail->setFrom($from, $from);
    $mail->Sender = $from;
    $mail->addAddress($toThisMail);

    if (defined('MAIL_REPORT_DEV')) {
        $mail->addAddress(MAIL_REPORT_DEV);
    }

    $mail->addAddress($toThisMail);
    $mail->CharSet = "utf-8";
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $content;

    if ($mail->send()) {
        $error = true;
    }
    return $error;
}


function tryPDO($action = false, $table = false, $where = false, $what = false, $key = false, $arrays = false, $q = false)
{

    $thisArray = [];
    $errors = false;
    $query = 'null';

    try {
        // first connect to database with the PDO object.
        $db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASSWORD, [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);

        switch ($action) {
            case "select":
                $query = "SELECT * FROM " . DB_TABLE_PREFIX . $table . " WHERE " . $where;
                $stmt = $db->query($query);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt = null;
                break;
            case "delete":
                $query = "DELETE FROM " . DB_TABLE_PREFIX . $table . " WHERE " . $where;
                $stmt = $db->query($query);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt = null;
                break;
            case "like":
                $query = "SELECT * FROM " . DB_TABLE_PREFIX . $table . " like " . $where;
                $stmt = $db->query($query);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt = null;
                break;
            case "insert":
                $query = "INSERT INTO " . DB_TABLE_PREFIX . $table . " SET " . $what;
                $thisArray = $db->query($query);
                break;
            case "update":
                $query = "UPDATE " . DB_TABLE_PREFIX . $table . " SET " . $what . " where " . $where;
                $thisArray = $db->query($query);
                break;
            case "insert duplicate key":
                $query = "INSERT INTO " . DB_TABLE_PREFIX . $table . " (" . $arrays['what_values'] . ") VALUES (" . $arrays['values'] . ")
ON DUPLICATE KEY UPDATE " . $what;
                $thisArray = $db->query($query);
                break;
            case "select key":
                $query = "SELECT " . $key . " FROM " . DB_TABLE_PREFIX . $table . " WHERE " . $where . " LIMIT 1";
                $stmt = $db->query($query);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($thisArray as $dataPdo => $th) {
                    $thisArray['key'] = $th[$key];
                }

                $stmt = null;
                break;
            case "count":
                $query = "SELECT " . $key . " FROM " . DB_TABLE_PREFIX . $table . " WHERE " . $where . " LIMIT 1";
                $stmt = $db->query($query);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $stmt = null;
                break;
            default:
                $query = $q;
                // default select - for unions, join and more
                $stmt = $db->query($q);
                $thisArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt = null;
        }

        $db = null;

        //echo " $query   || "  ;

    } catch (PDOException $e) {
        //echo "Error connecting to mysqli: || $query || " . $e->getMessage();
        trigger_error("Error connecting to mysqli: || $query || " . $e->getMessage(), E_USER_WARNING);
        $errors = true;
    }

    return $thisArray;
}


function connectPDO()
{
    $db = false;

    try {
        $db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASSWORD, [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);

    } catch (PDOException $e) {
        echo "ERROR : " . $e->getMessage();
    }

    return $db;
}


function validatorRegex($action, $dataArray)
{

    $thisError = false;

    switch ($action) {
        case "nls":
            if (preg_match("/^[A-Za-z0-9_-]+$/", $dataArray)) {
                $thisError = true;
            }
            break;
        case "ln":
            if (preg_match("/^[A-Za-z0-9]+$/", $dataArray)) {
                $thisError = true;
            }
            break;
        case "n":
            if (preg_match("/^[0-9]+$/", $dataArray)) {
                $thisError = true;
            }
            break;

        case "l":
            if (preg_match("/^[A-Za-z]+$/", $dataArray)) {
                $thisError = true;
            }
            break;
        case "s":
            if (preg_match("/^[_-]+$/", $dataArray)) {
                $thisError = true;
            }
            break;

        default:

    }

    return $thisError;
}



function getRealClientIp()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } elseif (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } elseif (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
    } elseif (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}


function jsWidgetGeneratedUrlCode($clientId, $widgetDomainId, $widgetServiceId)
{
    return '<script src="' . PROJECT_URL . '/widgets/' . $clientId . '/' . $widgetServiceId . '/' . $widgetDomainId . '/widget"></script><div id="' . md5($widgetServiceId) . '"></div>';
}


function createWidgetsFolder($widgetServiceId)
{
    $dir = str_replace("/engine", "", __DIR__);

    if (!file_exists($dir . '/widgets_base/' . $widgetServiceId)) {

        if (@!mkdir($concurrentDirectory = $dir . '/widgets_base/', 0777, true)
            && @!is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        if (@!mkdir($concurrentDirectory = $dir . '/widgets_base/' . $widgetServiceId, 0777, true)
            && @!is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

    }
}


function get32($widgetId)
{
    return abs(crc32($widgetId));
}


function words($inputString)
{
    global $translate;

    $check = $inputString;

    foreach ($translate as $tag => $word) {
        if ($tag === $inputString) {
            $check = $word;
        }
    }
    return $check;
}

/**
 * Decrypt data from a CryptoJS json encoding string
 *
 * @param mixed $passphrase
 * @param mixed $jsonString
 * @return mixed
 */
function cryptoJsAesDecrypt($passphrase, $jsonString)
{
    $jsondata = json_decode($jsonString, true);
    $salt = hex2bin($jsondata["s"]);
    $ct = base64_decode($jsondata["ct"]);
    $iv = hex2bin($jsondata["iv"]);
    $concatedPassphrase = $passphrase . $salt;
    $md5 = array();
    $md5[0] = md5($concatedPassphrase, true);
    $result = $md5[0];
    for ($i = 1; $i < 3; $i++) {
        $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
        $result .= $md5[$i];
    }
    $key = substr($result, 0, 32);
    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
    return json_decode($data, true);
}

/**
 * Encrypt value to a cryptojs compatiable json encoding string
 *
 * @param mixed $passphrase
 * @param mixed $value
 * @return string
 */
function cryptoJsAesEncrypt($passphrase, $value)
{
    $salt = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx = '';
    while (strlen($salted) < 48) {
        $dx = md5($dx . $passphrase . $salt, true);
        $salted .= $dx;
    }
    $key = substr($salted, 0, 32);
    $iv = substr($salted, 32, 16);
    $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    return json_encode($data);
}

function clearClassId($r)
{
    return str_replace(array(".", "#"), "", $r);
}


function javaScriptCrypts($array, $valuesArray)
{
    //var decrypted = JSON.parse(CryptoJS.AES.decrypt(encrypted, key, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));

    $style = array();
    $crypt = array();
    $url = array();

    $cnt = count($valuesArray);
    $i = 0;

    foreach ($valuesArray as $s) {
        $style[] = "let " . clearClassId($s) . " = $(\"" . $s . "\") . val();";
        $crypt[] = "let " . clearClassId($s) . "Encrypt = CryptoJS . AES . encrypt(JSON . stringify(" . clearClassId($s) . "), key, {format: CryptoJSAesJson}).toString();";

        ++$i;

        $a = '&';
        $b = ' + ';
        if (1 === $i) {
            $a = '';
        }
        if ($cnt === $i) {
            $b = '';
        }

        $url[] = "'" . $a . clearClassId($s) . " =' + " . clearClassId($s) . "Encrypt" . $b;

        $end = ' && ';
        if ($cnt === $i) {
            $end = '';
        }

        $reason[] = clearClassId($s) . " !== ''" . $end;
    }

    $styles = implode("\n", $style);
    $crypts = implode("\n", $crypt);
    $urls = implode("", $url);
    $reasons = implode("", $reason);

    return " <div class=\"overlay-wig\"></div>
  <script >
    $(document) . ready(function () {
  
        $('" . $array['idClassName'] . "') . on('click', function (e) {
            e . preventDefault();
            
        $(\"body\").addClass(\"loading-wig\");     
            
         " . $styles . "
            let key = '" . SSL_ENCRYPT_SECURE_KEY . "';
         " . $crypts . "
            
            let dataString = " . $urls . ";

            if (" . $reasons . ") {
                $.ajax({
                    type: \"POST\",
                    url: '" . $array['ajaxUrl'] . "',
                    data: dataString,
                    cache: false,
                    success: function (result) {
                    window . location . href = '" . $array['redirect'] . "';
                },
                    statusCode: {
                   403: function () {
                               $(\"" . $array['idClassName'] . "\").click();
                               $(\"body\").removeClass(\"loading-wig\"); 
                        },
                   404: function () {
                            alert('" . $array['error'] . "');
                            $(\"body\").removeClass(\"loading-wig\"); 
                        } ,
                   401: function () {
                            alert('" . $array['error'] . "');
                            $(\"body\").removeClass(\"loading-wig\"); 
                        } ,     
                   101: function () {
                            alert(result);
                            $(\"body\").removeClass(\"loading-wig\"); 
                        },  
                   500: function () {
                           $(\"" . $array['idClassName'] . "\").click();
                           $(\"body\").removeClass(\"loading-wig\"); 
                        }                        
                    }
                });
            }
        });
    });

    </script >";
}



function sendMessageTelegram($chatID, $messaggio, $token) {
    echo "sending message to " . $chatID . "\n";

    $url = "https://api.telegram.org/bot" . $token . "/sendMessage?chat_id=" . $chatID;
    $url = $url . "&text=" . urlencode($messaggio);
    $ch = curl_init();
    $optArray = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function cors() {

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

    echo "You have CORS!";
}