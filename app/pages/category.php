<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 class="page-name">Женщинам</h2>
            <p style="margin-top: -10px;" class="category-path"><img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico"> Женщинам</p>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="/client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/client/categories">Категории</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Категория</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">
        <div class="row to-toggle-loader">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Выручка</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="turnover">8,656,536,276</h1>
                        <div class="stat-percent font-bold text-navy" id="turnover-diff">1% <i class="fa fa-level-up"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Продаж</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="sales">4,925,064</h1>
                        <div class="stat-percent font-bold text-danger" id="sales-diff">2% <i class="fa fa-level-down"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Средний чек</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="bill">1,758</h1>
                        <div class="stat-percent font-bold text-navy" id="bill-diff">3% <i class="fa fa-level-up"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Остатки</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="amount">40,329,992</h1>
                        <div class="stat-percent font-bold text-success" id="amount-diff">0%</div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector" style="float: left;">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                    <label class="btn btn-sm btn-white" style="display: none;"> <input type="radio" id="option4" name="period" value="period"> Период </label>
                </div>
                <div class="form-group" style="display: none;float: left;margin-left: 9px;margin-top: 2px;">
                    <input type="text" id="date" name="q[date][between]" class="form-control">
                </div>
            </div>
        </div>

        <diw class="row to-toggle-loader">
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Состояние склада</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 332px; height: 300px;" id="chart-amount" height="300" width="332" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Динамика продаж</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 332px; height: 300px;" id="chart-price" height="300" width="332" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <diw class="row to-toggle-loader">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Распределение числа продаж от цены на товар</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div>
                            <canvas style="margin: 0px auto; display: block; box-sizing: border-box; width: 733px; height: 400px;" id="chart-prices2" height="400" width="733"></canvas>
                        </div>
                        <div style="margin-left: 65px;">
                            <input type="hidden" id="price-from" value="39">
                            <input type="hidden" id="price-to" value="150000">
                            <div id="slider-range" style="margin-bottom: 15px;" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span></div>
                            <p><strong>Выбранный диапазон цен:</strong> <span id="amount-range">39 руб. - 150000 руб.</span> <button id="apply-prices" type="button" class="btn btn-outline btn-primary">Показать товары</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs nav-tabs-entity" style="float: left;" role="tablist">
                        <li data-original-title="" title="">
                            <a class="nav-link active" data-toggle="tab" data-constructor="htmlConstructor" href="#tab-1">Товары</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-type="seller" data-constructor="htmlSellerConstructor" href="#tab-1">Продавцы</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-type="brand" data-constructor="htmlBrandConstructor" href="#tab-1">Бренды</a>
                        </li>
                    </ul>
                    <ul class="nav nav-tabs nav-tabs-type" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="only-new">
                                        <li><strong>Только новинки</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="is_new" class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <ul class="show-filters">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[turnover][between]">
                                                    <div class="col-lg-5"><strong>Оборот</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_days_in_period][between]">
                                                    <div class="col-lg-4"><strong>Период<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих запись о товаре">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[rating][between]">
                                                    <div class="col-lg-5"><strong>Рейтинг</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[loses][between]">
                                                    <div class="col-lg-5"><strong>Упущенный оброт <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка вызванная обнулением остатков">?</span></strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[last_amount][between]">
                                                    <div class="col-lg-4"><strong>Остаток </strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[reviews][between]">
                                                    <div class="col-lg-5"><strong>Отзывы</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_sales][between]">
                                                    <div class="col-lg-5"><strong>Кол-во продаж</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[days_with_sales][between]">
                                                    <div class="col-lg-4"><strong>Продажи <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих продажи">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[avg_position][between]">
                                                    <div class="col-lg-5" style="line-height: 0px;"><strong>Позиция в категории</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Фото</th>
                                            <th>Название</th>
                                            <th>SKU</th>
                                            <th>Продавец</th>
                                            <th>Бренд</th>
                                            <th>Наличие</th>
                                            <th>Цена</th>
                                            <th style="width: 65px;">ACP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Средняя позиция в категории">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="loses">LP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка, вызванная обнулением остатков">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="total_sales">Продажи</th>
                                            <th class="sorting" style="min-width: 95px;" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>


                                            <td>
                                                <a href="" data-id="32025179" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="27551457">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/27550000/27551457-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=32025179">Джинсы женские с высокой посадкой (мом,mom,бананы,широкие, клёш, рваные, летние, трубы) Для девочек</a><br>

                                                <a class="category-link" href="/client/category?id=3240">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Джинсы
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1609)</span>
                                                </div>
                                            </td>
                                            <td>27551457</td>
                                            <td><a href="/client/seller?id=99890">ИП Банишевский Георгий Александрович (321774600102053)</a></td>
                                            <td><a href="/client/brand?id=153002">Bronks</a></td>
                                            <td>1360</td>
                                            <td>
                                                2,575&nbsp;руб. <br>

                                                <span class="old-price-percent">(-36%)</span>
                                                <span class="old-price">4,065&nbsp;руб.</span>

                                            </td>
                                            <td>6</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>1219</td>
                                            <td>3,235,410&nbsp;руб.</td>
                                            <td><span id="sparkline32025179"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="27551457" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="3973128" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13841839">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13840000/13841839-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=3973128">Пальто</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(339)</span>
                                                </div>
                                            </td>
                                            <td>13841839</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=63358">GEOX</a></td>
                                            <td>0</td>
                                            <td>
                                                12,654&nbsp;руб. <br>

                                                <span class="old-price-percent">(-56%)</span>
                                                <span class="old-price">28,990&nbsp;руб.</span>

                                            </td>
                                            <td>67363</td>
                                            <td>17,842,140&nbsp;руб.</td>
                                            <td>235</td>
                                            <td>2,973,690&nbsp;руб.</td>
                                            <td><span id="sparkline3973128"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13841839" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="13565209" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="17568667">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/17560000/17568667-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=13565209">Худи</a><br>

                                                <a class="category-link" href="/client/category?id=3278">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Худи
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(187)</span>
                                                </div>
                                            </td>
                                            <td>17568667</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=56860">Tommy Hilfiger</a></td>
                                            <td>0</td>
                                            <td>
                                                4,945&nbsp;руб. <br>

                                                <span class="old-price-percent">(-55%)</span>
                                                <span class="old-price">10,990&nbsp;руб.</span>

                                            </td>
                                            <td>9059</td>
                                            <td>8,742,760&nbsp;руб.</td>
                                            <td>442</td>
                                            <td>2,185,690&nbsp;руб.</td>
                                            <td><span id="sparkline13565209"><canvas width="24" height="40" style="display: inline-block; width: 24px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="17568667" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="16165541" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="18132753">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/18130000/18132753-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=16165541">Куртка</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(28)</span>
                                                </div>
                                            </td>
                                            <td>18132753</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=56860">Tommy Hilfiger</a></td>
                                            <td>0</td>
                                            <td>
                                                19,345&nbsp;руб. <br>

                                                <span class="old-price-percent">(-55%)</span>
                                                <span class="old-price">42,990&nbsp;руб.</span>

                                            </td>
                                            <td>2728</td>
                                            <td>10,252,850&nbsp;руб.</td>
                                            <td>106</td>
                                            <td>2,050,570&nbsp;руб.</td>
                                            <td><span id="sparkline16165541"><canvas width="29" height="40" style="display: inline-block; width: 29px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="18132753" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="3948757" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13842026">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13840000/13842026-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=3948757">Куртка</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(117)</span>
                                                </div>
                                            </td>
                                            <td>13842026</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=63358">GEOX</a></td>
                                            <td>0</td>
                                            <td>
                                                6,300&nbsp;руб. <br>

                                                <span class="old-price-percent">(-51%)</span>
                                                <span class="old-price">12,990&nbsp;руб.</span>

                                            </td>
                                            <td>21476</td>
                                            <td>10,017,000&nbsp;руб.</td>
                                            <td>318</td>
                                            <td>2,003,400&nbsp;руб.</td>
                                            <td><span id="sparkline3948757"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13842026" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="301073671" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="53389663">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/53380000/53389663-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=301073671">Платье</a><br>

                                                <a class="category-link" href="/client/category?id=3264">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Платья и сарафаны
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(788)</span>
                                                </div>
                                            </td>
                                            <td>53389663</td>
                                            <td><a href="/client/seller?id=385762">ИП Шамрова В. Г. (321774600750894)</a></td>
                                            <td><a href="/client/brand?id=20442582">C-more</a></td>
                                            <td>343</td>
                                            <td>
                                                1,997&nbsp;руб. <br>

                                                <span class="old-price-percent">(-75%)</span>
                                                <span class="old-price">7,990&nbsp;руб.</span>

                                            </td>
                                            <td>3</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>949</td>
                                            <td>1,880,353&nbsp;руб.</td>
                                            <td><span id="sparkline301073671"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="53389663" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="3974511" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="15953057">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/15950000/15953057-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=3974511">Пальто женские демисезонные, верхняя женская одежда, пальто женское зимнее, подарок на Новый год</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(567)</span>
                                                </div>
                                            </td>
                                            <td>15953057</td>
                                            <td><a href="/client/seller?id=20348">ИП Матвиенко Виктория Юрьевна (313501108700033)</a></td>
                                            <td><a href="/client/brand?id=58836">MATVIENKO</a></td>
                                            <td>47</td>
                                            <td>
                                                10,088&nbsp;руб. <br>

                                                <span class="old-price-percent">(-60%)</span>
                                                <span class="old-price">25,475&nbsp;руб.</span>

                                            </td>
                                            <td>165</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>176</td>
                                            <td>1,675,071&nbsp;руб.</td>
                                            <td><span id="sparkline3974511"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="15953057" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="4572993" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13174194">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13170000/13174194-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=4572993">Брюки трикотажные широкие /свободные / штаны хлопковые/ спортивные, Зари</a><br>

                                                <a class="category-link" href="/client/category?id=3334">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Большие размеры / Джинсы, брюки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(3108)</span>
                                                </div>
                                            </td>
                                            <td>13174194</td>
                                            <td><a href="/client/seller?id=21509">ОМЕГА ООО (1207700067673)</a></td>
                                            <td><a href="/client/brand?id=58864">Зари</a></td>
                                            <td>3443</td>
                                            <td>
                                                1,045&nbsp;руб. <br>

                                                <span class="old-price-percent">(-59%)</span>
                                                <span class="old-price">2,550&nbsp;руб.</span>

                                            </td>
                                            <td>4</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>1579</td>
                                            <td>1,657,297&nbsp;руб.</td>
                                            <td><span id="sparkline4572993"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13174194" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="32636001" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="26193076">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/26190000/26193076-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=32636001">Юбка женская / юбка кожа / юбка кожаная женская / юбка карандаш женская / юбка женская теплая / миди</a><br>

                                                <a class="category-link" href="/client/category?id=3280">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Юбки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1597)</span>
                                                </div>
                                            </td>
                                            <td>26193076</td>
                                            <td><a href="/client/seller?id=26052">Воробьева Эльвира Фанисовна ИП (310745028100033)</a></td>
                                            <td><a href="/client/brand?id=248857">NIKONOROVA</a></td>
                                            <td>5675</td>
                                            <td>
                                                1,916&nbsp;руб. <br>

                                                <span class="old-price-percent">(-68%)</span>
                                                <span class="old-price">5,990&nbsp;руб.</span>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>770</td>
                                            <td>1,624,940&nbsp;руб.</td>
                                            <td><span id="sparkline32636001"><canvas width="29" height="40" style="display: inline-block; width: 29px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="26193076" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="55261854" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="34425969">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/34420000/34425969-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=55261854">Пальто / Пальто женское / Пальто вязаное / Длинное пальто / Осеннее пальто / Кардиган женский</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(114)</span>
                                                </div>
                                            </td>
                                            <td>34425969</td>
                                            <td><a href="/client/seller?id=61791">ИП Юшников Игорь Игоревич (320619600139086)</a></td>
                                            <td><a href="/client/brand?id=133160">VESHALKA</a></td>
                                            <td>176</td>
                                            <td>
                                                3,960&nbsp;руб. <br>

                                                <span class="old-price-percent">(-60%)</span>
                                                <span class="old-price">10,000&nbsp;руб.</span>

                                            </td>
                                            <td>7</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>406</td>
                                            <td>1,613,820&nbsp;руб.</td>
                                            <td><span id="sparkline55261854"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="34425969" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="22847077" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="20953714">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/20950000/20953714-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=22847077">Джинсы зауженные / джинсы скинни / джинсы с высокой посадкой DENIM</a><br>

                                                <a class="category-link" href="/client/category?id=3240">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Джинсы
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1067)</span>
                                                </div>
                                            </td>
                                            <td>20953714</td>
                                            <td><a href="/client/seller?id=60818">ИП Степанов Александр Витальевич (320774600468381)</a></td>
                                            <td><a href="/client/brand?id=189674">Pear</a></td>
                                            <td>349</td>
                                            <td>
                                                1,621&nbsp;руб. <br>

                                                <span class="old-price-percent">(-67%)</span>
                                                <span class="old-price">4,990&nbsp;руб.</span>

                                            </td>
                                            <td>3</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>995</td>
                                            <td>1,612,895&nbsp;руб.</td>
                                            <td><span id="sparkline22847077"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="20953714" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="15154408" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="18221972">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/18220000/18221972-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=15154408">Кроссовки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(23)</span>
                                                </div>
                                            </td>
                                            <td>18221972</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=61898">New balance</a></td>
                                            <td>1</td>
                                            <td>
                                                8,900&nbsp;руб. <br>

                                                <span class="old-price-percent">(-1%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>169407</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>175</td>
                                            <td>1,557,500&nbsp;руб.</td>
                                            <td><span id="sparkline15154408"><canvas width="4" height="40" style="display: inline-block; width: 4px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="18221972" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="32025181" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="27551458">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/27550000/27551458-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=32025181">Джинсы женские с высокой посадкой (мом,mom,бананы,широкие, клёш, рваные, летние, трубы) Для девочек</a><br>

                                                <a class="category-link" href="/client/category?id=3240">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Джинсы
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1609)</span>
                                                </div>
                                            </td>
                                            <td>27551458</td>
                                            <td><a href="/client/seller?id=99890">ИП Банишевский Георгий Александрович (321774600102053)</a></td>
                                            <td><a href="/client/brand?id=153002">Bronks</a></td>
                                            <td>14</td>
                                            <td>
                                                2,495&nbsp;руб. <br>

                                                <span class="old-price-percent">(-38%)</span>
                                                <span class="old-price">4,065&nbsp;руб.</span>

                                            </td>
                                            <td>60</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>592</td>
                                            <td>1,535,476&nbsp;руб.</td>
                                            <td><span id="sparkline32025181"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="27551458" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="10544756" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="15813752">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/15810000/15813752-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=10544756">Жилет/ Жилет женский/ Жилет асимметричный/ Жилетка женская/ Жилет кожаный/ жилет на одно плечо</a><br>

                                                <a class="category-link" href="/client/category?id=3243">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Жилеты
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1590)</span>
                                                </div>
                                            </td>
                                            <td>15813752</td>
                                            <td><a href="/client/seller?id=19120">ИП Карзакова Нина Алексеевна (319420500052810)</a></td>
                                            <td><a href="/client/brand?id=57128">Annemore</a></td>
                                            <td>1991</td>
                                            <td>
                                                2,920&nbsp;руб. <br>

                                                <span class="old-price-percent">(-75%)</span>
                                                <span class="old-price">11,990&nbsp;руб.</span>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>504</td>
                                            <td>1,478,060&nbsp;руб.</td>
                                            <td><span id="sparkline10544756"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="15813752" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="3949663" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13158496">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13150000/13158496-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=3949663">Куртка Reimatec Nappaa</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(342)</span>
                                                </div>
                                            </td>
                                            <td>13158496</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=58862">Reima</a></td>
                                            <td>0</td>
                                            <td>
                                                3,398&nbsp;руб. <br>

                                                <span class="old-price-percent">(-53%)</span>
                                                <span class="old-price">7,299&nbsp;руб.</span>

                                            </td>
                                            <td>null</td>
                                            <td>7,101,820&nbsp;руб.</td>
                                            <td>418</td>
                                            <td>1,420,364&nbsp;руб.</td>
                                            <td><span id="sparkline3949663"><canvas width="24" height="40" style="display: inline-block; width: 24px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13158496" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="4749651" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="12492367">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/12490000/12492367-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=4749651">Кроссовки CARNABY ACE 120 7 SFA</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(16)</span>
                                                </div>
                                            </td>
                                            <td>12492367</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=56665">Lacoste</a></td>
                                            <td>1</td>
                                            <td>
                                                9,884&nbsp;руб. <br>

                                                <span class="old-price-percent">(-20%)</span>
                                                <span class="old-price">12,480&nbsp;руб.</span>

                                            </td>
                                            <td>163103</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>113</td>
                                            <td>1,396,115&nbsp;руб.</td>
                                            <td><span id="sparkline4749651"><canvas width="14" height="40" style="display: inline-block; width: 14px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="12492367" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="12712702" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="17361478">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/17360000/17361478-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=12712702">Пальто/ Стеганое пальто/ Пальто плащевое/ Плащ утепленный/ Плащ стеганый</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(548)</span>
                                                </div>
                                            </td>
                                            <td>17361478</td>
                                            <td><a href="/client/seller?id=23423">ИП Тюков Роман Николаевич (315774600130569)</a></td>
                                            <td><a href="/client/brand?id=64960">AVALON</a></td>
                                            <td>3853</td>
                                            <td>
                                                10,347&nbsp;руб. <br>

                                                <span class="old-price-percent">(-33%)</span>
                                                <span class="old-price">15,600&nbsp;руб.</span>

                                            </td>
                                            <td>26</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>135</td>
                                            <td>1,396,110&nbsp;руб.</td>
                                            <td><span id="sparkline12712702"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="17361478" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="8352213" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13418923">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13410000/13418923-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=8352213">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(133)</span>
                                                </div>
                                            </td>
                                            <td>13418923</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=63358">GEOX</a></td>
                                            <td>1</td>
                                            <td>
                                                4,360&nbsp;руб. <br>

                                                <span class="old-price-percent">(-51%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>84584</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>318</td>
                                            <td>1,386,480&nbsp;руб.</td>
                                            <td><span id="sparkline8352213"><canvas width="14" height="40" style="display: inline-block; width: 14px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13418923" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="29218783" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="25753449">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/25750000/25753449-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=29218783">Пальто/ Стеганое пальто/ Пальто плащевое/ Плащ утепленный/ Плащ стеганый</a><br>

                                                <a class="category-link" href="/client/category?id=3210">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Одежда / Верхняя одежда
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(217)</span>
                                                </div>
                                            </td>
                                            <td>25753449</td>
                                            <td><a href="/client/seller?id=23423">ИП Тюков Роман Николаевич (315774600130569)</a></td>
                                            <td><a href="/client/brand?id=64960">AVALON</a></td>
                                            <td>209</td>
                                            <td>
                                                9,850&nbsp;руб. <br>

                                                <span class="old-price-percent">(-33%)</span>
                                                <span class="old-price">14,850&nbsp;руб.</span>

                                            </td>
                                            <td>279</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>140</td>
                                            <td>1,376,096&nbsp;руб.</td>
                                            <td><span id="sparkline29218783"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="25753449" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="15154417" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="18221975">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/18220000/18221975-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=15154417">Кроссовки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(24)</span>
                                                </div>
                                            </td>
                                            <td>18221975</td>
                                            <td><a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a></td>
                                            <td><a href="/client/brand?id=61898">New balance</a></td>
                                            <td>1</td>
                                            <td>
                                                6,777&nbsp;руб. <br>

                                                <span class="old-price-percent">(-47%)</span>
                                                <span class="old-price">12,990&nbsp;руб.</span>

                                            </td>
                                            <td>105102</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>201</td>
                                            <td>1,362,177&nbsp;руб.</td>
                                            <td><span id="sparkline15154417"><canvas width="9" height="40" style="display: inline-block; width: 9px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="18221975" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (2222936)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #modal .modal-dialog {
            max-width: 900px;
        }
    </style>
    <script src="<?=PROJECT_URL?>client/js/pages/category.js?id=21"></script></div>