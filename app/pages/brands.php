<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Бренды</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Бренды</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div id="get-extra-days" class="ibox-content" style="display: none;">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row">
            <h2>Пройдите обучение и получите +6 дней премиум доступа в подарок!</h2>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="extra-video active" data-days="1" data-lesson="40129654" data-step="0" data-video-id="lw9KLm1C0mU">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 1</p>
                        <span>Обратный поиск товаров</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play active"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129657" data-step="1" data-video-id="pIPzI2KxCcA">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 2</p>
                        <span>Анализ ниш на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129669" data-step="2" data-video-id="gz8dw0Sfx3s">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 3</p>
                        <span>Выявление хайповых товаров на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129672" data-step="3" data-video-id="hjpdOSwQ0es">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 4</p>
                        <span>Аналитика категорий на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129675" data-step="4" data-video-id="9a9F3tqpJoo">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 5</p>
                        <span>Конкурентный анализ</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="6" data-lesson="40193338" data-step="5" data-video-id="ZRc2Bwwwl18">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 6</p>
                        <span>API кабинет и аналитика собственного магазина</span>
                        <div class="extra-video-label"><img src="/client/img/video-gift.svg" alt=""> + 6 дней после прохождения!</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="close-extra-video"><img src="/client/img/video-close.svg" alt=""></div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: left" role="tablist">
                        <li data-original-title="" title="">
                            <a class="nav-link active" data-toggle="tab" href="#tab-1">Все</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-field="mp" data-field-value="wildberries" data-toggle="tab" href="#tab-1">Wildberries</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ozon" href="#tab-1">Ozon</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ali" href="#tab-1">AliExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="beru" href="#tab-1">Я.Маркет</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="kazan" href="#tab-1">KazanExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="sber" href="#tab-1">СберМегаМаркет</a>
                        </li>
                    </ul>
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="only-new">
                                        <li><strong>Только новинки</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="is_new" class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <ul class="show-filters">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[turnover][between]">
                                                    <div class="col-lg-5"><strong>Оборот</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[rating][between]">
                                                    <div class="col-lg-4"><strong>Рейтинг</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[average_bill][between]">
                                                    <div class="col-lg-5"><strong>Средний чек</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_sales][between]">
                                                    <div class="col-lg-5"><strong>Продажи</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[products_count][between]">
                                                    <div class="col-lg-4"><strong>Товаров</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Бренд</th>
                                            <th>Товаров</th>
                                            <th>Рейтинг</th>
                                            <th>Средний чек</th>
                                            <th class="sorting" data-sort="total_sales">Продажи</th>
                                            <th class="sorting" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/brand?id=128120">Ali</a>
                                            </td>
                                            <td>444455</td>
                                            <td>4.7</td>
                                            <td>1,986&nbsp;руб.</td>
                                            <td>694613</td>
                                            <td>1,379,372,967&nbsp;руб.</td>
                                            <td><button data-id="1" data-mp="ali" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/brand?id=130">Graspy</a>
                                            </td>
                                            <td>399880</td>
                                            <td>4.7</td>
                                            <td>463&nbsp;руб.</td>
                                            <td>973091</td>
                                            <td>450,386,276&nbsp;руб.</td>
                                            <td><button data-id="0" data-mp="ozon" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56601">adidas</a>
                                            </td>
                                            <td>7199</td>
                                            <td>4.3</td>
                                            <td>1,835&nbsp;руб.</td>
                                            <td>141705</td>
                                            <td>260,052,745&nbsp;руб.</td>
                                            <td><button data-id="21" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/brand?id=3">Apple</a>
                                            </td>
                                            <td>448</td>
                                            <td>4.9</td>
                                            <td>29,878&nbsp;руб.</td>
                                            <td>6555</td>
                                            <td>195,853,375&nbsp;руб.</td>
                                            <td><button data-id="26303000" data-mp="ozon" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>9511</td>
                                            <td>4</td>
                                            <td>3,578&nbsp;руб.</td>
                                            <td>38933</td>
                                            <td>139,295,343&nbsp;руб.</td>
                                            <td><button data-id="19467" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>4906</td>
                                            <td>4.1</td>
                                            <td>16,054&nbsp;руб.</td>
                                            <td>8627</td>
                                            <td>138,502,152&nbsp;руб.</td>
                                            <td><button data-id="5772" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/brand?id=2">Xiaomi</a>
                                            </td>
                                            <td>3398</td>
                                            <td>4.7</td>
                                            <td>4,364&nbsp;руб.</td>
                                            <td>31104</td>
                                            <td>135,727,478&nbsp;руб.</td>
                                            <td><button data-id="32686750" data-mp="ozon" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56392">ТВОЕ</a>
                                            </td>
                                            <td>8093</td>
                                            <td>4.6</td>
                                            <td>631&nbsp;руб.</td>
                                            <td>197226</td>
                                            <td>124,500,839&nbsp;руб.</td>
                                            <td><button data-id="3991" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/brand?id=7">Samsung</a>
                                            </td>
                                            <td>2300</td>
                                            <td>4.8</td>
                                            <td>14,928&nbsp;руб.</td>
                                            <td>7437</td>
                                            <td>111,017,756&nbsp;руб.</td>
                                            <td><button data-id="24565087" data-mp="ozon" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=90083">Apple</a>
                                            </td>
                                            <td>655</td>
                                            <td>4.4</td>
                                            <td>26,991&nbsp;руб.</td>
                                            <td>3740</td>
                                            <td>100,947,004&nbsp;руб.</td>
                                            <td><button data-id="6049" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=81106">Philips</a>
                                            </td>
                                            <td>1277</td>
                                            <td>4.3</td>
                                            <td>3,109&nbsp;руб.</td>
                                            <td>32098</td>
                                            <td>99,787,498&nbsp;руб.</td>
                                            <td><button data-id="6012" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=63358">GEOX</a>
                                            </td>
                                            <td>5227</td>
                                            <td>4.4</td>
                                            <td>4,137&nbsp;руб.</td>
                                            <td>22309</td>
                                            <td>92,295,279&nbsp;руб.</td>
                                            <td><button data-id="370" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=95756">DFC</a>
                                            </td>
                                            <td>3692</td>
                                            <td>4</td>
                                            <td>29,341&nbsp;руб.</td>
                                            <td>3125</td>
                                            <td>91,690,309&nbsp;руб.</td>
                                            <td><button data-id="62528" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56491">GUESS</a>
                                            </td>
                                            <td>7323</td>
                                            <td>4.1</td>
                                            <td>3,321&nbsp;руб.</td>
                                            <td>23206</td>
                                            <td>77,070,924&nbsp;руб.</td>
                                            <td><button data-id="398" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56401">Befree</a>
                                            </td>
                                            <td>7948</td>
                                            <td>4.4</td>
                                            <td>1,166&nbsp;руб.</td>
                                            <td>64383</td>
                                            <td>75,058,406&nbsp;руб.</td>
                                            <td><button data-id="4126" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89645">SOKOLOV</a>
                                            </td>
                                            <td>20658</td>
                                            <td>4.6</td>
                                            <td>2,657&nbsp;руб.</td>
                                            <td>26805</td>
                                            <td>71,222,212&nbsp;руб.</td>
                                            <td><button data-id="4603" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=72750">ECCO</a>
                                            </td>
                                            <td>1403</td>
                                            <td>4.3</td>
                                            <td>7,737&nbsp;руб.</td>
                                            <td>8902</td>
                                            <td>68,877,960&nbsp;руб.</td>
                                            <td><button data-id="291" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56628">S.OLIVER</a>
                                            </td>
                                            <td>21845</td>
                                            <td>4.3</td>
                                            <td>1,877&nbsp;руб.</td>
                                            <td>35269</td>
                                            <td>66,185,772&nbsp;руб.</td>
                                            <td><button data-id="806" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=278072">КАРАНДАНДИУМ</a>
                                            </td>
                                            <td>3838</td>
                                            <td>null</td>
                                            <td>14,662&nbsp;руб.</td>
                                            <td>4500</td>
                                            <td>65,980,665&nbsp;руб.</td>
                                            <td><button data-id="246157" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=59009">ASICS</a>
                                            </td>
                                            <td>3206</td>
                                            <td>4.4</td>
                                            <td>2,594&nbsp;руб.</td>
                                            <td>24864</td>
                                            <td>64,487,810&nbsp;руб.</td>
                                            <td><button data-id="61" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (539610)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=PROJECT_URL?>client/js/pages/brands.js?id=7"></script></div>