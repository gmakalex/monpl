<div id="page-content" style=""><div style="display: block;" class="row wrapper border-bottom white-bg page-heading">
        <div class="row" id="product-info">
            <div class="col-lg-12">
                <table>
                    <tbody><tr>
                        <td id="for-image"><img style="height: 100px; margin-right: 10px;" src="https://images.wbstatic.net/c246x328/new/13730000/13738266-1.jpg"></td>
                        <td>
                            <h2 class="page-name">Носки мужские / OMSA ECO 401/ носки мужские набор - 10 пар / носки длинные / носки высокие</h2>
                            <p style="margin-top: -10px;" class="category-path"><img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico"> Мужчинам / Белье</p>
                            <p class="link"><a target="_blank" href="https://www.wildberries.ru/catalog/13738266/detail.aspx">https://www.wildberries.ru/catalog/13738266/detail.aspx</a></p>
                        </td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
        <!--<div style="display: block;" class="row">
            <button style="display:none;float: right;margin: -31px 28px 0 28px;position: relative;z-index: 10;" class="btn btn-primary" id="get-sertificate">Сертифицировать товар</button>
            &lt;!&ndash;<button style="float: right;margin: -31px 28px 0;position: relative;z-index: 10;" class="btn btn-warning" id="get-stats">Заказать аналитику товара</button>&ndash;&gt;
            <a href="https://mrqz.me/613b4226157d21003f5501a5?utm_source=moneyplace&utm_medium=site&utm_campaign=promo&utm_content=link" target="_blank" style="float: right;margin: -31px 0px 0 0;position: relative;z-index: 10;border-color: #1f84c6;color: #1f84c6;" class="btn btn-default" >Отправить на фулфилмент</a>
        </div>-->
        <div class="row" style="float: right;margin-top: -35px;margin-right: 17px;height: 40px;position: relative;z-index: 100;">
            <button id="create-transaction" style="margin-right: 15px;height: 34px;" type="button" class="btn btn-outline btn-warning">Заведите меня на маркетплейс</button>
            <div class="form-group">
                <select id="services" style="background: #1cb393;color: #fff;border-radius: 3px;border: none;-webkit-appearance: none;text-align: center;font-size: 13px;" class="form-control">
                    <option value="">Дополнительные опции</option>
                    <option value="seo">Заказать SEO оптимизацию</option>
                    <option value="cert">Сертифицировать товар</option>
                    <option value="fullfilment">Отправить на фулфилмент</option>
                    <!--<option value="alanytics">Заказать подбор товара</option>
                    <option value="credit">Получить оборотные средства</option>-->
                </select>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content product-page" id="content">

        <div class="row to-toggle-loader product-page-first-line">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Выручка</h5>
                                <div class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Разница со вчерашним днем">?</div>
                                <div class="stat-percent font-bold text-navy" id="turnover-diff">5% <i class="fa fa-level-up"></i></div>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="turnover" style="font-size: 26px">10,474,497 <span>р.</span></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Продаж</h5>
                                <div class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Разница со вчерашним днем">?</div>
                                <div class="stat-percent font-bold text-navy" id="sales-diff">5% <i class="fa fa-level-up"></i></div>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="sales">18,734 <span>шт.</span></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Ср. чек</h5>
                                <div class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Разница со вчерашним днем">?</div>
                                <div class="stat-percent font-bold text-success" id="bill-diff">0%</div>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="bill">559 <span>р.</span></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5>Остатки</h5>
                                <div class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Разница со вчерашним днем">?</div>
                                <div class="stat-percent font-bold text-danger" id="amount-diff">15% <i class="fa fa-level-down"></i></div>
                            </div>
                            <div class="ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <h1 class="no-margins" id="amount">16,663 <span>шт.</span></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox-content" id="position-hint">
                    <h5 style="font-size: 12px;margin-bottom: 0;">
                        Позиция категории <button id="show-positions" class="btn btn-primary btn-xs">Все позиции</button>
                    </h5>
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div id="sparkline1" class="m-b-sm"><canvas style="display: inline-block; width: 345.75px; height: 40px; vertical-align: top;" width="345" height="40"></canvas></div>
                    <div class="row" style="margin-bottom: -12px;">
                        <div class="col-5">
                            <small class="stats-label">Прошлая неделя</small>
                            <h4 id="position-last-week">1</h4>
                        </div>

                        <div class="col-4">
                            <small class="stats-label">Динамика</small>
                            <h4 id="position-increase">0%</h4>
                        </div>
                        <div class="col-3">
                            <small class="stats-label">Сейчас</small>
                            <h4 id="position-now">1</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="product-modifications" style="">
            <div><strong>Все<br>товары</strong></div>

            <div data-id="33806175" class="" style="background: url('https://images.wbstatic.net/c246x328/new/29000000/29005620-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/29000000/29005620-1.jpg" alt="">
            </div>

            <div data-id="655670981" class="" style="background: url('https://images.wbstatic.net/c246x328/new/62150000/62156571-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/62150000/62156571-1.jpg" alt="">
            </div>

            <div data-id="655670983" class="" style="background: url('https://images.wbstatic.net/c246x328/new/62150000/62156568-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/62150000/62156568-1.jpg" alt="">
            </div>

            <div data-id="82139902" class="" style="background: url('https://images.wbstatic.net/c246x328/new/46500000/46502315-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/46500000/46502315-1.jpg" alt="">
            </div>

            <div data-id="111847066" class="" style="background: url('https://images.wbstatic.net/c246x328/new/49450000/49454959-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/49450000/49454959-1.jpg" alt="">
            </div>

            <div data-id="33806176" class="" style="background: url('https://images.wbstatic.net/c246x328/new/29000000/29005621-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/29000000/29005621-1.jpg" alt="">
            </div>

            <div data-id="655670984" class="" style="background: url('https://images.wbstatic.net/c246x328/new/62150000/62156569-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/62150000/62156569-1.jpg" alt="">
            </div>

            <div data-id="111847064" class="" style="background: url('https://images.wbstatic.net/c246x328/new/49450000/49454958-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/49450000/49454958-1.jpg" alt="">
            </div>

            <div data-id="655670980" class="" style="background: url('https://images.wbstatic.net/c246x328/new/62150000/62156567-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/62150000/62156567-1.jpg" alt="">
            </div>

            <div data-id="111847075" class="" style="background: url('https://images.wbstatic.net/c246x328/new/49450000/49454957-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/49450000/49454957-1.jpg" alt="">
            </div>

            <div data-id="5325638" class="" style="background: url('https://images.wbstatic.net/c246x328/new/13730000/13738264-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/13730000/13738264-1.jpg" alt="">
            </div>

            <div data-id="5325349" class="" style="background: url('https://images.wbstatic.net/c246x328/new/13730000/13738265-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/13730000/13738265-1.jpg" alt="">
            </div>

            <div data-id="5325333" class="active" style="background: url('https://images.wbstatic.net/c246x328/new/13730000/13738266-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/13730000/13738266-1.jpg" alt="">
            </div>

            <div data-id="44470315" class="" style="background: url('https://images.wbstatic.net/c246x328/new/39140000/39148358-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/39140000/39148358-1.jpg" alt="">
            </div>

            <div data-id="44470308" class="" style="background: url('https://images.wbstatic.net/c246x328/new/39140000/39148356-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/39140000/39148356-1.jpg" alt="">
            </div>

            <div data-id="111847072" class="" style="background: url('https://images.wbstatic.net/c246x328/new/49450000/49454955-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/49450000/49454955-1.jpg" alt="">
            </div>

            <div data-id="111847076" class="" style="background: url('https://images.wbstatic.net/c246x328/new/49450000/49454956-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/49450000/49454956-1.jpg" alt="">
            </div>

            <div data-id="655670982" class="" style="background: url('https://images.wbstatic.net/c246x328/new/62150000/62156570-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/62150000/62156570-1.jpg" alt="">
            </div>

            <div data-id="71590541" class="" style="background: url('https://images.wbstatic.net/c246x328/new/46500000/46502316-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/46500000/46502316-1.jpg" alt="">
            </div>

            <div data-id="44470311" class="" style="background: url('https://images.wbstatic.net/c246x328/new/39140000/39148357-1.jpg') no-repeat center">
                <img src="https://images.wbstatic.net/c246x328/new/39140000/39148357-1.jpg" alt="">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector" style="float: left;">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option4" name="period" value="period"> Период </label>
                </div>
                <div class="form-group" style="display: none;float: left;margin-left: 9px;margin-top: 2px;">
                    <input type="text" id="date" name="q[date][between]" class="form-control">
                </div>
            </div>
        </div>

        <div class="row to-toggle-loader">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3 class="font-bold no-margins">
                            Динамика продаж
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-8">
                                    <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                        <canvas style="margin: 0px auto; display: block; width: 483px; height: 200px;" id="chart-price" height="200" class="chartjs-render-monitor" width="483"></canvas>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul class="stat-list m-t-lg" id="predict-sales-hint">
                                        <li>
                                            <h2 class="no-margins" id="predicted-month-turnover">42,178,320 руб.</h2>
                                            <small>Ожидаемые продажи за февраль</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" id="predicted-turnover-bar" style="width: 31%"></div>
                                            </div>

                                        </li>
                                        <li>
                                            <h2 class="no-margins rating">5</h2>
                                            <small>Рейтинг товара</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" id="rating-bar" style="width: 100%;"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-md">
                            <small>
                                Повышая рейтинг, вы повышаете продажи
                            </small>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-lg-3">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: -40px;">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content table-responsive" id="product">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <table class="table" id="product-main-info" style="margin: -8px 0 -11px;">
                                    <tbody>
                                    <tr>
                                        <td style="border: none;">
                                            <button data-id="13738266" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button>
                                            SKU
                                        </td>
                                        <td style="border: none;" id="sku">13738266</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button data-id="567" data-mp="wildberries" type="category" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button>
                                            Категория
                                        </td>
                                        <td id="category">
                                            <a class="category-link" href="/client/category?id=4006">
                                                <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                Белье
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button data-id="9752" data-mp="wildberries" type="brand" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button>
                                            Бренд
                                        </td>
                                        <td id="brand"><a href="/client/brand?id=71316" title="OMSA" target="_blank">OMSA</a></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button data-id="22104" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button>
                                            Продавец
                                        </td>
                                        <td id="seller"><a href="/client/seller?id=22104" title="ООО " ЕДИНАЯ="" ЕВРОПА-ЭЛИТ"="" (1027739177047)"="" target="_blank">ООО "ЕДИНА...</a></td>
                                    </tr>
                                    <tr>
                                        <td>Цена</td>
                                        <td id="price">1,284&nbsp;руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Скидка</td>
                                        <td id="discount">57&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td>Продажа</td>
                                        <td id="sellPrice">552&nbsp;руб.</td>
                                    </tr>
                                    <tr>
                                        <td>Отзывы</td>
                                        <td id="reviews">19721</td>
                                    </tr>
                                    <tr>
                                        <td>Схема работы</td>
                                        <td id="schema">FBO</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row to-toggle-loader">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3 class="font-bold no-margins">
                            Состояние склада
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-8">
                                    <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                        <canvas style="margin: 0px auto; display: block; width: 483px; height: 200px;" id="chart-amount" height="200" class="chartjs-render-monitor" width="483"></canvas>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul class="stat-list m-t-lg" id="ali-bug">
                                        <li>
                                            <h2 class="no-margins" id="predicted-amount-finish">6 дней</h2>
                                            <small>Осталось до обнуления остатков</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" id="predicted-amount-finish-bar" style="width: 68%"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins" id="predicted-amount-finish-date">14.02.2022</h2>
                                            <small>Рекомендуемая дата поставки</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" id="predicted-amount-finish-date-bar" style="width: 93%"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-md">
                            <small>
                                Обнуление остатков, ведет к пессимизации выдачи
                            </small>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Сравнение с прошлой неделей</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <small class="stats-label">Продаж</small>
                                <h4 id="sales-this-week">-</h4>
                            </div>

                            <div class="col-4">
                                <small class="stats-label">%</small>
                                <h4 id="sales-difference">-</h4>
                            </div>
                            <div class="col-4">
                                <small class="stats-label">Было</small>
                                <h4 id="sales-last-week">-</h4>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <small class="stats-label">Ск. продаж</small>
                                <h4 id="sales-speed-this-week">-</h4>
                            </div>

                            <div class="col-4">
                                <small class="stats-label">%</small>
                                <h4 id="sales-speed-difference">-</h4>
                            </div>
                            <div class="col-4">
                                <small class="stats-label">Было</small>
                                <h4 id="sales-speed-last-week">-</h4>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="row" style="margin-bottom: 32px;">
                            <div class="col-4">
                                <small class="stats-label">Склад</small>
                                <h4 id="amount-this-week">-</h4>
                            </div>

                            <div class="col-4">
                                <small class="stats-label">%</small>
                                <h4 id="amount-diffetence">-</h4>
                            </div>
                            <div class="col-4">
                                <small class="stats-label">Было</small>
                                <h4 id="amount-last-week">-</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row to-toggle-loader" id="product-wharehouses" style="display: none;margin-top: 40px;">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3 class="font-bold no-margins">
                            Динамика продаж по складам
                        </h3>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <canvas style="margin: 0 auto;" id="chart-product-wharehouses" height="200"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-toggle="tab" href="#tab-2">Продажи</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">Наличие</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: -40px;">
                        <div role="tabpanel" id="tab-2" class="tab-pane active">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5>Сводные данные по складам</h5>
                                </div>
                                <div class="ibox-content sk-loading">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row to-toggle-loader" id="product-colors" style="display: none;margin-top: 40px;">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3 class="font-bold no-margins">
                            Динамика продаж по цветам
                        </h3>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <canvas style="margin: 0 auto;" id="chart-product-colors" height="200"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-toggle="tab" href="#tab-2">Продажи</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">Наличие</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: -40px;">
                        <div role="tabpanel" id="tab-3" class="tab-pane active">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5>Сводные данные по цветам</h5>
                                </div>
                                <div class="ibox-content sk-loading">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row to-toggle-loader" id="product-sizes" style="display: none;margin-top: 40px;">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h3 class="font-bold no-margins">
                            Динамика продаж по размерам
                        </h3>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="m-t-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <canvas style="margin: 0 auto;" id="chart-product-sizes" height="200"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-toggle="tab" href="#tab-2">Продажи</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">Наличие</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: -40px;">
                        <div role="tabpanel" id="tab-4" class="tab-pane active">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5>Сводные данные по размерам</h5>
                                </div>
                                <div class="ibox-content sk-loading">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="table-responsive">
                            <table id="model-table" class="table table-striped">
                                <thead>
                                <tr><th>Дата</th>
                                    <th>Позиция</th>
                                    <th>Отзывы</th>
                                    <th>Новые отзывы</th>
                                    <th>Рейтинг</th>
                                    <th>Количество</th>
                                    <th>Цена</th>
                                    <th>Скидка</th>
                                    <th>Цена продажи</th>
                                    <th>Продажи</th>
                                    <th>Выручка</th>
                                </tr></thead>
                                <tbody>
                                <tr>
                                    <td>2022-02-12</td>
                                    <td>1</td>
                                    <td>19721</td>
                                    <td>107</td>
                                    <td>5</td>
                                    <td>16663 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>3034 шт.</td>
                                    <td>1,674,768 руб.</td>
                                </tr>

                                <tr>
                                    <td>2022-02-11</td>
                                    <td>1</td>
                                    <td>19614</td>
                                    <td>94</td>
                                    <td>5</td>
                                    <td>19697 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>2896 шт.</td>
                                    <td>1,598,592 руб.</td>
                                </tr>

                                <tr>
                                    <td>2022-02-10</td>
                                    <td>1</td>
                                    <td>19520</td>
                                    <td>85</td>
                                    <td>5</td>
                                    <td>22593 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>3081 шт.</td>
                                    <td>1,700,712 руб.</td>
                                </tr>

                                <tr>
                                    <td>2022-02-09</td>
                                    <td>1</td>
                                    <td>19435</td>
                                    <td>54</td>
                                    <td>5</td>
                                    <td>25674 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>2938 шт.</td>
                                    <td>1,621,776 руб.</td>
                                </tr>

                                <tr style="-webkit-filter: blur(4px);">
                                    <td>2022-02-08</td>
                                    <td>1</td>
                                    <td>19381</td>
                                    <td>61</td>
                                    <td>5</td>
                                    <td>28612 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>3374 шт.</td>
                                    <td>1,862,448 руб.</td>
                                </tr>

                                <tr style="-webkit-filter: blur(4px);">
                                    <td>2022-02-07</td>
                                    <td>1</td>
                                    <td>19320</td>
                                    <td>59</td>
                                    <td>5</td>
                                    <td>31986 шт.</td>
                                    <td>1,284 руб.</td>
                                    <td>57%</td>
                                    <td>552 руб.</td>
                                    <td>2504 шт.</td>
                                    <td>1,382,208 руб.</td>
                                </tr>

                                <tr style="-webkit-filter: blur(4px);">
                                    <td>2022-02-06</td>
                                    <td>1</td>
                                    <td>19261</td>
                                    <td>60</td>
                                    <td>5</td>
                                    <td>34490 шт.</td>
                                    <td>1,626 руб.</td>
                                    <td>57%</td>
                                    <td>699 руб.</td>
                                    <td>907 шт.</td>
                                    <td>633,993 руб.</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="8"></td>
                                    <td><strong>Итого:</strong></td>
                                    <td>18734 шт.</td>
                                    <td>10,474,497 руб.</td>
                                </tr>
                                </tfoot>



                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <h3>Вам может быть интересно:</h3>
                </div>
            </div>
        </div>
        <div class="row" id="competitors">
            <div class="col-lg-4">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-lg-3" style="padding: 0;">
                                <img style="height: 96px; width: 72px; margin: 0 auto;float: right;" src="https://images.wbstatic.net/c246x328/new/39140000/39148358-1.jpg">
                            </div>

                            <div class="col-lg-9">
                                <a href="/client/product?id=44470315">Носки мужские / OMSA ECO 401/ носки мужские набор - 10 пар / носки длинные / носки высокие</a><br>

                                <a class="category-link" href="/client/category?id=4006">
                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                    Белье
                                </a>

                                <div style="margin-top: 5px;">549&nbsp;руб.
                                    <span class="old-price-percent">(-30%)</span>
                                    <span class="old-price">785&nbsp;руб.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin: 7px 0;">
                                Продавец: <a href="/client/seller?id=22104">ООО "ЕДИНАЯ ЕВРОПА-ЭЛИТ" (1027739177047)</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <strong>Выручка</strong>: 2,384,271&nbsp;руб.
                                <strong>Рейтинг</strong>: 5
                            </div>
                            <div class="col-lg-6">
                                <strong>Продажи</strong>: 4279 шт. <br>
                                <strong>Отзывы</strong>: 19721
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-lg-3" style="padding: 0;">
                                <img style="height: 96px; width: 72px; margin: 0 auto;float: right;" src="https://images.wbstatic.net/c246x328/new/13730000/13738269-1.jpg">
                            </div>

                            <div class="col-lg-9">
                                <a href="/client/product?id=5325338">Носки мужские / OMSA ECO 401 / носки мужские набор - 5 пар / носки длинные / носки высокие / хлопок</a><br>

                                <a class="category-link" href="/client/category?id=4006">
                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                    Белье
                                </a>

                                <div style="margin-top: 5px;">324&nbsp;руб.
                                    <span class="old-price-percent">(-57%)</span>
                                    <span class="old-price">754&nbsp;руб.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin: 7px 0;">
                                Продавец: <a href="/client/seller?id=22104">ООО "ЕДИНАЯ ЕВРОПА-ЭЛИТ" (1027739177047)</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <strong>Выручка</strong>: 1,665,237&nbsp;руб.
                                <strong>Рейтинг</strong>: 5
                            </div>
                            <div class="col-lg-6">
                                <strong>Продажи</strong>: 5038 шт. <br>
                                <strong>Отзывы</strong>: 6264
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-lg-3" style="padding: 0;">
                                <img style="height: 96px; width: 72px; margin: 0 auto;float: right;" src="https://images.wbstatic.net/c246x328/new/12240000/12246403-1.jpg">
                            </div>

                            <div class="col-lg-9">
                                <a href="/client/product?id=5325546">Носки мужские набор 5 пар, высокие, спортивные, черные, теплые, комплект подарок на новый год</a><br>

                                <a class="category-link" href="/client/category?id=4006">
                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                    Белье
                                </a>

                                <div style="margin-top: 5px;">399&nbsp;руб.
                                    <span class="old-price-percent">(-68%)</span>
                                    <span class="old-price">1,267&nbsp;руб.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="margin: 7px 0;">
                                Продавец: <a href="/client/seller?id=26242">ООО "ПАРИТЕТ" (1153668036543)</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <strong>Выручка</strong>: 1,409,667&nbsp;руб.
                                <strong>Рейтинг</strong>: 5
                            </div>
                            <div class="col-lg-6">
                                <strong>Продажи</strong>: 3533 шт. <br>
                                <strong>Отзывы</strong>: 3007
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #modal .modal-dialog {
            max-width: 800px;
        }
    </style>

    <!-- modal about video -->

    <div class="modal inmodal" id="get-sertificate-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Сертифицировать товар</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: center;">
                        Вы можете заказать консультацию по сертификации данного товара через Moneyplace. После оплаты консультации, с вами в порядке очереди свяжется сертификационный менеджер и ответит на все интересующие вопросы.
                    </p>
                    <p style="text-align: center;">При заказе сертификата на товар, стоимость консультации будет вычтена из стоимости</p>
                    <button id="btn-get-cert" style="margin: 0 auto;display: block;" type="button" class="btn btn-success">Заказать консультацию</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="get-stats-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Заказать аналитику</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: center;">
                        Если вы хотите точно определиться с товаром, который стоит заводить на маркетплейс или у вас нет
                        желания заниматься анализом рынка, сегментарным анализом, конкурентной разведкой, финансовой аналитикой
                        и поиском оптовика, оставьте заявку на данный товар и мы сделаем для вас аналитический отчет.
                    </p>
                    <p style="text-align: center;">
                        Все что вам останется - это закупить у поставщика товар и завести его на маркетплейс
                    </p>
                    <p style="text-align: center;">
                        Нажмите оплатить и в течении 3 суток мы подготовим для вас глубокую аналитику по данному
                        товару. Ссылка на аналитический отчет придет на почту, указанную при регистрации вашего аккаунта.
                    </p>
                    <button id="get-payment" style="margin: 0 auto;display: block;" type="button" class="btn btn-success">Оплатить (6000 руб)</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="get-seo-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Для чего нужна SEO оптимизация товарных карточек?</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: center;">
                        1. Во первых, чтобы привлечь на свою карточку больше трафика, благодаря тому, что она начинает выводиться по новым ключевым запросам
                        <br>
                        2. Во вторых, чтобы избавиться от конкуренции и, например, выводиться только по неконкурентным ключам. Вы можете даже выводиться по ключевым
                        запросам, где менее 100 конкурентных товаров - а это Гарантия того, что вы сразу попадете на 1 страницу выдачи по популярности, ведь на 1 странице
                        размещается 100 товаров
                    </p>
                    <p style="text-align: center;">
                        Вы можете заказать у команды Moneyplace грамотно оптимизированный текст, который будет включать: заголовок по выбранному товару, описание,
                        оптимизированные отзыв и вопрос,  оптимизированные ответ на отзыв и ответ на вопрос.
                        Ответы вы сможете использовать в дальнейшем уже на органические отзывы и вопросы
                    </p>
                    <button id="get-seo-btn" style="margin: 0 auto;display: block;" type="button" class="btn btn-success">Заказать SEO оптимизацию</button>
                    <p style="text-align: center;margin-top: 10px;">
                        Стоимость оптимизации 1 карточки 2500 руб, минимальный заказ 5 карточек товара
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="get-credit-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Получите деньги на развитие магазина</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: center;">
                        Своевременное пополнение оборотных средств - это главная необходимость для роста вашего торгового бизнеса. Если вы хотите
                        продолжить быстрый рост, не дожидаясь выплат от маркетплейса или задумываетесь делать закупы большими партиями, чтобы снизить
                        себестоимость товара, оставьте заявку на финансирование своего магазина до 10 000 000 руб.
                    </p>
                    <p style="text-align: center;">
                        В течении нескольких дней мы рассмотрим ваш запрос и свяжемся для обсуждения одобренной суммы и сроков займа
                    </p>
                    <div class="form-group">
                        <label for="credit-sum">Укажите желаемую сумму финансирования</label>
                        <input type="tel" pattern="[0-9\-]+" class="form-control" name="turnover" placeholder="100000" id="credit-sum">
                    </div>
                    <button id="get-credit-btn" style="margin: 0 auto;display: block;" type="button" class="btn btn-success">Отправить заявку на получение средств</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal" id="get-ff-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated flipInY">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Услуга фулфилмента</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: center;">
                        Если вы живете в регионе или у вас отсутствует собственный склад,
                        Вы можете воспользоваться услугами нашего Фулфилмента.
                    </p>
                    <p style="text-align: center;">
                        Это значит, что вы заказываете у поставщика (где бы он не находился) товар,
                        и указываете адрес нашего склада в подмосковье, далее мы примем ваш товар,
                        проверим на брак, упакуем, полностью подготовим к отправке и организуем
                        поставку на маркетплейс
                    </p>
                    <p style="text-align: center;">
                        Благодаря нам, вы сможете создать полностью удаленный бизнес, без складов
                        и сотрудников. Доверьте эту работу профессионалам Маниплейс!
                    </p>
                    <a style="margin: 0 auto;display: block;" href="https://bazaseller.ru/moneyplace" target="_blank" class="btn btn-success">Заказать консультацию по услуге Фулфилмента</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=PROJECT_URL?>client/js/pages/product.js?id=45"></script></div>