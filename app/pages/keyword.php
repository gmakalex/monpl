<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 class="page-name">Ключевое слово: <a href="https://www.wildberries.ru/catalog/0/search.aspx?search=ботинки женские&amp;xsearch=true" target="_blank">ботинки женские</a></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="/client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/client/brands" class="page-name">Ключевое слово: <a href="https://www.wildberries.ru/catalog/0/search.aspx?search=ботинки женские&amp;xsearch=true" target="_blank">ботинки женские</a></a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row to-toggle-loader">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5 id="matche-text">Рекомендации</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="matches">20,277</h1>
                        <div class="stat-percent font-bold text-danger" id="matches-diff">-82% <i class="fa fa-level-down"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Товары</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="total_products_count">92,777</h1>
                        <div class="stat-percent font-bold text-navy" id="total_products_count-diff">3% <i class="fa fa-level-up"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Бренды</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="total_brands_count">2,488</h1>
                        <div class="stat-percent font-bold text-navy" id="total_brands_count-diff">2% <i class="fa fa-level-up"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Категории</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="total_categories_count">2</h1>
                        <div class="stat-percent font-bold text-success" id="total_categories_count-diff">0%</div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector" style="float: left;">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option4" name="period" value="period"> Период </label>
                </div>
                <div class="form-group" style="display: none;float: left;margin-left: 9px;margin-top: 2px;">
                    <input type="text" id="date" name="q[date][between]" class="form-control">
                </div>
            </div>
        </div>

        <diw class="row to-toggle-loader">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Общая динамика по ключевому слову</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 733px; height: 300px;" id="chart-price" height="300" width="733" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <div class="row to-toggle-loader">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Оборот (за последний день)</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="turnover">10,689,248</h1>
                        <div class="stat-percent font-bold text-navy" id="turnover-diff">7% <i class="fa fa-level-up"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Продаж</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="sales">2,775</h1>
                        <div class="stat-percent font-bold text-navy" id="sales-diff">17% <i class="fa fa-level-up"></i></div>
                        <small>Разница с началом периода</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Средний чек</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="bill">4,986.27</h1>
                        <div class="stat-percent font-bold text-danger" id="bill-diff">-3% <i class="fa fa-level-down"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Товары</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="amount">569</h1>
                        <div class="stat-percent font-bold text-navy" id="amount-diff">1% <i class="fa fa-level-up"></i></div>
                        <small>Разница с началом периода:</small>
                    </div>
                </div>
            </div>
        </div>

        <diw class="row to-toggle-loader">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">
                        <span style="display: inline-block;">Проанализированная динамика
                            <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="По ключевым словам анализируются первые
                            500-1000 товаров в поисковой выдаче. Ключей, имеющих более 500 товаров, менее 30%, они высоко-конкурентные,
                            поэтому анализировать весь объем не рационально. (Пример: у ключа 10.000 товаров, вы получаете данные по первым 500-1000
                            товарам, они составляют основную долю рынка по этому ключевому запросу)">?</span></span></h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 733px; height: 300px;" id="chart-analyzed" height="300" width="733" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <diw class="row to-toggle-loader">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Распределение числа продаж от цены на товар</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div>
                            <canvas style="margin: 0px auto; display: block; box-sizing: border-box; width: 733px; height: 400px;" id="chart-prices2" height="400" width="733"></canvas>
                        </div>
                        <div style="margin-left: 65px;">
                            <input type="hidden" id="price-from" value="719">
                            <input type="hidden" id="price-to" value="23050">
                            <div id="slider-range" style="margin-bottom: 15px;" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span></div>
                            <p><strong>Выбранный диапазон цен:</strong> <span id="amount-range">719 руб. - 23050 руб.</span> <button id="apply-prices" type="button" class="btn btn-outline btn-primary">Показать товары</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs nav-tabs-entity" style="float: left;" role="tablist">
                        <li>
                            <a class="nav-link active" data-toggle="tab" data-constructor="htmlConstructor" href="#tab-1">Товары</a>
                        </li>
                        <li>
                            <a class="nav-link" data-toggle="tab" data-type="seller" data-constructor="htmlSellerConstructor" href="#tab-1">Продавцы</a>
                        </li>
                        <li>
                            <a class="nav-link" data-toggle="tab" data-type="brand" data-constructor="htmlBrandConstructor" href="#tab-1">Бренды</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="only-new">
                                        <li><strong>Только новинки</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="is_new" class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <ul class="show-filters">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[turnover][between]">
                                                    <div class="col-lg-5"><strong>Оборот</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_days_in_period][between]">
                                                    <div class="col-lg-4"><strong>Период<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих запись о товаре">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[rating][between]">
                                                    <div class="col-lg-5"><strong>Рейтинг</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[loses][between]">
                                                    <div class="col-lg-5"><strong>Упущенный оброт <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка вызванная обнулением остатков">?</span></strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[days_in_stock][between]">
                                                    <div class="col-lg-4"><strong>Остаток <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих не нулевой остаток">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[reviews][between]">
                                                    <div class="col-lg-5"><strong>Отзывы</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_sales][between]">
                                                    <div class="col-lg-5"><strong>Продажи</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[days_with_sales][between]">
                                                    <div class="col-lg-4"><strong>Продажи <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих продажи">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[position][between]">
                                                    <div class="col-lg-5" style="line-height: 0px;"><strong>Позиция в категории</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Фото</th>
                                            <th>Название</th>
                                            <th>SKU</th>
                                            <th>Продавец</th>
                                            <th>Бренд</th>
                                            <th>Наличие</th>
                                            <th>Цена</th>
                                            <th style="width: 65px;">ACP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Средняя позиция в категории">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="loses">LP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка, вызванная обнулением остатков">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="total_sales">Продажи</th>
                                            <th class="sorting" style="min-width: 95px;" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th>Тренд</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>
                                                <a href="" data-id="58322014" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="41104970">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/41100000/41104970-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=58322014">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(54)</span>
                                                </div>
                                            </td>
                                            <td>41104970</td>
                                            <td><a href="/client/seller?id=202818">ИП Сысоева Ольга Станиславовна (316774600272987)</a></td>
                                            <td><a href="/client/brand?id=73192">O`SHADE</a></td>
                                            <td>1</td>
                                            <td>
                                                5,606&nbsp;руб. <br>

                                                <span class="old-price-percent">(-48%)</span>
                                                <span class="old-price">10,890&nbsp;руб.</span>

                                            </td>
                                            <td>448</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>186</td>
                                            <td>878,934&nbsp;руб.</td>
                                            <td><span id="sparkline58322014"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="41104970" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="72141519" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="49281122">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/49280000/49281122-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=72141519">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=7771">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Спорт / Спортивная обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(110)</span>
                                                </div>
                                            </td>
                                            <td>49281122</td>
                                            <td><a href="/client/seller?id=83865">ИП Ерёменко Иван Сергеевич (317774600061872)</a></td>
                                            <td><a href="/client/brand?id=227748">4STORIES</a></td>
                                            <td>782</td>
                                            <td>
                                                3,404&nbsp;руб. <br>

                                                <span class="old-price-percent">(-28%)</span>
                                                <span class="old-price">4,776&nbsp;руб.</span>

                                            </td>
                                            <td>61</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>209</td>
                                            <td>658,112&nbsp;руб.</td>
                                            <td><span id="sparkline72141519"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="49281122" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="583884262" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="52104284">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/52100000/52104284-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=583884262">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(80)</span>
                                                </div>
                                            </td>
                                            <td>52104284</td>
                                            <td><a href="/client/seller?id=27193">ИП Пириев Махир Ильхам Оглы (318774600058288)</a></td>
                                            <td><a href="/client/brand?id=73833">DOLCE FABIANA</a></td>
                                            <td>0</td>
                                            <td>
                                                2,392&nbsp;руб. <br>

                                                <span class="old-price-percent">(-54%)</span>
                                                <span class="old-price">5,300&nbsp;руб.</span>

                                            </td>
                                            <td>206</td>
                                            <td>105,375.33&nbsp;руб.</td>
                                            <td>263</td>
                                            <td>632,192&nbsp;руб.</td>
                                            <td><span id="sparkline583884262"><canvas width="29" height="40" style="display: inline-block; width: 29px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="52104284" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="583885380" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="57213738">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/57210000/57213738-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=583885380">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(55)</span>
                                                </div>
                                            </td>
                                            <td>57213738</td>
                                            <td><a href="/client/seller?id=27193">ИП Пириев Махир Ильхам Оглы (318774600058288)</a></td>
                                            <td><a href="/client/brand?id=73833">DOLCE FABIANA</a></td>
                                            <td>83</td>
                                            <td>
                                                2,035&nbsp;руб. <br>

                                                <span class="old-price-percent">(-61%)</span>
                                                <span class="old-price">5,300&nbsp;руб.</span>

                                            </td>
                                            <td>357</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>286</td>
                                            <td>628,354&nbsp;руб.</td>
                                            <td><span id="sparkline583885380"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="57213738" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="43520457" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="38301592">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/38300000/38301592-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=43520457">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(114)</span>
                                                </div>
                                            </td>
                                            <td>38301592</td>
                                            <td><a href="/client/seller?id=202818">ИП Сысоева Ольга Станиславовна (316774600272987)</a></td>
                                            <td><a href="/client/brand?id=73192">O`SHADE</a></td>
                                            <td>119</td>
                                            <td>
                                                4,554&nbsp;руб. <br>

                                                <span class="old-price-percent">(-60%)</span>
                                                <span class="old-price">11,500&nbsp;руб.</span>

                                            </td>
                                            <td>1375</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>127</td>
                                            <td>580,382&nbsp;руб.</td>
                                            <td><span id="sparkline43520457"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="38301592" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="583884143" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="55257404">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/55250000/55257404-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=583884143">Ботинки женские натуральная кожа/ бежевые/ на шнуровке/ на платформе/ демисезонные/ весенние</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(50)</span>
                                                </div>
                                            </td>
                                            <td>55257404</td>
                                            <td><a href="/client/seller?id=112102">ИП Варнавская Наталия Владимировна (321519000006092)</a></td>
                                            <td><a href="/client/brand?id=72974">YOURBOX</a></td>
                                            <td>3</td>
                                            <td>
                                                8,047&nbsp;руб. <br>

                                                <span class="old-price-percent">(-25%)</span>
                                                <span class="old-price">10,730&nbsp;руб.</span>

                                            </td>
                                            <td>219</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>72</td>
                                            <td>577,544&nbsp;руб.</td>
                                            <td><span id="sparkline583884143"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="55257404" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="41551500" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="36923816">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/36920000/36923816-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=41551500">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(109)</span>
                                                </div>
                                            </td>
                                            <td>36923816</td>
                                            <td><a href="/client/seller?id=26870">ООО "МОСКОВСКАЯ ОБУВНАЯ ФАБРИКА" (1157746819768)</a></td>
                                            <td><a href="/client/brand?id=72760">BERG</a></td>
                                            <td>4</td>
                                            <td>
                                                7,243&nbsp;руб. <br>

                                                <span class="old-price-percent">(-50%)</span>
                                                <span class="old-price">14,500&nbsp;руб.</span>

                                            </td>
                                            <td>471</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>76</td>
                                            <td>550,833&nbsp;руб.</td>
                                            <td><span id="sparkline41551500"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="36923816" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="16118163" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="18281169">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/18280000/18281169-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=16118163">Ботинки/Челси/Натуральная кожа/Зимние</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(169)</span>
                                                </div>
                                            </td>
                                            <td>18281169</td>
                                            <td><a href="/client/seller?id=60741">ИП Лаврентьев Никита Александрович (320774600455630)</a></td>
                                            <td><a href="/client/brand?id=73931">MODESCO</a></td>
                                            <td>95</td>
                                            <td>
                                                5,454&nbsp;руб. <br>

                                                <span class="old-price-percent">(-42%)</span>
                                                <span class="old-price">9,500&nbsp;руб.</span>

                                            </td>
                                            <td>10</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>101</td>
                                            <td>549,767&nbsp;руб.</td>
                                            <td><span id="sparkline16118163"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="18281169" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="4683793" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="13455755">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13450000/13455755-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=4683793">Ботинки/ Тимберленды / Зима / Тренд</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(861)</span>
                                                </div>
                                            </td>
                                            <td>13455755</td>
                                            <td><a href="/client/seller?id=26903">ИП Самбикин Евгений Игоревич (304502919700028)</a></td>
                                            <td><a href="/client/brand?id=72809">Dina Grata</a></td>
                                            <td>21</td>
                                            <td>
                                                5,398&nbsp;руб. <br>

                                                <span class="old-price-percent">(-30%)</span>
                                                <span class="old-price">7,790&nbsp;руб.</span>

                                            </td>
                                            <td>25</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>100</td>
                                            <td>534,425&nbsp;руб.</td>
                                            <td><span id="sparkline4683793"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13455755" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <a href="" data-id="18916049" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="15728055">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/15720000/15728055-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=18916049">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(262)</span>
                                                </div>
                                            </td>
                                            <td>15728055</td>
                                            <td><a href="/client/seller?id=18533">ООО "ТВОЕ" (1107746597782)</a></td>
                                            <td><a href="/client/brand?id=56392">ТВОЕ</a></td>
                                            <td>501</td>
                                            <td>
                                                929&nbsp;руб. <br>

                                                <span class="old-price-percent">(-69%)</span>
                                                <span class="old-price">2,999&nbsp;руб.</span>

                                            </td>
                                            <td>339</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>563</td>
                                            <td>523,027&nbsp;руб.</td>
                                            <td><span id="sparkline18916049"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="15728055" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="44308091" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="38615934">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/38610000/38615934-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=44308091">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(114)</span>
                                                </div>
                                            </td>
                                            <td>38615934</td>
                                            <td><a href="/client/seller?id=202818">ИП Сысоева Ольга Станиславовна (316774600272987)</a></td>
                                            <td><a href="/client/brand?id=73192">O`SHADE</a></td>
                                            <td>10</td>
                                            <td>
                                                4,667&nbsp;руб. <br>

                                                <span class="old-price-percent">(-59%)</span>
                                                <span class="old-price">11,500&nbsp;руб.</span>

                                            </td>
                                            <td>680</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>111</td>
                                            <td>518,887&nbsp;руб.</td>
                                            <td><span id="sparkline44308091"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="38615934" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="42920032" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="36890595">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/36890000/36890595-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=42920032">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(312)</span>
                                                </div>
                                            </td>
                                            <td>36890595</td>
                                            <td><a href="/client/seller?id=69982">ИП Вячина Наталия Александровна (320508100331402)</a></td>
                                            <td><a href="/client/brand?id=73833">DOLCE FABIANA</a></td>
                                            <td>376</td>
                                            <td>
                                                2,229&nbsp;руб. <br>

                                                <span class="old-price-percent">(-64%)</span>
                                                <span class="old-price">6,300&nbsp;руб.</span>

                                            </td>
                                            <td>276</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>228</td>
                                            <td>509,822&nbsp;руб.</td>
                                            <td><span id="sparkline42920032"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="36890595" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="12008883" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="17180445">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/17180000/17180445-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=12008883">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(249)</span>
                                                </div>
                                            </td>
                                            <td>17180445</td>
                                            <td><a href="/client/seller?id=26870">ООО "МОСКОВСКАЯ ОБУВНАЯ ФАБРИКА" (1157746819768)</a></td>
                                            <td><a href="/client/brand?id=72760">BERG</a></td>
                                            <td>309</td>
                                            <td>
                                                5,627&nbsp;руб. <br>

                                                <span class="old-price-percent">(-59%)</span>
                                                <span class="old-price">14,000&nbsp;руб.</span>

                                            </td>
                                            <td>73</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>86</td>
                                            <td>490,852&nbsp;руб.</td>
                                            <td><span id="sparkline12008883"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="17180445" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="116933962" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="50132660">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/50130000/50132660-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=116933962">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(59)</span>
                                                </div>
                                            </td>
                                            <td>50132660</td>
                                            <td><a href="/client/seller?id=27331">ИП Дорощенко Сергей Олегович (320774600245992)</a></td>
                                            <td><a href="/client/brand?id=73833">DOLCE FABIANA</a></td>
                                            <td>438</td>
                                            <td>
                                                2,524&nbsp;руб. <br>

                                                <span class="old-price-percent">(-70%)</span>
                                                <span class="old-price">8,500&nbsp;руб.</span>

                                            </td>
                                            <td>143</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>185</td>
                                            <td>469,358&nbsp;руб.</td>
                                            <td><span id="sparkline116933962"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="50132660" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="42540488" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="37504334">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/37500000/37504334-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=42540488">Ботинки женские / Челси /</a><br>

                                                <a class="category-link" href="/client/category?id=5011">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботильоны
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(199)</span>
                                                </div>
                                            </td>
                                            <td>37504334</td>
                                            <td><a href="/client/seller?id=18875">ИП Высоких Юлия Владимировна (319325600016703)</a></td>
                                            <td><a href="/client/brand?id=73255">LUmeiTA</a></td>
                                            <td>1</td>
                                            <td>
                                                2,698&nbsp;руб. <br>

                                                <span class="old-price-percent">(-61%)</span>
                                                <span class="old-price">6,990&nbsp;руб.</span>

                                            </td>
                                            <td>21084</td>
                                            <td>862,044.03&nbsp;руб.</td>
                                            <td>143</td>
                                            <td>461,537&nbsp;руб.</td>
                                            <td><span id="sparkline42540488"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="37504334" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="9617806" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="11152183">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/11150000/11152183-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9617806">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(287)</span>
                                                </div>
                                            </td>
                                            <td>11152183</td>
                                            <td><a href="/client/seller?id=26894">ИП Сысоев Андрей Вадимович (315774600378101)</a></td>
                                            <td><a href="/client/brand?id=73192">O`SHADE</a></td>
                                            <td>2</td>
                                            <td>
                                                4,615&nbsp;руб. <br>

                                                <span class="old-price-percent">(-37%)</span>
                                                <span class="old-price">7,400&nbsp;руб.</span>

                                            </td>
                                            <td>622</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>102</td>
                                            <td>457,638&nbsp;руб.</td>
                                            <td><span id="sparkline9617806"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="11152183" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="43520818" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="38281094">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/38280000/38281094-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=43520818">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(75)</span>
                                                </div>
                                            </td>
                                            <td>38281094</td>
                                            <td><a href="/client/seller?id=202818">ИП Сысоева Ольга Станиславовна (316774600272987)</a></td>
                                            <td><a href="/client/brand?id=73192">O`SHADE</a></td>
                                            <td>48</td>
                                            <td>
                                                4,554&nbsp;руб. <br>

                                                <span class="old-price-percent">(-50%)</span>
                                                <span class="old-price">9,200&nbsp;руб.</span>

                                            </td>
                                            <td>492</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>100</td>
                                            <td>454,894&nbsp;руб.</td>
                                            <td><span id="sparkline43520818"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="38281094" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="4726113" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="9087357">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/9080000/9087357-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=4726113">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(605)</span>
                                                </div>
                                            </td>
                                            <td>9087357</td>
                                            <td><a href="/client/seller?id=26870">ООО "МОСКОВСКАЯ ОБУВНАЯ ФАБРИКА" (1157746819768)</a></td>
                                            <td><a href="/client/brand?id=72760">BERG</a></td>
                                            <td>108</td>
                                            <td>
                                                5,879&nbsp;руб. <br>

                                                <span class="old-price-percent">(-63%)</span>
                                                <span class="old-price">16,000&nbsp;руб.</span>

                                            </td>
                                            <td>3500</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>74</td>
                                            <td>434,346&nbsp;руб.</td>
                                            <td><span id="sparkline4726113"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="9087357" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="583884120" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="54573730">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/54570000/54573730-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=583884120">Ботинки женские натуральная кожа 100%/ высокие/ на молнии/ на шнуровке/ демисезонные/ весенние</a><br>

                                                <a class="category-link" href="/client/category?id=3561">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Женщинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(17)</span>
                                                </div>
                                            </td>
                                            <td>54573730</td>
                                            <td><a href="/client/seller?id=266886">ИП Вейшнер Владимир Андреевич (314784702400308)</a></td>
                                            <td><a href="/client/brand?id=72974">YOURBOX</a></td>
                                            <td>16</td>
                                            <td>
                                                8,050&nbsp;руб. <br>

                                                <span class="old-price-percent">(-30%)</span>
                                                <span class="old-price">11,500&nbsp;руб.</span>

                                            </td>
                                            <td>279</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>53</td>
                                            <td>426,650&nbsp;руб.</td>
                                            <td><span id="sparkline583884120"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="54573730" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <a href="" data-id="42920331" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input style="margin-top: 10px;" data-mp="wildberries" type="checkbox" value="36792135">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/36790000/36792135-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=42920331">Ботинки</a><br>

                                                <a class="category-link" href="/client/category?id=5012">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Обувь / Женская / Ботинки и полуботинки
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(291)</span>
                                                </div>
                                            </td>
                                            <td>36792135</td>
                                            <td><a href="/client/seller?id=27193">ИП Пириев Махир Ильхам Оглы (318774600058288)</a></td>
                                            <td><a href="/client/brand?id=73833">DOLCE FABIANA</a></td>
                                            <td>245</td>
                                            <td>
                                                2,226&nbsp;руб. <br>

                                                <span class="old-price-percent">(-64%)</span>
                                                <span class="old-price">6,300&nbsp;руб.</span>

                                            </td>
                                            <td>152</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>191</td>
                                            <td>426,431&nbsp;руб.</td>
                                            <td><span id="sparkline42920331"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="36792135" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (783)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #modal .modal-dialog {
            max-width: 900px;
        }

    </style>
    <script src="<?=PROJECT_URL?>client/js/pages/keyword.js?id=19"></script>
</div>