<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Продавцы</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Продавцы</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div id="get-extra-days" class="ibox-content" style="display: none;">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
        <div class="row">
            <h2>Пройдите обучение и получите +6 дней премиум доступа в подарок!</h2>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="extra-video active" data-days="1" data-lesson="40129654" data-step="0" data-video-id="lw9KLm1C0mU">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 1</p>
                        <span>Обратный поиск товаров</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play active"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129657" data-step="1" data-video-id="pIPzI2KxCcA">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 2</p>
                        <span>Анализ ниш на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129669" data-step="2" data-video-id="gz8dw0Sfx3s">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 3</p>
                        <span>Выявление хайповых товаров на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129672" data-step="3" data-video-id="hjpdOSwQ0es">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 4</p>
                        <span>Аналитика категорий на маркетплейсах</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="1" data-lesson="40129675" data-step="4" data-video-id="9a9F3tqpJoo">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 5</p>
                        <span>Конкурентный анализ</span>
                        <div class="extra-video-label">Урок не пройден</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
                <div class="col">
                    <div class="extra-video" data-days="6" data-lesson="40193338" data-step="5" data-video-id="ZRc2Bwwwl18">
                        <img src="/client/img/extra-days-preview.png" alt="">
                        <p>Урок 6</p>
                        <span>API кабинет и аналитика собственного магазина</span>
                        <div class="extra-video-label"><img src="/client/img/video-gift.svg" alt=""> + 6 дней после прохождения!</div>
                        <div class="extra-video-play"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="close-extra-video"><img src="/client/img/video-close.svg" alt=""></div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: left" role="tablist">
                        <li data-original-title="" title="">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">Все</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-field="mp" data-field-value="wildberries" data-toggle="tab" href="#tab-1">Wildberries</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ozon" href="#tab-1">Ozon</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ali" href="#tab-1">AliExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="beru" href="#tab-1">Я.Маркет</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="kazan" href="#tab-1">KazanExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="sber" href="#tab-1">СберМегаМаркет</a>
                        </li>
                    </ul>
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active show">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="only-new">
                                        <li><strong>Только новинки</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="is_new" class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <ul class="show-filters">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[turnover][between]">
                                                    <div class="col-lg-5"><strong>Оборот</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[rating][between]">
                                                    <div class="col-lg-4"><strong>Рейтинг</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[average_bill][between]">
                                                    <div class="col-lg-5"><strong>Средний чек</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_sales][between]">
                                                    <div class="col-lg-5"><strong>Продажи</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[products_count][between]">
                                                    <div class="col-lg-4"><strong>Товаров</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Продавец</th>
                                            <th>Товаров</th>
                                            <th>Брендов</th>
                                            <th>Рейтинг</th>
                                            <th>Средний чек</th>
                                            <th class="sorting" data-sort="total_sales">Продажи</th>
                                            <th class="sorting" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18545">ВАЙЛДБЕРРИЗ ООО (1067746062449)</a>
                                            </td>
                                            <td>657002</td>
                                            <td>19787</td>
                                            <td>4.1</td>
                                            <td>1,256&nbsp;руб.</td>
                                            <td>2799428</td>
                                            <td>3,515,432,993&nbsp;руб.</td>
                                            <td><button data-id="18545" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/seller?id=2">OZON</a>
                                            </td>
                                            <td>158772</td>
                                            <td>6906</td>
                                            <td>4.7</td>
                                            <td>788&nbsp;руб.</td>
                                            <td>2451869</td>
                                            <td>1,931,627,756&nbsp;руб.</td>
                                            <td><button data-id="0" data-mp="ozon" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=22420">МЕЙДЕНЛИ ООО (1616022807)</a>
                                            </td>
                                            <td>708061</td>
                                            <td>79951</td>
                                            <td>4.4</td>
                                            <td>971&nbsp;руб.</td>
                                            <td>1772962</td>
                                            <td>1,721,008,167&nbsp;руб.</td>
                                            <td><button data-id="22420" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/seller?id=309925">Яндекс.Маркет</a>
                                            </td>
                                            <td>66148</td>
                                            <td>2519</td>
                                            <td>4.6</td>
                                            <td>1,733&nbsp;руб.</td>
                                            <td>302081</td>
                                            <td>523,480,730&nbsp;руб.</td>
                                            <td><button data-id="924574" data-mp="beru" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18542">Акционерное общество "МЭЛОН ФЭШН ГРУП" (1057813298553)</a>
                                            </td>
                                            <td>16582</td>
                                            <td>4</td>
                                            <td>4.4</td>
                                            <td>1,383&nbsp;руб.</td>
                                            <td>171618</td>
                                            <td>237,426,063&nbsp;руб.</td>
                                            <td><button data-id="18542" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18743">Общество с ограниченной ответственностью "КАРИ" (1117746491500)</a>
                                            </td>
                                            <td>19807</td>
                                            <td>75</td>
                                            <td>4.5</td>
                                            <td>1,765&nbsp;руб.</td>
                                            <td>72012</td>
                                            <td>127,133,741&nbsp;руб.</td>
                                            <td><button data-id="18743" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18533">ООО "ТВОЕ" (1107746597782)</a>
                                            </td>
                                            <td>7919</td>
                                            <td>1</td>
                                            <td>4.6</td>
                                            <td>629&nbsp;руб.</td>
                                            <td>193749</td>
                                            <td>121,842,434&nbsp;руб.</td>
                                            <td><button data-id="18533" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=84533">ИП Пониматкин Сергей Александрович (304130816900170)</a>
                                            </td>
                                            <td>3887</td>
                                            <td>25</td>
                                            <td>4.1</td>
                                            <td>14,653&nbsp;руб.</td>
                                            <td>4503</td>
                                            <td>65,981,007&nbsp;руб.</td>
                                            <td><button data-id="84533" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=26865">Общество с ограниченной ответственностью "ЭККО-РОС" (1025005330404)</a>
                                            </td>
                                            <td>1359</td>
                                            <td>1</td>
                                            <td>4.3</td>
                                            <td>7,715&nbsp;руб.</td>
                                            <td>8514</td>
                                            <td>65,683,939&nbsp;руб.</td>
                                            <td><button data-id="26865" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=176255">ИП Гришков Виктор Олегович (320774600398006)</a>
                                            </td>
                                            <td>683</td>
                                            <td>21</td>
                                            <td>3.9</td>
                                            <td>27,579&nbsp;руб.</td>
                                            <td>2334</td>
                                            <td>64,370,493&nbsp;руб.</td>
                                            <td><button data-id="176255" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/seller?id=45744">Официальный магазин Tmall - Техника</a>
                                            </td>
                                            <td>784</td>
                                            <td>1</td>
                                            <td>4.7</td>
                                            <td>8,524&nbsp;руб.</td>
                                            <td>7279</td>
                                            <td>62,045,584&nbsp;руб.</td>
                                            <td><button data-id="2792104" data-mp="ali" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=28808">ООО "ОФИСМАГ-ПОВОЛЖЬЕ" (1023404239540)</a>
                                            </td>
                                            <td>8298</td>
                                            <td>125</td>
                                            <td>4.5</td>
                                            <td>424&nbsp;руб.</td>
                                            <td>141673</td>
                                            <td>60,063,167&nbsp;руб.</td>
                                            <td><button data-id="28808" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18781">ООО "АВГУСТ" (1037843082100)</a>
                                            </td>
                                            <td>10115</td>
                                            <td>3</td>
                                            <td>4.3</td>
                                            <td>642&nbsp;руб.</td>
                                            <td>93036</td>
                                            <td>59,715,889&nbsp;руб.</td>
                                            <td><button data-id="18781" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=31565">ООО "МЕГАПРОФСТИЛЬ" (5167746237148)</a>
                                            </td>
                                            <td>5513</td>
                                            <td>72</td>
                                            <td>4.2</td>
                                            <td>521&nbsp;руб.</td>
                                            <td>107844</td>
                                            <td>56,234,426&nbsp;руб.</td>
                                            <td><button data-id="31565" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18741">Общество с ограниченной ответственностью "ХЬЮГО БОСС РУС" (1127747005451)</a>
                                            </td>
                                            <td>2217</td>
                                            <td>4</td>
                                            <td>4.1</td>
                                            <td>7,886&nbsp;руб.</td>
                                            <td>6891</td>
                                            <td>54,341,844&nbsp;руб.</td>
                                            <td><button data-id="18741" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18557">Общество с ограниченной ответственностью "КОНЦЕПТ ГРУП" (1097847317721)</a>
                                            </td>
                                            <td>5269</td>
                                            <td>6</td>
                                            <td>4.4</td>
                                            <td>597&nbsp;руб.</td>
                                            <td>85733</td>
                                            <td>51,182,957&nbsp;руб.</td>
                                            <td><button data-id="18557" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=30683">ТД Эксмо ООО (1037789006034)</a>
                                            </td>
                                            <td>19707</td>
                                            <td>11</td>
                                            <td>4.6</td>
                                            <td>477&nbsp;руб.</td>
                                            <td>101384</td>
                                            <td>48,367,661&nbsp;руб.</td>
                                            <td><button data-id="30683" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=19590">ООО "ТК ДТ" (1197746321145)</a>
                                            </td>
                                            <td>2925</td>
                                            <td>11</td>
                                            <td>4.7</td>
                                            <td>1,144&nbsp;руб.</td>
                                            <td>40519</td>
                                            <td>46,365,625&nbsp;руб.</td>
                                            <td><button data-id="19590" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=18879">АО "ГЛОРИЯ ДЖИНС" (1026104024737)</a>
                                            </td>
                                            <td>5207</td>
                                            <td>2</td>
                                            <td>4.6</td>
                                            <td>583&nbsp;руб.</td>
                                            <td>76893</td>
                                            <td>44,800,353&nbsp;руб.</td>
                                            <td><button data-id="18879" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/seller?id=22104">ООО "ЕДИНАЯ ЕВРОПА-ЭЛИТ" (1027739177047)</a>
                                            </td>
                                            <td>4729</td>
                                            <td>8</td>
                                            <td>4.3</td>
                                            <td>444&nbsp;руб.</td>
                                            <td>99696</td>
                                            <td>44,215,538&nbsp;руб.</td>
                                            <td><button data-id="22104" data-mp="wildberries" type="seller" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (256787)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=PROJECT_URL?>client/js/pages/sellers.js?id=7"></script></div>