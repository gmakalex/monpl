<div id="page-content" style="">

    <div class="wrapper wrapper-content dashboard" id="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title">
                    <ul class="main-title-shops-list">
                        <li class="main-title-shops-list-item"><img src="/client/img/price-wb.svg" alt=""></li>
                        <li class="main-title-shops-list-item"><img src="/client/img/price-ozon.svg" alt=""></li>
                        <li class="main-title-shops-list-item"><img src="/client/img/price-aliexpress.svg" alt=""></li>
                    </ul>
                    <h2 class="main-title-value">Анализируйте конкурентов и находите прибыльные ниши на всех маркетплейсах</h2>
                    <img class="main-title-img" src="img/main-title-img.png" alt="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="main-leader-tabs">
                    <h3 class="main-leader-title">Лидеры по количеству продаж за 7 дней</h3>
                    <ul class="main-leader-tabs-nav">
                        <li class="main-leader-tabs-nav-item active">Товары</li>
                        <li class="main-leader-tabs-nav-item">Категории</li>
                        <li class="main-leader-tabs-nav-item">Продавцы</li>
                    </ul>
                    <div class="main-leader-tabs-content active" id="product">


                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://images.wbstatic.net/c246x328/new/13730000/13738266-1.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=5325333" class="product-item-name">Носки мужские / OMSA ECO 401/ носки мужские набор - 10 пар / носки длинные / носки высокие</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">552 руб.</div>
                                        <div class="product-item-cost-old">1,284 руб.</div>
                                        <div class="product-item-cost-discount">(-57%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=22104">ООО "ЕДИНАЯ ЕВРОПА-ЭЛИТ" (1027739177047)</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=4006">Белье</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/wildberries.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3 active">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            10,474,497 р.
                                        </div>
                                        <span class="sparkline5325333"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">18734 шт</div>
                                        <span class="sparkline5325333"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">19721 отзывов</div>
                                        <div class="product-item-reviews-rating">5 </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://nowatermark.ozone.ru/s3/multimedia-v/6069236779.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=37003999" class="product-item-name">Робот-пылесос  Roborock  Xiaomi S7, Русская версия, белый</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">42,990 руб.</div>
                                        <div class="product-item-cost-old">49,990 руб.</div>
                                        <div class="product-item-cost-discount">(-14%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=1133">ООО Ламобайл (1185027020947)</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=2814">Роботы-пылесосы</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ozon.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            3,207,280 р.
                                        </div>
                                        <span class="sparkline37003999"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">72 шт</div>
                                        <span class="sparkline37003999"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">1311 отзывов</div>
                                        <div class="product-item-reviews-rating">5 </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://ae04.alicdn.com/kf/Hd916e9d1d2cf47d2b77b311c9f29ec35F/ROIDMI-EVE-Plus-Robot-Vacuum-Cleaner-Smart-Home-APP-Control-Assistant-Alexa-Mi-Home-Floor-Cleaning.jpg_220x220xz.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=39620504" class="product-item-name">29999-2600₽ промо код 【UHWOW2600】ROIDMI ЕВА плюс робот-пылесос Smart Home приложение Contron По...</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">31,204 руб.</div>
                                        <div class="product-item-cost-old">42,168 руб.</div>
                                        <div class="product-item-cost-discount">(-26%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=187184">DreamMall Store</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=15426">Роботы-пылесосы</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ali.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            5,562,470 р.
                                        </div>
                                        <span class="sparkline39620504"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">178 шт</div>
                                        <span class="sparkline39620504"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">0 отзывов</div>
                                        <div class="product-item-reviews-rating">4.8 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-leader-tabs-content" id="category">



                        <div class="product-item product-item-category">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <div class="product-item-category-title">Категория: <a href="/client/category?id=3195">Женщинам</a></div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего продавцов:</div>
                                        <div class="product-item-info-item-value">49,934</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">2,223,142</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">70,128</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/wildberries.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">8,656,536,276 р.</div>
                                        <span class="sparkline3195"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">4,925,064 шт</div>
                                        <span class="sparkline3195"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">1,757.6 р.</div>
                                        <span class="sparkline3195"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="product-item product-item-category">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <div class="product-item-category-title">Категория: <a href="/client/category?id=1">Электроника</a></div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего продавцов:</div>
                                        <div class="product-item-info-item-value">10,493</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">160,628</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">7,868</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ozon.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">1,039,809,362 р.</div>
                                        <span class="sparkline1"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">666,604 шт</div>
                                        <span class="sparkline1"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">1,559.8 р.</div>
                                        <span class="sparkline1"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="product-item product-item-category">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <div class="product-item-category-title">Категория: <a href="/client/category?id=14136">Электроника</a></div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего продавцов:</div>
                                        <div class="product-item-info-item-value">2,158</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">36,983</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">1</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ali.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">185,268,899 р.</div>
                                        <span class="sparkline14136"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">66,927 шт</div>
                                        <span class="sparkline14136"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">2,768.2 р.</div>
                                        <span class="sparkline14136"><canvas width="34" height="60" style="display: inline-block; width: 34px; height: 60px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-leader-tabs-content" id="seller">



                        <div class="product-item product-item-sellers">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <a href="#" class="product-item-name"></a><a href="/client/seller?id=22420">МЕЙДЕНЛИ ООО (1616022807)</a>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">708,061</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">79,951</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/wildberries.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">1,721,008,167 р.</div>
                                        <span class="sparkline22420"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">1,772,962 шт</div>
                                        <span class="sparkline22420"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">970.6 р.</div>
                                        <span class="sparkline22420"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="product-item product-item-sellers">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <a href="#" class="product-item-name"></a><a href="/client/seller?id=121978">ООО Торговый Дом Эксмо (1037789006034)</a>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">7,912</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">3</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ozon.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">37,507,908 р.</div>
                                        <span class="sparkline121978"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">73,050 шт</div>
                                        <span class="sparkline121978"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">513.4 р.</div>
                                        <span class="sparkline121978"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="product-item product-item-sellers">
                            <div class="product-item-left">
                                <div class="product-item-info">
                                    <a href="#" class="product-item-name"></a><a href="/client/seller?id=45744">Официальный магазин Tmall - Техника</a>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего товаров:</div>
                                        <div class="product-item-info-item-value">784</div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Всего брендов:</div>
                                        <div class="product-item-info-item-value">1</div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ali.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Ср. чек</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">62,045,584 р.</div>
                                        <span class="sparkline45744"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">7,279 шт</div>
                                        <span class="sparkline45744"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-check">
                                        <div class="product-item-check-value">8,523.9 р.</div>
                                        <span class="sparkline45744"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="main-new">
                    <h3 class="main-new-title">Новинки за 7 дней</h3>
                    <div id="product-new">
                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://images.wbstatic.net/c246x328/new/58250000/58250708-1.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=652810513" class="product-item-name">Увлажняющий гель-крем для душа/пена для ванны без парабенов 2в1, 750 мл, Испания, женский/мужск...</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">243 руб.</div>
                                        <div class="product-item-cost-old">609 руб.</div>
                                        <div class="product-item-cost-discount">(-60%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=37079">ООО "УБТ РУС" (1177746426120)</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=7793">Красота</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/wildberries.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            140,697 р.
                                        </div>
                                        <span class="sparkline652810513"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">579 шт</div>
                                        <span class="sparkline652810513"><canvas width="34" height="50" style="display: inline-block; width: 34px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">126 отзывов</div>
                                        <div class="product-item-reviews-rating">5 </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://cdn1.ozone.ru/s3/multimedia-p/6196341109.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=696497662" class="product-item-name">Lavazza Qualita Oro 1 кг. Кофе в зернах</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">999 руб.</div>
                                        <div class="product-item-cost-old">1,975 руб.</div>
                                        <div class="product-item-cost-discount">(-49%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=442678">ООО АЛЬФАКЛИК (1215400052273)</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=895">Кофе</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ozon.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            655,344 р.
                                        </div>
                                        <span class="sparkline696497662"><canvas width="24" height="50" style="display: inline-block; width: 24px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">656 шт</div>
                                        <span class="sparkline696497662"><canvas width="24" height="50" style="display: inline-block; width: 24px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">822 отзывов</div>
                                        <div class="product-item-reviews-rating">4.8 </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-item">
                            <div class="product-item-left">
                                <div class="product-item-img">
                                    <img src="https://ae04.alicdn.com/kf/U75399b4bea0049b897bc981e46b72c66g/Apple-20W-USB-C-power-adapter-20w-power-adapter-mhje3zm-A-.jpg_220x220xz.jpg">
                                </div>
                                <div class="product-item-info">
                                    <a href="/client/product?id=704859712" class="product-item-name">Адаптер питания Apple 20W USB-C Power Adapter мощностью 20 Вт (MHJE3ZM/A)</a>
                                    <div class="product-item-cost-wrapper">
                                        <div class="product-item-cost">1,592 руб.</div>
                                        <div class="product-item-cost-old">1,990 руб.</div>
                                        <div class="product-item-cost-discount">(-20%)</div>

                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Продавец:</div>
                                        <div class="product-item-info-item-value"><a href="/client/seller?id=45744">Официальный магазин Tmall - Техника</a></div>
                                    </div>
                                    <div class="product-item-info-item">
                                        <div class="product-item-info-item-title">Категория:</div>
                                        <div class="product-item-info-item-value"><a class="category-link" href="/client/category?id=13230">Зарядные устройства</a></div>
                                    </div>
                                </div>
                                <img class="product-item-shop" src="/client/img/ali.svg" alt="">
                            </div>
                            <div class="product-item-tabs">
                                <ul class="product-item-tabs-nav">
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-1 active">Выручка</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-2">Продажи</li>
                                    <li class="product-item-tabs-nav-item product-item-tabs-nav-item-3">Отзывы</li>
                                </ul>
                                <div class="product-item-tabs-content active">
                                    <div class="product-item-revenue">
                                        <div class="product-item-revenue-value">
                                            222,880 р.
                                        </div>
                                        <span class="sparkline704859712"><canvas width="19" height="50" style="display: inline-block; width: 19px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-sales">
                                        <div class="product-item-sales-value">140 шт</div>
                                        <span class="sparkline704859712"><canvas width="19" height="50" style="display: inline-block; width: 19px; height: 50px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                                <div class="product-item-tabs-content">
                                    <div class="product-item-reviews">
                                        <div class="product-item-reviews-value">0 отзывов</div>
                                        <div class="product-item-reviews-rating">5 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <style>
        .modal-dialog {
            max-width: 630px!important;
            width: auto;
        }
    </style>

    <link rel="stylesheet" href="<?=PROJECT_URL?>client/css/main.css">
    <script src="<?=PROJECT_URL?>client/js/pages/dashboard.js?id=12"></script>
</div>