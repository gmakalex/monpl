<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Категории</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Категории</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: left" role="tablist">
                        <li data-original-title="" title="">
                            <a class="nav-link active" data-toggle="tab" href="#tab-1">Все</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-field="mp" data-field-value="wildberries" data-toggle="tab" href="#tab-1">Wildberries</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ozon" href="#tab-1">Ozon</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="ali" href="#tab-1">AliExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="beru" href="#tab-1">Я.Маркет</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="kazan" href="#tab-1">KazanExpress</a>
                        </li>
                        <li data-original-title="" title="">
                            <a class="nav-link" data-toggle="tab" data-field="mp" data-field-value="sber" href="#tab-1">СберМегаМаркет</a>
                        </li>
                    </ul>
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <table id="category-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Категория</th>
                                            <th class="sorting" data-sort="sellers_count">Продавцов</th>
                                            <th class="sorting" data-sort="brands_count">Брендов</th>
                                            <th class="sorting" data-sort="products_count">Товаров</th>
                                            <th class="sorting" data-sort="average_bill">Средний чек</th>
                                            <th class="sorting" data-sort="total_sales">Продаж</th>
                                            <th class="sorting" data-state="-turnover" data-sort="turnover">Выручка</th>
                                            <th class="sorting" data-sort="tstc">TStC<span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество продаж, приходящееся на одного продавца.">?</span></th>
                                            <th class="sorting" data-sort="tsts">TStS<span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество продаж, приходящееся на одну товарную позицию.">?</span></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr data-category-id="3195" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=3195"><strong>Женщинам</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="3195"></button>
                                            </td>
                                            <td>49934</td>
                                            <td>70128</td>
                                            <td>2223142</td>
                                            <td>1,758&nbsp;руб.</td>
                                            <td>4925064</td>
                                            <td>8,656,536,276&nbsp;руб.</td>
                                            <td>98.6</td>
                                            <td>2.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="9642" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=9642"><strong>Дом</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="9642"></button>
                                            </td>
                                            <td>66191</td>
                                            <td>115054</td>
                                            <td>4091400</td>
                                            <td>752&nbsp;руб.</td>
                                            <td>4281898</td>
                                            <td>3,218,978,559&nbsp;руб.</td>
                                            <td>64.7</td>
                                            <td>1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="4050" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=4050"><strong>Детям</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="4050"></button>
                                            </td>
                                            <td>41839</td>
                                            <td>59145</td>
                                            <td>1109015</td>
                                            <td>941&nbsp;руб.</td>
                                            <td>2605846</td>
                                            <td>2,452,232,984&nbsp;руб.</td>
                                            <td>62.3</td>
                                            <td>2.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="3838" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=3838"><strong>Мужчинам</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="3838"></button>
                                            </td>
                                            <td>18648</td>
                                            <td>24435</td>
                                            <td>565069</td>
                                            <td>1,498&nbsp;руб.</td>
                                            <td>1139708</td>
                                            <td>1,707,186,077&nbsp;руб.</td>
                                            <td>61.1</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="5137" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=5137"><strong>Аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="5137"></button>
                                            </td>
                                            <td>47884</td>
                                            <td>64201</td>
                                            <td>1942938</td>
                                            <td>782&nbsp;руб.</td>
                                            <td>1819871</td>
                                            <td>1,422,968,101&nbsp;руб.</td>
                                            <td>38</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="7793" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=7793"><strong>Красота</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="7793"></button>
                                            </td>
                                            <td>24828</td>
                                            <td>45143</td>
                                            <td>690108</td>
                                            <td>418&nbsp;руб.</td>
                                            <td>3129270</td>
                                            <td>1,307,426,736&nbsp;руб.</td>
                                            <td>126</td>
                                            <td>4.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="4968" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=4968"><strong>Обувь</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="4968"></button>
                                            </td>
                                            <td>7693</td>
                                            <td>11941</td>
                                            <td>268177</td>
                                            <td>2,437&nbsp;руб.</td>
                                            <td>526106</td>
                                            <td>1,282,051,768&nbsp;руб.</td>
                                            <td>68.4</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="6473" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=6473"><strong>Спорт</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="6473"></button>
                                            </td>
                                            <td>26305</td>
                                            <td>40604</td>
                                            <td>510211</td>
                                            <td>1,380&nbsp;руб.</td>
                                            <td>769425</td>
                                            <td>1,061,551,617&nbsp;руб.</td>
                                            <td>29.3</td>
                                            <td>1.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="1" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=1"><strong>Электроника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="1"></button>
                                            </td>
                                            <td>10493</td>
                                            <td>7868</td>
                                            <td>160628</td>
                                            <td>1,560&nbsp;руб.</td>
                                            <td>666604</td>
                                            <td>1,039,809,362&nbsp;руб.</td>
                                            <td>63.5</td>
                                            <td>4.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="5388" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=5388"><strong>Электроника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="5388"></button>
                                            </td>
                                            <td>15919</td>
                                            <td>32744</td>
                                            <td>842367</td>
                                            <td>1,199&nbsp;руб.</td>
                                            <td>776906</td>
                                            <td>931,388,447&nbsp;руб.</td>
                                            <td>48.8</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="6054" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=6054"><strong>Бытовая техника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="6054"></button>
                                            </td>
                                            <td>10723</td>
                                            <td>16997</td>
                                            <td>185447</td>
                                            <td>1,985&nbsp;руб.</td>
                                            <td>439468</td>
                                            <td>872,137,473&nbsp;руб.</td>
                                            <td>41</td>
                                            <td>2.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="3" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=3"><strong>Дом и сад</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="3"></button>
                                            </td>
                                            <td>22646</td>
                                            <td>19159</td>
                                            <td>375088</td>
                                            <td>588&nbsp;руб.</td>
                                            <td>1255323</td>
                                            <td>738,391,848&nbsp;руб.</td>
                                            <td>55.4</td>
                                            <td>3.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="8191" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=8191"><strong>Игрушки</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="8191"></button>
                                            </td>
                                            <td>20565</td>
                                            <td>32429</td>
                                            <td>615441</td>
                                            <td>779&nbsp;руб.</td>
                                            <td>879299</td>
                                            <td>684,700,620&nbsp;руб.</td>
                                            <td>42.8</td>
                                            <td>1.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="6" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=6"><strong>Бытовая техника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="6"></button>
                                            </td>
                                            <td>5469</td>
                                            <td>3784</td>
                                            <td>49019</td>
                                            <td>2,668&nbsp;руб.</td>
                                            <td>233251</td>
                                            <td>622,336,570&nbsp;руб.</td>
                                            <td>42.6</td>
                                            <td>4.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="5" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=5"><strong>Красота и здоровье</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="5"></button>
                                            </td>
                                            <td>8267</td>
                                            <td>9151</td>
                                            <td>114349</td>
                                            <td>437&nbsp;руб.</td>
                                            <td>1351052</td>
                                            <td>590,742,085&nbsp;руб.</td>
                                            <td>163.4</td>
                                            <td>11.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="8342" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=8342"><strong>Продукты</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="8342"></button>
                                            </td>
                                            <td>9938</td>
                                            <td>18149</td>
                                            <td>257503</td>
                                            <td>342&nbsp;руб.</td>
                                            <td>1674165</td>
                                            <td>572,045,738&nbsp;руб.</td>
                                            <td>168.5</td>
                                            <td>6.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="4" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=4"><strong>Детские товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="4"></button>
                                            </td>
                                            <td>11141</td>
                                            <td>10437</td>
                                            <td>146659</td>
                                            <td>621&nbsp;руб.</td>
                                            <td>765367</td>
                                            <td>475,387,318&nbsp;руб.</td>
                                            <td>68.7</td>
                                            <td>5.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="2" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=2"><strong>Одежда, обувь и&nbsp;аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="2"></button>
                                            </td>
                                            <td>16281</td>
                                            <td>15145</td>
                                            <td>447497</td>
                                            <td>648&nbsp;руб.</td>
                                            <td>712321</td>
                                            <td>461,406,715&nbsp;руб.</td>
                                            <td>43.8</td>
                                            <td>1.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="9" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=9"><strong>Продукты питания</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="9"></button>
                                            </td>
                                            <td>4200</td>
                                            <td>6747</td>
                                            <td>56448</td>
                                            <td>261&nbsp;руб.</td>
                                            <td>1458612</td>
                                            <td>380,204,314&nbsp;руб.</td>
                                            <td>347.3</td>
                                            <td>25.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="9010" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=9010"><strong>Здоровье</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="9010"></button>
                                            </td>
                                            <td>10093</td>
                                            <td>17503</td>
                                            <td>184941</td>
                                            <td>580&nbsp;руб.</td>
                                            <td>609933</td>
                                            <td>354,044,943&nbsp;руб.</td>
                                            <td>60.4</td>
                                            <td>3.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="10" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=10"><strong>Аптека</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="10"></button>
                                            </td>
                                            <td>5342</td>
                                            <td>5641</td>
                                            <td>47232</td>
                                            <td>658&nbsp;руб.</td>
                                            <td>513994</td>
                                            <td>338,023,462&nbsp;руб.</td>
                                            <td>96.2</td>
                                            <td>10.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="8643" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=8643"><strong>Зоотовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="8643"></button>
                                            </td>
                                            <td>8318</td>
                                            <td>11421</td>
                                            <td>168088</td>
                                            <td>648&nbsp;руб.</td>
                                            <td>502623</td>
                                            <td>325,560,862&nbsp;руб.</td>
                                            <td>60.4</td>
                                            <td>3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="11623" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=11623"><strong>Ювелирные изделия</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="11623"></button>
                                            </td>
                                            <td>1650</td>
                                            <td>1920</td>
                                            <td>242250</td>
                                            <td>2,141&nbsp;руб.</td>
                                            <td>140624</td>
                                            <td>301,142,651&nbsp;руб.</td>
                                            <td>85.2</td>
                                            <td>0.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="9265" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=9265"><strong>Для ремонта</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="9265"></button>
                                            </td>
                                            <td>8420</td>
                                            <td>20491</td>
                                            <td>424550</td>
                                            <td>1,219&nbsp;руб.</td>
                                            <td>229111</td>
                                            <td>279,250,401&nbsp;руб.</td>
                                            <td>27.2</td>
                                            <td>0.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="6351" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=6351"><strong>Книги</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="6351"></button>
                                            </td>
                                            <td>2947</td>
                                            <td>36012</td>
                                            <td>1056123</td>
                                            <td>409&nbsp;руб.</td>
                                            <td>673085</td>
                                            <td>275,624,720&nbsp;руб.</td>
                                            <td>228.4</td>
                                            <td>0.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="11" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=11"><strong>Товары для животных</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="11"></button>
                                            </td>
                                            <td>3869</td>
                                            <td>3451</td>
                                            <td>46359</td>
                                            <td>665&nbsp;руб.</td>
                                            <td>362948</td>
                                            <td>241,324,254&nbsp;руб.</td>
                                            <td>93.8</td>
                                            <td>7.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="8" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=8"><strong>Строительство и ремонт</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="8"></button>
                                            </td>
                                            <td>8272</td>
                                            <td>7560</td>
                                            <td>140126</td>
                                            <td>831&nbsp;руб.</td>
                                            <td>286042</td>
                                            <td>237,686,336&nbsp;руб.</td>
                                            <td>34.6</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="5760" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=5760"><strong>Premium</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="5760"></button>
                                            </td>
                                            <td>5286</td>
                                            <td>6105</td>
                                            <td>50729</td>
                                            <td>3,505&nbsp;руб.</td>
                                            <td>64879</td>
                                            <td>227,401,787&nbsp;руб.</td>
                                            <td>12.3</td>
                                            <td>1.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="7" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=7"><strong>Спортивные товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="7"></button>
                                            </td>
                                            <td>6935</td>
                                            <td>6327</td>
                                            <td>80074</td>
                                            <td>909&nbsp;руб.</td>
                                            <td>240487</td>
                                            <td>218,519,445&nbsp;руб.</td>
                                            <td>34.7</td>
                                            <td>3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17710" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17710"><strong>Бытовая техника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17710"></button>
                                            </td>
                                            <td>5408</td>
                                            <td>2597</td>
                                            <td>458147</td>
                                            <td>11,050&nbsp;руб.</td>
                                            <td>19711</td>
                                            <td>217,813,261&nbsp;руб.</td>
                                            <td>3.6</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="8866" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=8866"><strong>Канцтовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="8866"></button>
                                            </td>
                                            <td>10226</td>
                                            <td>16727</td>
                                            <td>408574</td>
                                            <td>397&nbsp;руб.</td>
                                            <td>531944</td>
                                            <td>211,358,082&nbsp;руб.</td>
                                            <td>52</td>
                                            <td>1.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=14"><strong>Автотовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14"></button>
                                            </td>
                                            <td>7289</td>
                                            <td>6771</td>
                                            <td>220570</td>
                                            <td>926&nbsp;руб.</td>
                                            <td>217359</td>
                                            <td>201,352,901&nbsp;руб.</td>
                                            <td>29.8</td>
                                            <td>1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14136" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14136"><strong>Электроника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14136"></button>
                                            </td>
                                            <td>2158</td>
                                            <td>1</td>
                                            <td>36983</td>
                                            <td>2,768&nbsp;руб.</td>
                                            <td>66927</td>
                                            <td>185,268,899&nbsp;руб.</td>
                                            <td>31</td>
                                            <td>1.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15631" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15631"><strong>Инструменты</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15631"></button>
                                            </td>
                                            <td>1978</td>
                                            <td>1</td>
                                            <td>27512</td>
                                            <td>2,448&nbsp;руб.</td>
                                            <td>72987</td>
                                            <td>178,666,276&nbsp;руб.</td>
                                            <td>36.9</td>
                                            <td>2.7</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="12" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=12"><strong>Книги</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="12"></button>
                                            </td>
                                            <td>1082</td>
                                            <td>12</td>
                                            <td>128063</td>
                                            <td>450&nbsp;руб.</td>
                                            <td>382231</td>
                                            <td>171,945,977&nbsp;руб.</td>
                                            <td>353.3</td>
                                            <td>3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="11670" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=11670"><strong>Подарки </strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="11670"></button>
                                            </td>
                                            <td>27455</td>
                                            <td>35650</td>
                                            <td>261646</td>
                                            <td>628&nbsp;руб.</td>
                                            <td>271946</td>
                                            <td>170,702,151&nbsp;руб.</td>
                                            <td>9.9</td>
                                            <td>1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13487" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13487"><strong>Автомобили и мотоциклы</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13487"></button>
                                            </td>
                                            <td>2380</td>
                                            <td>1</td>
                                            <td>65805</td>
                                            <td>2,281&nbsp;руб.</td>
                                            <td>73650</td>
                                            <td>168,014,255&nbsp;руб.</td>
                                            <td>30.9</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="11405" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=11405"><strong>Автотовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="11405"></button>
                                            </td>
                                            <td>9914</td>
                                            <td>15254</td>
                                            <td>898947</td>
                                            <td>796&nbsp;руб.</td>
                                            <td>208062</td>
                                            <td>165,682,678&nbsp;руб.</td>
                                            <td>21</td>
                                            <td>0.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=15"><strong>Мебель</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15"></button>
                                            </td>
                                            <td>3074</td>
                                            <td>2298</td>
                                            <td>30483</td>
                                            <td>4,155&nbsp;руб.</td>
                                            <td>37475</td>
                                            <td>155,708,841&nbsp;руб.</td>
                                            <td>12.2</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="16" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=16"><strong>Хобби и творчество</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="16"></button>
                                            </td>
                                            <td>6544</td>
                                            <td>5885</td>
                                            <td>124750</td>
                                            <td>419&nbsp;руб.</td>
                                            <td>322259</td>
                                            <td>135,067,468&nbsp;руб.</td>
                                            <td>49.2</td>
                                            <td>2.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17706" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17706"><strong>Электроника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17706"></button>
                                            </td>
                                            <td>4509</td>
                                            <td>3094</td>
                                            <td>369099</td>
                                            <td>7,846&nbsp;руб.</td>
                                            <td>15927</td>
                                            <td>124,969,148&nbsp;руб.</td>
                                            <td>3.5</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15398" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15398"><strong>Бытовая техника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15398"></button>
                                            </td>
                                            <td>816</td>
                                            <td>1</td>
                                            <td>8928</td>
                                            <td>5,755&nbsp;руб.</td>
                                            <td>21707</td>
                                            <td>124,922,442&nbsp;руб.</td>
                                            <td>26.6</td>
                                            <td>2.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13749" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13749"><strong>Компьютеры и офис</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13749"></button>
                                            </td>
                                            <td>1360</td>
                                            <td>1</td>
                                            <td>19046</td>
                                            <td>3,174&nbsp;руб.</td>
                                            <td>39088</td>
                                            <td>124,079,484&nbsp;руб.</td>
                                            <td>28.7</td>
                                            <td>2.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="23" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=23"><strong>Бытовая химия</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="23"></button>
                                            </td>
                                            <td>2144</td>
                                            <td>1935</td>
                                            <td>16603</td>
                                            <td>396&nbsp;руб.</td>
                                            <td>264280</td>
                                            <td>104,780,543&nbsp;руб.</td>
                                            <td>123.3</td>
                                            <td>15.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="19" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=19"><strong>Канцелярские товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="19"></button>
                                            </td>
                                            <td>3913</td>
                                            <td>3238</td>
                                            <td>59764</td>
                                            <td>348&nbsp;руб.</td>
                                            <td>295056</td>
                                            <td>102,688,152&nbsp;руб.</td>
                                            <td>75.4</td>
                                            <td>4.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14331" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14331"><strong>Красота и здоровье</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14331"></button>
                                            </td>
                                            <td>2285</td>
                                            <td>1</td>
                                            <td>43379</td>
                                            <td>1,157&nbsp;руб.</td>
                                            <td>87719</td>
                                            <td>101,503,461&nbsp;руб.</td>
                                            <td>38.4</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13223" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13223"><strong>Мобильные телефоны и аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13223"></button>
                                            </td>
                                            <td>913</td>
                                            <td>1</td>
                                            <td>17001</td>
                                            <td>2,740&nbsp;руб.</td>
                                            <td>33676</td>
                                            <td>92,263,779&nbsp;руб.</td>
                                            <td>36.9</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13927" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13927"><strong>Дом и сад</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13927"></button>
                                            </td>
                                            <td>3193</td>
                                            <td>1</td>
                                            <td>80946</td>
                                            <td>888&nbsp;руб.</td>
                                            <td>96766</td>
                                            <td>85,940,848&nbsp;руб.</td>
                                            <td>30.3</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="12428" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=12428"><strong>Товары для взрослых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="12428"></button>
                                            </td>
                                            <td>2168</td>
                                            <td>4099</td>
                                            <td>68808</td>
                                            <td>838&nbsp;руб.</td>
                                            <td>90756</td>
                                            <td>76,077,628&nbsp;руб.</td>
                                            <td>41.9</td>
                                            <td>1.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="20" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=20"><strong>Товары для взрослых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="20"></button>
                                            </td>
                                            <td>1966</td>
                                            <td>1611</td>
                                            <td>34628</td>
                                            <td>668&nbsp;руб.</td>
                                            <td>111046</td>
                                            <td>74,207,141&nbsp;руб.</td>
                                            <td>56.5</td>
                                            <td>3.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17712" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17712"><strong>Компьютеры</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17712"></button>
                                            </td>
                                            <td>4244</td>
                                            <td>2568</td>
                                            <td>614099</td>
                                            <td>4,634&nbsp;руб.</td>
                                            <td>15654</td>
                                            <td>72,538,735&nbsp;руб.</td>
                                            <td>3.7</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14472" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14472"><strong>Спорт и развлечения</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14472"></button>
                                            </td>
                                            <td>1566</td>
                                            <td>1</td>
                                            <td>33394</td>
                                            <td>1,724&nbsp;руб.</td>
                                            <td>37983</td>
                                            <td>65,479,243&nbsp;руб.</td>
                                            <td>24.3</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=13"><strong>Туризм, рыбалка, охота</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13"></button>
                                            </td>
                                            <td>4698</td>
                                            <td>4132</td>
                                            <td>47995</td>
                                            <td>851&nbsp;руб.</td>
                                            <td>72285</td>
                                            <td>61,480,370&nbsp;руб.</td>
                                            <td>15.4</td>
                                            <td>1.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14763" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14763"><strong>Лампы и освещение</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14763"></button>
                                            </td>
                                            <td>1448</td>
                                            <td>1</td>
                                            <td>16997</td>
                                            <td>1,589&nbsp;руб.</td>
                                            <td>34878</td>
                                            <td>55,435,305&nbsp;руб.</td>
                                            <td>24.1</td>
                                            <td>2.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74524" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74524"><strong>Электроника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74524"></button>
                                            </td>
                                            <td>2245</td>
                                            <td>2703</td>
                                            <td>39702</td>
                                            <td>310&nbsp;руб.</td>
                                            <td>171188</td>
                                            <td>53,151,995&nbsp;руб.</td>
                                            <td>76.3</td>
                                            <td>4.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15506" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15506"><strong>Обустройство дома</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15506"></button>
                                            </td>
                                            <td>1628</td>
                                            <td>1</td>
                                            <td>18457</td>
                                            <td>1,906&nbsp;руб.</td>
                                            <td>26298</td>
                                            <td>50,133,886&nbsp;руб.</td>
                                            <td>16.2</td>
                                            <td>1.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17708" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17708"><strong>Детские товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17708"></button>
                                            </td>
                                            <td>7374</td>
                                            <td>7391</td>
                                            <td>918561</td>
                                            <td>1,434&nbsp;руб.</td>
                                            <td>31736</td>
                                            <td>45,506,250&nbsp;руб.</td>
                                            <td>4.3</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17717" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17717"><strong>Строительство и ремонт</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17717"></button>
                                            </td>
                                            <td>8200</td>
                                            <td>6978</td>
                                            <td>1572365</td>
                                            <td>2,023&nbsp;руб.</td>
                                            <td>22000</td>
                                            <td>44,499,736&nbsp;руб.</td>
                                            <td>2.7</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15768" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15768"><strong>Безопасность и защита</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15768"></button>
                                            </td>
                                            <td>726</td>
                                            <td>1</td>
                                            <td>5691</td>
                                            <td>3,081&nbsp;руб.</td>
                                            <td>13572</td>
                                            <td>41,815,192&nbsp;руб.</td>
                                            <td>18.7</td>
                                            <td>2.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17709" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17709"><strong>Продукты питания</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17709"></button>
                                            </td>
                                            <td>2781</td>
                                            <td>4290</td>
                                            <td>131238</td>
                                            <td>262&nbsp;руб.</td>
                                            <td>139261</td>
                                            <td>36,477,424&nbsp;руб.</td>
                                            <td>50.1</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74854" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74854"><strong>Красота</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74854"></button>
                                            </td>
                                            <td>2802</td>
                                            <td>3509</td>
                                            <td>61826</td>
                                            <td>149&nbsp;руб.</td>
                                            <td>225585</td>
                                            <td>33,581,599&nbsp;руб.</td>
                                            <td>80.5</td>
                                            <td>3.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17711" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17711"><strong>Красота и&nbsp;гигиена</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17711"></button>
                                            </td>
                                            <td>5395</td>
                                            <td>5831</td>
                                            <td>584883</td>
                                            <td>459&nbsp;руб.</td>
                                            <td>69782</td>
                                            <td>32,056,413&nbsp;руб.</td>
                                            <td>12.9</td>
                                            <td>0.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="16115" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/category?id=16115"><strong>НОВЫЙ ГОД</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="16115"></button>
                                            </td>
                                            <td>5168</td>
                                            <td>6285</td>
                                            <td>19364</td>
                                            <td>1,379&nbsp;руб.</td>
                                            <td>22950</td>
                                            <td>31,646,646&nbsp;руб.</td>
                                            <td>4.4</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14868" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14868"><strong>Игрушки и хобби</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14868"></button>
                                            </td>
                                            <td>768</td>
                                            <td>1</td>
                                            <td>17641</td>
                                            <td>1,735&nbsp;руб.</td>
                                            <td>16411</td>
                                            <td>28,480,458&nbsp;руб.</td>
                                            <td>21.4</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="24" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=24"><strong>Всё для игр</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="24"></button>
                                            </td>
                                            <td>927</td>
                                            <td>678</td>
                                            <td>8837</td>
                                            <td>3,541&nbsp;руб.</td>
                                            <td>8021</td>
                                            <td>28,404,236&nbsp;руб.</td>
                                            <td>8.7</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17715" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17715"><strong>Дом</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17715"></button>
                                            </td>
                                            <td>8284</td>
                                            <td>7117</td>
                                            <td>888890</td>
                                            <td>969&nbsp;руб.</td>
                                            <td>28545</td>
                                            <td>27,651,765&nbsp;руб.</td>
                                            <td>3.4</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74998" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74998"><strong>Товары для дома</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74998"></button>
                                            </td>
                                            <td>4369</td>
                                            <td>5522</td>
                                            <td>79138</td>
                                            <td>172&nbsp;руб.</td>
                                            <td>150353</td>
                                            <td>25,877,068&nbsp;руб.</td>
                                            <td>34.4</td>
                                            <td>1.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13290" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13290"><strong>Мать и ребенок</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13290"></button>
                                            </td>
                                            <td>417</td>
                                            <td>1</td>
                                            <td>7351</td>
                                            <td>1,523&nbsp;руб.</td>
                                            <td>16389</td>
                                            <td>24,952,810&nbsp;руб.</td>
                                            <td>39.3</td>
                                            <td>2.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17714" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17714"><strong>Товары для животных</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17714"></button>
                                            </td>
                                            <td>1598</td>
                                            <td>1132</td>
                                            <td>169783</td>
                                            <td>757&nbsp;руб.</td>
                                            <td>31099</td>
                                            <td>23,555,892&nbsp;руб.</td>
                                            <td>19.5</td>
                                            <td>0.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74653" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74653"><strong>Одежда</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74653"></button>
                                            </td>
                                            <td>2927</td>
                                            <td>3547</td>
                                            <td>57062</td>
                                            <td>335&nbsp;руб.</td>
                                            <td>69169</td>
                                            <td>23,173,483&nbsp;руб.</td>
                                            <td>23.6</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17720" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17720"><strong>Здоровье</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17720"></button>
                                            </td>
                                            <td>3813</td>
                                            <td>4334</td>
                                            <td>476579</td>
                                            <td>1,308&nbsp;руб.</td>
                                            <td>17185</td>
                                            <td>22,473,073&nbsp;руб.</td>
                                            <td>4.5</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17713" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17713"><strong>Спорт и отдых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17713"></button>
                                            </td>
                                            <td>6040</td>
                                            <td>5436</td>
                                            <td>449961</td>
                                            <td>1,965&nbsp;руб.</td>
                                            <td>10944</td>
                                            <td>21,499,807&nbsp;руб.</td>
                                            <td>1.8</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74794" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74794"><strong>Аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74794"></button>
                                            </td>
                                            <td>3125</td>
                                            <td>4093</td>
                                            <td>54530</td>
                                            <td>176&nbsp;руб.</td>
                                            <td>85745</td>
                                            <td>15,063,909&nbsp;руб.</td>
                                            <td>27.4</td>
                                            <td>1.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=17"><strong>Ювелирные украшения</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17"></button>
                                            </td>
                                            <td>401</td>
                                            <td>392</td>
                                            <td>17646</td>
                                            <td>975&nbsp;руб.</td>
                                            <td>13209</td>
                                            <td>12,879,014&nbsp;руб.</td>
                                            <td>32.9</td>
                                            <td>0.7</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17707" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17707"><strong>Дача и сад</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17707"></button>
                                            </td>
                                            <td>4155</td>
                                            <td>2450</td>
                                            <td>242438</td>
                                            <td>1,420&nbsp;руб.</td>
                                            <td>7767</td>
                                            <td>11,029,432&nbsp;руб.</td>
                                            <td>1.9</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75194" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75194"><strong>Автотовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75194"></button>
                                            </td>
                                            <td>1653</td>
                                            <td>1949</td>
                                            <td>27462</td>
                                            <td>301&nbsp;руб.</td>
                                            <td>33129</td>
                                            <td>9,957,887&nbsp;руб.</td>
                                            <td>20</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74944" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74944"><strong>Здоровье</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74944"></button>
                                            </td>
                                            <td>1628</td>
                                            <td>1910</td>
                                            <td>8846</td>
                                            <td>294&nbsp;руб.</td>
                                            <td>31466</td>
                                            <td>9,256,533&nbsp;руб.</td>
                                            <td>19.3</td>
                                            <td>3.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17726" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17726"><strong>Товары для авто- и&nbsp;мототехники</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17726"></button>
                                            </td>
                                            <td>4892</td>
                                            <td>3832</td>
                                            <td>1042155</td>
                                            <td>1,825&nbsp;руб.</td>
                                            <td>4564</td>
                                            <td>8,329,441&nbsp;руб.</td>
                                            <td>0.9</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13096" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13096"><strong>Женская одежда</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13096"></button>
                                            </td>
                                            <td>371</td>
                                            <td>1</td>
                                            <td>7540</td>
                                            <td>1,205&nbsp;руб.</td>
                                            <td>6776</td>
                                            <td>8,166,538&nbsp;руб.</td>
                                            <td>18.3</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17718" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17718"><strong>Бытовая химия</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17718"></button>
                                            </td>
                                            <td>2354</td>
                                            <td>1558</td>
                                            <td>80543</td>
                                            <td>395&nbsp;руб.</td>
                                            <td>20458</td>
                                            <td>8,071,388&nbsp;руб.</td>
                                            <td>8.7</td>
                                            <td>0.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17723" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17723"><strong>Одежда, обувь и аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17723"></button>
                                            </td>
                                            <td>5325</td>
                                            <td>5544</td>
                                            <td>706521</td>
                                            <td>1,216&nbsp;руб.</td>
                                            <td>6564</td>
                                            <td>7,979,741&nbsp;руб.</td>
                                            <td>1.2</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75334" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75334"><strong>Детские товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75334"></button>
                                            </td>
                                            <td>2408</td>
                                            <td>2861</td>
                                            <td>31750</td>
                                            <td>289&nbsp;руб.</td>
                                            <td>27283</td>
                                            <td>7,893,438&nbsp;руб.</td>
                                            <td>11.3</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74614" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74614"><strong>Бытовая техника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74614"></button>
                                            </td>
                                            <td>658</td>
                                            <td>739</td>
                                            <td>4937</td>
                                            <td>1,011&nbsp;руб.</td>
                                            <td>7127</td>
                                            <td>7,203,420&nbsp;руб.</td>
                                            <td>10.8</td>
                                            <td>1.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75692" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75692"><strong>Бытовая химия и личная гигиена</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75692"></button>
                                            </td>
                                            <td>1515</td>
                                            <td>1920</td>
                                            <td>12621</td>
                                            <td>204&nbsp;руб.</td>
                                            <td>34060</td>
                                            <td>6,951,189&nbsp;руб.</td>
                                            <td>22.5</td>
                                            <td>2.7</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75099" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75099"><strong>Строительство и ремонт</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75099"></button>
                                            </td>
                                            <td>1554</td>
                                            <td>1806</td>
                                            <td>20762</td>
                                            <td>286&nbsp;руб.</td>
                                            <td>24069</td>
                                            <td>6,883,095&nbsp;руб.</td>
                                            <td>15.5</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13881" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13881"><strong>Багаж и сумки</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13881"></button>
                                            </td>
                                            <td>368</td>
                                            <td>1</td>
                                            <td>3156</td>
                                            <td>1,870&nbsp;руб.</td>
                                            <td>3443</td>
                                            <td>6,439,075&nbsp;руб.</td>
                                            <td>9.4</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75399" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75399"><strong>Хобби и творчество</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75399"></button>
                                            </td>
                                            <td>1905</td>
                                            <td>2304</td>
                                            <td>35359</td>
                                            <td>122&nbsp;руб.</td>
                                            <td>52193</td>
                                            <td>6,375,279&nbsp;руб.</td>
                                            <td>27.4</td>
                                            <td>1.5</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15034" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15034"><strong>Мебель</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15034"></button>
                                            </td>
                                            <td>322</td>
                                            <td>1</td>
                                            <td>1840</td>
                                            <td>3,041&nbsp;руб.</td>
                                            <td>2044</td>
                                            <td>6,216,116&nbsp;руб.</td>
                                            <td>6.3</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="21" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=21"><strong>Антиквариат и коллекционирование</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="21"></button>
                                            </td>
                                            <td>807</td>
                                            <td>859</td>
                                            <td>20699</td>
                                            <td>436&nbsp;руб.</td>
                                            <td>13672</td>
                                            <td>5,964,363&nbsp;руб.</td>
                                            <td>16.9</td>
                                            <td>0.7</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17725" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17725"><strong>Оборудование</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17725"></button>
                                            </td>
                                            <td>4715</td>
                                            <td>2874</td>
                                            <td>212672</td>
                                            <td>2,455&nbsp;руб.</td>
                                            <td>2395</td>
                                            <td>5,880,570&nbsp;руб.</td>
                                            <td>0.5</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13668" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13668"><strong>Украшения и аксессуары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13668"></button>
                                            </td>
                                            <td>216</td>
                                            <td>1</td>
                                            <td>7131</td>
                                            <td>989&nbsp;руб.</td>
                                            <td>5814</td>
                                            <td>5,751,754&nbsp;руб.</td>
                                            <td>26.9</td>
                                            <td>0.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15181" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15181"><strong>Канцтовары для офиса и дома</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15181"></button>
                                            </td>
                                            <td>501</td>
                                            <td>1</td>
                                            <td>10611</td>
                                            <td>739&nbsp;руб.</td>
                                            <td>7622</td>
                                            <td>5,632,576&nbsp;руб.</td>
                                            <td>15.2</td>
                                            <td>0.7</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75514" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75514"><strong>Спорт и отдых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75514"></button>
                                            </td>
                                            <td>1472</td>
                                            <td>1729</td>
                                            <td>17123</td>
                                            <td>307&nbsp;руб.</td>
                                            <td>17238</td>
                                            <td>5,286,946&nbsp;руб.</td>
                                            <td>11.7</td>
                                            <td>1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13727" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13727"><strong>Наручные часы</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13727"></button>
                                            </td>
                                            <td>245</td>
                                            <td>1</td>
                                            <td>2141</td>
                                            <td>1,152&nbsp;руб.</td>
                                            <td>4454</td>
                                            <td>5,128,829&nbsp;руб.</td>
                                            <td>18.2</td>
                                            <td>2.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75487" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75487"><strong>Товары для взрослых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75487"></button>
                                            </td>
                                            <td>455</td>
                                            <td>494</td>
                                            <td>7347</td>
                                            <td>262&nbsp;руб.</td>
                                            <td>19060</td>
                                            <td>4,989,785&nbsp;руб.</td>
                                            <td>41.9</td>
                                            <td>2.6</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75904" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75904"><strong>Зоотовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75904"></button>
                                            </td>
                                            <td>761</td>
                                            <td>840</td>
                                            <td>10202</td>
                                            <td>229&nbsp;руб.</td>
                                            <td>18713</td>
                                            <td>4,286,817&nbsp;руб.</td>
                                            <td>24.6</td>
                                            <td>1.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17721" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17721"><strong>Хобби и творчество</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17721"></button>
                                            </td>
                                            <td>3486</td>
                                            <td>3212</td>
                                            <td>266982</td>
                                            <td>1,131&nbsp;руб.</td>
                                            <td>3661</td>
                                            <td>4,142,243&nbsp;руб.</td>
                                            <td>1.1</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15120" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15120"><strong>Продукты</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15120"></button>
                                            </td>
                                            <td>98</td>
                                            <td>1</td>
                                            <td>1770</td>
                                            <td>305&nbsp;руб.</td>
                                            <td>13029</td>
                                            <td>3,971,895&nbsp;руб.</td>
                                            <td>132.9</td>
                                            <td>7.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75783" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75783"><strong>Канцтовары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75783"></button>
                                            </td>
                                            <td>1423</td>
                                            <td>1680</td>
                                            <td>18621</td>
                                            <td>82&nbsp;руб.</td>
                                            <td>41923</td>
                                            <td>3,427,773&nbsp;руб.</td>
                                            <td>29.5</td>
                                            <td>2.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="13171" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=13171"><strong>Мужская одежда</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="13171"></button>
                                            </td>
                                            <td>205</td>
                                            <td>1</td>
                                            <td>4441</td>
                                            <td>963&nbsp;руб.</td>
                                            <td>3433</td>
                                            <td>3,304,427&nbsp;руб.</td>
                                            <td>16.7</td>
                                            <td>0.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17716" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17716"><strong>Мебель</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17716"></button>
                                            </td>
                                            <td>2564</td>
                                            <td>1638</td>
                                            <td>154022</td>
                                            <td>3,150&nbsp;руб.</td>
                                            <td>1034</td>
                                            <td>3,256,629&nbsp;руб.</td>
                                            <td>0.4</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15604" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15604"><strong>Шиньоны и парики</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15604"></button>
                                            </td>
                                            <td>53</td>
                                            <td>1</td>
                                            <td>326</td>
                                            <td>984&nbsp;руб.</td>
                                            <td>2996</td>
                                            <td>2,948,053&nbsp;руб.</td>
                                            <td>56.5</td>
                                            <td>9.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="14719" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=14719"><strong>Обувь</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="14719"></button>
                                            </td>
                                            <td>166</td>
                                            <td>1</td>
                                            <td>2501</td>
                                            <td>972&nbsp;руб.</td>
                                            <td>2868</td>
                                            <td>2,788,353&nbsp;руб.</td>
                                            <td>17.3</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="74754" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=74754"><strong>Обувь</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="74754"></button>
                                            </td>
                                            <td>452</td>
                                            <td>510</td>
                                            <td>4866</td>
                                            <td>470&nbsp;руб.</td>
                                            <td>5821</td>
                                            <td>2,736,584&nbsp;руб.</td>
                                            <td>12.9</td>
                                            <td>1.2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="76063" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=76063"><strong>Дача, сад и огород</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="76063"></button>
                                            </td>
                                            <td>640</td>
                                            <td>706</td>
                                            <td>15519</td>
                                            <td>77&nbsp;руб.</td>
                                            <td>30940</td>
                                            <td>2,370,964&nbsp;руб.</td>
                                            <td>48.3</td>
                                            <td>2</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75591" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75591"><strong>Продукты питания</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75591"></button>
                                            </td>
                                            <td>68</td>
                                            <td>304</td>
                                            <td>10201</td>
                                            <td>101&nbsp;руб.</td>
                                            <td>21904</td>
                                            <td>2,206,523&nbsp;руб.</td>
                                            <td>322.1</td>
                                            <td>2.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15849" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15849"><strong>Аксессуары для одежды</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15849"></button>
                                            </td>
                                            <td>236</td>
                                            <td>1</td>
                                            <td>1787</td>
                                            <td>629&nbsp;руб.</td>
                                            <td>2013</td>
                                            <td>1,266,666&nbsp;руб.</td>
                                            <td>8.5</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17705" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17705"><strong>Для школы и офиса</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17705"></button>
                                            </td>
                                            <td>2788</td>
                                            <td>2012</td>
                                            <td>206303</td>
                                            <td>401&nbsp;руб.</td>
                                            <td>2897</td>
                                            <td>1,160,471&nbsp;руб.</td>
                                            <td>1</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="75973" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/kazan.ico">
                                                <a href="/client/category?id=75973"><strong>Книги</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="75973"></button>
                                            </td>
                                            <td>364</td>
                                            <td>415</td>
                                            <td>9975</td>
                                            <td>124&nbsp;руб.</td>
                                            <td>9202</td>
                                            <td>1,137,417&nbsp;руб.</td>
                                            <td>25.3</td>
                                            <td>0.9</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17727" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17727"><strong>Товары для взрослых</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17727"></button>
                                            </td>
                                            <td>566</td>
                                            <td>525</td>
                                            <td>136544</td>
                                            <td>728&nbsp;руб.</td>
                                            <td>1529</td>
                                            <td>1,112,526&nbsp;руб.</td>
                                            <td>2.7</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="22" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=22"><strong>Цифровые товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="22"></button>
                                            </td>
                                            <td>65</td>
                                            <td>81</td>
                                            <td>2490</td>
                                            <td>908&nbsp;руб.</td>
                                            <td>973</td>
                                            <td>883,103&nbsp;руб.</td>
                                            <td>15</td>
                                            <td>0.4</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="18" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=18"><strong>Музыка и видео</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="18"></button>
                                            </td>
                                            <td>122</td>
                                            <td>33</td>
                                            <td>20015</td>
                                            <td>417&nbsp;руб.</td>
                                            <td>1597</td>
                                            <td>666,262&nbsp;руб.</td>
                                            <td>13.1</td>
                                            <td>0.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15094" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15094"><strong>Электронные компоненты и принадлежности</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15094"></button>
                                            </td>
                                            <td>83</td>
                                            <td>1</td>
                                            <td>1534</td>
                                            <td>356&nbsp;руб.</td>
                                            <td>1659</td>
                                            <td>590,756&nbsp;руб.</td>
                                            <td>20</td>
                                            <td>1.1</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="16999" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=16999"><strong>Электронные сигареты и товары для курения</strong></a>

                                            </td>
                                            <td>101</td>
                                            <td>79</td>
                                            <td>205</td>
                                            <td>566&nbsp;руб.</td>
                                            <td>1028</td>
                                            <td>582,248&nbsp;руб.</td>
                                            <td>10.2</td>
                                            <td>5</td>
                                            <td><button data-id="35659" data-mp="ozon" type="category" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>
                                        </tr>

                                        <tr data-category-id="17073" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=17073"><strong>Ozon Услуги</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17073"></button>
                                            </td>
                                            <td>379</td>
                                            <td>265</td>
                                            <td>3249</td>
                                            <td>579&nbsp;руб.</td>
                                            <td>877</td>
                                            <td>507,793&nbsp;руб.</td>
                                            <td>2.3</td>
                                            <td>0.3</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17722" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17722"><strong>Книги</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17722"></button>
                                            </td>
                                            <td>553</td>
                                            <td>1785</td>
                                            <td>183929</td>
                                            <td>579&nbsp;руб.</td>
                                            <td>538</td>
                                            <td>311,628&nbsp;руб.</td>
                                            <td>1</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15582" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15582"><strong>Тематическая одежда и униформа</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15582"></button>
                                            </td>
                                            <td>48</td>
                                            <td>1</td>
                                            <td>540</td>
                                            <td>515&nbsp;руб.</td>
                                            <td>411</td>
                                            <td>211,591&nbsp;руб.</td>
                                            <td>8.6</td>
                                            <td>0.8</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="25" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=25"><strong>Автомобили и мототехника</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="25"></button>
                                            </td>
                                            <td>92</td>
                                            <td>42</td>
                                            <td>700</td>
                                            <td>40,643&nbsp;руб.</td>
                                            <td>1</td>
                                            <td>40,643&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17724" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17724"><strong>Цифровые товары</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17724"></button>
                                            </td>
                                            <td>275</td>
                                            <td>131</td>
                                            <td>2498</td>
                                            <td>334&nbsp;руб.</td>
                                            <td>57</td>
                                            <td>19,013&nbsp;руб.</td>
                                            <td>0.2</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="77721" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ozon.ico">
                                                <a href="/client/category?id=77721"><strong>Аксессуары</strong></a>

                                            </td>
                                            <td>2</td>
                                            <td>2</td>
                                            <td>2</td>
                                            <td>340&nbsp;руб.</td>
                                            <td>3</td>
                                            <td>1,020&nbsp;руб.</td>
                                            <td>1.5</td>
                                            <td>1.5</td>
                                            <td><button data-id="7697" data-mp="ozon" type="category" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>
                                        </tr>

                                        <tr data-category-id="25042" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=25042"><strong>Цветы, букеты, композиции</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="25042"></button>
                                            </td>
                                            <td>323</td>
                                            <td>58</td>
                                            <td>7084</td>
                                            <td>349&nbsp;руб.</td>
                                            <td>1</td>
                                            <td>349&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="15012" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/ali.ico">
                                                <a href="/client/category?id=15012"><strong>Свадьбы и торжества</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="15012"></button>
                                            </td>
                                            <td>5</td>
                                            <td>1</td>
                                            <td>6</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="17719" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=17719"><strong>Ювелирные украшения</strong></a>
                                                <button class="btn-white dropdown-toggle" type="button" data-level="0" data-category-id="17719"></button>
                                            </td>
                                            <td>131</td>
                                            <td>146</td>
                                            <td>53805</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td></td>
                                        </tr>

                                        <tr data-category-id="25041" data-parent="false">
                                            <td style="padding-left: 5px">
                                                <img style="width: 12px;margin-top: -4px;" src="/images/beru.ico">
                                                <a href="/client/category?id=25041"><strong>Книги</strong></a>

                                            </td>
                                            <td>143</td>
                                            <td>248</td>
                                            <td>1100</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td><button data-id="54510" data-mp="beru" type="category" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=PROJECT_URL?>client/js/pages/categories.js?id=6"></script></div>