<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Топ товаров</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Товары</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                </div>
            </div>
        </div>

        <div class="row search-block to-toggle-loader" style="display: none">
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Объем рынка</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="turnover">40 886,200</h2>
                        <h5 class="no-margins" id="turnover-diff">40 886,200</h5>
                        <div class="stat-percent font-bold" id="turnover-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Упущенная выручка</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="loses">275,800</h2>
                        <h5 class="no-margins" id="loses-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="loses-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Общий потенциал рынка</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="potential_turnover">106,120</h2>
                        <h5 class="no-margins" id="potential_turnover-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="potential_turnover-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Ср. скорость продаж</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="avg_sale_speed">80,600</h2>
                        <h5 class="no-margins" id="avg_sale_speed-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="avg_sale_speed-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Cр. выручка на продавца</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="sales_to_seller">80,600</h2>
                        <h5 class="no-margins" id="sales_to_seller-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="sales_to_seller-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row search-block to-toggle-loader" style="display: none">

            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Динамика продаж и конкуренция</h4>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div>
                            <canvas style="margin: 0 auto;" id="chart-price" height="300"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row search-block to-toggle-loader" style="display: none">
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Обьем рынка у ТОП 10</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="top_10_turnover">40 886,200</h2>
                        <h5 class="no-margins" id="top_10_turnover-diff">40 886,200</h5>
                        <div class="stat-percent font-bold" id="top_10_turnover-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>% продавцов с продажами</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="percent_sellers_with_sales">275,800</h2>
                        <h5 class="no-margins" id="percent_sellers_with_sales-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="percent_sellers_with_sales-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>% товаров с продажами</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="percent_products_with_sales">106,120</h2>
                        <h5 class="no-margins" id="percent_products_with_sales-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="percent_products_with_sales-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Ср. рейтинг</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="avg_rating">80,600</h2>
                        <h5 class="no-margins" id="avg_rating-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="avg_rating-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Ср. количество отзывов</h5>
                    </div>
                    <div class="ibox-content sk-loading">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h2 class="no-margins" id="avg_reviews">80,600</h2>
                        <h5 class="no-margins" id="avg_reviews-diff">275,800</h5>
                        <div class="stat-percent font-bold" id="avg_reviews-diff-percent"></div>
                        <small>Разница с пред. периодом:</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: left;" role="tablist" data-hint-mp="1">
                        <li data-original-title="" title=""><a class="nav-link" data-field="mp=wildberries&amp;q[mp][in]" data-field-value="wildberries" data-toggle="tab" href="#tab-1">Wildberries</a></li>
                        <li data-original-title="" title=""><a class="nav-link" data-field="mp=ozon&amp;q[mp][in]" data-field-value="ozon" data-toggle="tab" href="#tab-1">Ozon</a></li>
                        <li data-original-title="" title=""><a class="nav-link" data-field="mp=ali&amp;q[mp][in]" data-field-value="ali" data-toggle="tab" href="#tab-1">Aliexpress</a></li>
                        <li data-original-title="" title=""><a class="nav-link" data-field="mp=beru&amp;q[mp][in]" data-field-value="beru" data-toggle="tab" href="#tab-1">Я.Маркет</a></li>
                        <li data-original-title="" title=""><a class="nav-link active show" data-field="mp=kazan&amp;q[mp][in]" data-field-value="kazan" data-toggle="tab" href="#tab-1">KazanExpress</a></li>
                        <li data-original-title="" title=""><a class="nav-link" data-field="mp=sber&amp;q[mp][in]" data-field-value="sber" data-toggle="tab" href="#tab-1">СберМегаМаркет</a></li>
                    </ul>
                    <ul class="nav nav-tabs" style="float: right" role="tablist" data-hint-mp="2">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active show">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">

                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Фото</th>
                                            <th>Название</th>
                                            <th>SKU</th>
                                            <th>Продавец</th>
                                            <th>Бренд</th>
                                            <th>Наличие</th>
                                            <th>Отзывы</th>
                                            <th>Оценка</th>
                                            <th>Цена</th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="Sales">Продажи</th>
                                            <th class="sorting" style="min-width: 95px;" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6sob9l8g8bl6nea9nqg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=236567325">Смартфон Samsung Galaxy A52 128 GB</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1231549</td>
                                            <td><a href="/client/seller?id=363137">ИП Малкина Эльвина Раисовна (319169000095433)</a></td>
                                            <td><a href="/client/brand?id=8838472">MediaMarket</a></td>
                                            <td>6</td>
                                            <td>10</td>
                                            <td>4.6</td>
                                            <td>
                                                22,990&nbsp;руб. <br>

                                                <span class="old-price-percent">(-1%)</span>
                                                <span class="old-price">23,277&nbsp;руб.</span>

                                            </td>
                                            <td>35</td>
                                            <td>804,650&nbsp;руб.</td>
                                            <td><button data-id="1231549" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c1uni227qr497pbo0mv0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=439784780">Смартфон Apple iPhone 11 128GB</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>542466</td>
                                            <td><a href="/client/seller?id=363138">ИП Гимадиев Эмиль Рафаэлевич (316167700052415)</a></td>
                                            <td><a href="/client/brand?id=8838474">AppleMall</a></td>
                                            <td>11</td>
                                            <td>17</td>
                                            <td>4.8</td>
                                            <td>
                                                50,490&nbsp;руб. <br>

                                                <span class="old-price-percent">(-6%)</span>
                                                <span class="old-price">53,490&nbsp;руб.</span>

                                            </td>
                                            <td>13</td>
                                            <td>667,370&nbsp;руб.</td>
                                            <td><button data-id="542466" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c2h2l1haof0vb8qnq770/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=672159601">Смартфон Apple iPhone 11 128GB (РСТ)</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>588275</td>
                                            <td><a href="/client/seller?id=361199">ИП Мухаметзянова Диляра Ренатовна (321169000073970)</a></td>
                                            <td><a href="/client/brand?id=8782145">AppleShop</a></td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>5</td>
                                            <td>
                                                50,490&nbsp;руб. <br>

                                                <span class="old-price-percent">(-9%)</span>
                                                <span class="old-price">55,610&nbsp;руб.</span>

                                            </td>
                                            <td>12</td>
                                            <td>613,880&nbsp;руб.</td>
                                            <td><button data-id="588275" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6fkgtmkkodd3ir6jkb0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=258257315">Беспроводные наушники Apple AirPods (2-го поколения) в зарядном футляре, MV7N2RU/A</a><br>

                                                <a class="category-link" href="/client/category?id=74549">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Наушники и аудиотехника / Наушники
                                                </a>


                                            </td>
                                            <td>721019</td>
                                            <td><a href="/client/seller?id=360990">ООО "Казаньэкспресс Диджитал Ритейл" (1181690055557)</a></td>
                                            <td><a href="/client/brand?id=8776014">Техника Apple</a></td>
                                            <td>6</td>
                                            <td>31</td>
                                            <td>4.9</td>
                                            <td>
                                                10,679&nbsp;руб. <br>

                                                <span class="old-price-percent">(-14%)</span>
                                                <span class="old-price">12,490&nbsp;руб.</span>

                                            </td>
                                            <td>53</td>
                                            <td>571,542&nbsp;руб.</td>
                                            <td><button data-id="721019" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/bth44rjbkrvg89vtfhlg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=165673577">Робот-пылесос Xiaomi Mijia Sweeping Vacuum Cleaner 1C (Mi Robot Vacuum-Mop)</a><br>

                                                <a class="category-link" href="/client/category?id=74631">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Бытовая техника / Техника для дома / Пылесосы
                                                </a>


                                            </td>
                                            <td>190502</td>
                                            <td><a href="/client/seller?id=357652">ИП Валиев Азат Мансурович (321169000118515)</a></td>
                                            <td><a href="/client/brand?id=8725690">Xiaomi Mi Store</a></td>
                                            <td>10</td>
                                            <td>101</td>
                                            <td>4.8</td>
                                            <td>
                                                15,990&nbsp;руб. <br>

                                                <span class="old-price-percent">(-3%)</span>
                                                <span class="old-price">16,490&nbsp;руб.</span>

                                            </td>
                                            <td>37</td>
                                            <td>569,130&nbsp;руб.</td>
                                            <td><button data-id="190502" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c2ik49l667fl5e8j1alg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=166041953">Фитнес-браслет Xiaomi Mi Band 6, NFC</a><br>

                                                <a class="category-link" href="/client/category?id=74535">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Умные часы и фитнес браслеты / Фитнес браслеты
                                                </a>


                                            </td>
                                            <td>570456</td>
                                            <td><a href="/client/seller?id=357652">ИП Валиев Азат Мансурович (321169000118515)</a></td>
                                            <td><a href="/client/brand?id=8725690">Xiaomi Mi Store</a></td>
                                            <td>385</td>
                                            <td>324</td>
                                            <td>4.9</td>
                                            <td>
                                                2,727&nbsp;руб. <br>

                                                <span class="old-price-percent">(-32%)</span>
                                                <span class="old-price">3,990&nbsp;руб.</span>

                                            </td>
                                            <td>171</td>
                                            <td>464,999&nbsp;руб.</td>
                                            <td><button data-id="570456" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c3ta6uivjs096f8j5rqg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=583316314">Смартфон Apple iPhone 11 128GB (РСТ)</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>707488</td>
                                            <td><a href="/client/seller?id=363144">ИП Гильмутдинов Ильнур Тагирович (321169000089072)</a></td>
                                            <td><a href="/client/brand?id=8838495">AppleWorld</a></td>
                                            <td>1</td>
                                            <td>3</td>
                                            <td>5</td>
                                            <td>
                                                51,490&nbsp;руб. <br>

                                                <span class="old-price-percent">(-5%)</span>
                                                <span class="old-price">53,990&nbsp;руб.</span>

                                            </td>
                                            <td>9</td>
                                            <td>461,910&nbsp;руб.</td>
                                            <td><button data-id="707488" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6fkp66anmn8vek2g6c0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=247436738">Смартфон Xiaomi Redmi 9A 2/32GB</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1135938</td>
                                            <td><a href="/client/seller?id=360401">ИП Латыпов Азат Ильмирович (321169000005230)</a></td>
                                            <td><a href="/client/brand?id=9303851">TechnoSale</a></td>
                                            <td>6</td>
                                            <td>33</td>
                                            <td>5</td>
                                            <td>
                                                7,590&nbsp;руб. <br>

                                                <span class="old-price-percent">(-5%)</span>
                                                <span class="old-price">7,990&nbsp;руб.</span>

                                            </td>
                                            <td>56</td>
                                            <td>425,040&nbsp;руб.</td>
                                            <td><button data-id="1135938" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c7tp6cohmedd7lh2inug/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=662898725">Смартфон Xiaomi Redmi 9A 2/32Gb</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1417129</td>
                                            <td><a href="/client/seller?id=430564">ИП Гатауллин Камиль Фаритович (320169000092080)</a></td>
                                            <td><a href="/client/brand?id=50730179">SmartPlace</a></td>
                                            <td>46</td>
                                            <td>1</td>
                                            <td>5</td>
                                            <td>
                                                7,590&nbsp;руб. <br>

                                                <span class="old-price-percent">(-16%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>50</td>
                                            <td>379,500&nbsp;руб.</td>
                                            <td><button data-id="1417129" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c0j8p2qth9d9mrchrqf0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=166135588">Беспроводные наушники Xiaomi Redmi Airdots 2</a><br>

                                                <a class="category-link" href="/client/category?id=74549">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Наушники и аудиотехника / Наушники
                                                </a>


                                            </td>
                                            <td>59104</td>
                                            <td><a href="/client/seller?id=357652">ИП Валиев Азат Мансурович (321169000118515)</a></td>
                                            <td><a href="/client/brand?id=8725690">Xiaomi Mi Store</a></td>
                                            <td>975</td>
                                            <td>805</td>
                                            <td>4.6</td>
                                            <td>
                                                1,390&nbsp;руб. <br>

                                                <span class="old-price-percent">(-22%)</span>
                                                <span class="old-price">1,790&nbsp;руб.</span>

                                            </td>
                                            <td>261</td>
                                            <td>367,590&nbsp;руб.</td>
                                            <td><button data-id="59104" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c3rtkr2f2trr4t3p7oa0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=439784779">Смартфон Apple iPhone 11 128GB</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>702651</td>
                                            <td><a href="/client/seller?id=363143">ИП Мамаева Анастасия Алексеевна (321169000091768)</a></td>
                                            <td><a href="/client/brand?id=8838494">ApplePlace</a></td>
                                            <td>17</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>
                                                51,490&nbsp;руб. <br>

                                                <span class="old-price-percent">(-5%)</span>
                                                <span class="old-price">53,990&nbsp;руб.</span>

                                            </td>
                                            <td>7</td>
                                            <td>360,430&nbsp;руб.</td>
                                            <td><button data-id="702651" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c7ls6c4jkgpi43dqrekg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=591819115">Смартфон Apple iPhone 11, 128 ГБ, белый (новая комплектация)</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1352971</td>
                                            <td><a href="/client/seller?id=360990">ООО "Казаньэкспресс Диджитал Ритейл" (1181690055557)</a></td>
                                            <td><a href="/client/brand?id=8776014">Техника Apple</a></td>
                                            <td>16</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>
                                                50,342&nbsp;руб. <br>

                                                <span class="old-price-percent">(-20%)</span>
                                                <span class="old-price">62,928&nbsp;руб.</span>

                                            </td>
                                            <td>7</td>
                                            <td>352,394&nbsp;руб.</td>
                                            <td><button data-id="1352971" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c5p8fmidjik3tvfjlv90/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=165739970">Защитное стекло для iPhone 11 / XS / XR / XS Max / Х / 11 Pro / X / XR / 11 / 13 / 13 Pro</a><br>

                                                <a class="category-link" href="/client/category?id=74526">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Аксессуары и запчасти
                                                </a>


                                            </td>
                                            <td>126042</td>
                                            <td><a href="/client/seller?id=361007">ИП Никифорова Кристина Владимировна (320213000004139)</a></td>
                                            <td><a href="/client/brand?id=8776049">Mr.Steklov</a></td>
                                            <td>19066</td>
                                            <td>5489</td>
                                            <td>4.7</td>
                                            <td>
                                                95&nbsp;руб. <br>

                                                <span class="old-price-percent">(-68%)</span>
                                                <span class="old-price">299&nbsp;руб.</span>

                                            </td>
                                            <td>3389</td>
                                            <td>319,095&nbsp;руб.</td>
                                            <td><button data-id="126042" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/brs5s4mpskc6i343nm3g/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=166883903">Массажная подушка массажер для тела,шеи,спины, поясницы и ног с инфракрасным прогревом</a><br>

                                                <a class="category-link" href="/client/category?id=74955">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Здоровье / Массажное оборудование / Массажные подушки
                                                </a>


                                            </td>
                                            <td>150127</td>
                                            <td><a href="/client/seller?id=357691">ИП Баялиева Розалина Владимировна (319169000043499)</a></td>
                                            <td><a href="/client/brand?id=8735472">Dam Dom</a></td>
                                            <td>293</td>
                                            <td>286</td>
                                            <td>4.6</td>
                                            <td>
                                                989&nbsp;руб. <br>

                                                <span class="old-price-percent">(-49%)</span>
                                                <span class="old-price">1,950&nbsp;руб.</span>

                                            </td>
                                            <td>307</td>
                                            <td>311,148&nbsp;руб.</td>
                                            <td><button data-id="150127" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6iukfefrsa1drr4se10/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=194058705">Смартфон Apple iPhone 12 Pro Max, 256 ГБ, серебристый</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1158291</td>
                                            <td><a href="/client/seller?id=360990">ООО "Казаньэкспресс Диджитал Ритейл" (1181690055557)</a></td>
                                            <td><a href="/client/brand?id=8776014">Техника Apple</a></td>
                                            <td>10</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>
                                                103,697&nbsp;руб. <br>

                                                <span class="old-price-percent">(-14%)</span>
                                                <span class="old-price">119,980&nbsp;руб.</span>

                                            </td>
                                            <td>3</td>
                                            <td>311,091&nbsp;руб.</td>
                                            <td><button data-id="1158291" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/brss2mmpskc6i343ok80/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=165681494">Увлажнитель воздуха Xiaomi Deerma Air Humidifier DEM F600</a><br>

                                                <a class="category-link" href="/client/category?id=74616">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Бытовая техника / Климатическая техника / Очищение и увлажнение воздуха
                                                </a>


                                            </td>
                                            <td>230149</td>
                                            <td><a href="/client/seller?id=357652">ИП Валиев Азат Мансурович (321169000118515)</a></td>
                                            <td><a href="/client/brand?id=8725690">Xiaomi Mi Store</a></td>
                                            <td>96</td>
                                            <td>147</td>
                                            <td>4.4</td>
                                            <td>
                                                2,190&nbsp;руб. <br>

                                                <span class="old-price-percent">(-19%)</span>
                                                <span class="old-price">2,690&nbsp;руб.</span>

                                            </td>
                                            <td>148</td>
                                            <td>308,520&nbsp;руб.</td>
                                            <td><button data-id="230149" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6p1olm4gv9v8o5nrfl0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=194058696">Смартфон Xiaomi Redmi 9A 32Gb, зеленый</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1158399</td>
                                            <td><a href="/client/seller?id=360990">ООО "Казаньэкспресс Диджитал Ритейл" (1181690055557)</a></td>
                                            <td><a href="/client/brand?id=8782276">Xiaomi Электроника</a></td>
                                            <td>48</td>
                                            <td>2</td>
                                            <td>2.5</td>
                                            <td>
                                                7,911&nbsp;руб. <br>

                                                <span class="old-price-percent">(-12%)</span>
                                                <span class="old-price">8,980&nbsp;руб.</span>

                                            </td>
                                            <td>39</td>
                                            <td>307,451&nbsp;руб.</td>
                                            <td><button data-id="1158399" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c1m2bj1s4p4727roa8f0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=166141986">Светодиодная кольцевая цветная селфи лампа  кольцо 26 см со штативом 210 см</a><br>

                                                <a class="category-link" href="/client/category?id=74554">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Фото- и видеотехника / Оборудование для фотографов
                                                </a>


                                            </td>
                                            <td>459882</td>
                                            <td><a href="/client/seller?id=359108">ИП Хусаинов Салават Рустамович (318169000184534)</a></td>
                                            <td><a href="/client/brand?id=8747015">Skyproject</a></td>
                                            <td>62</td>
                                            <td>243</td>
                                            <td>4.7</td>
                                            <td>
                                                1,499&nbsp;руб. <br>

                                                <span class="old-price-percent">(-58%)</span>
                                                <span class="old-price">3,600&nbsp;руб.</span>

                                            </td>
                                            <td>202</td>
                                            <td>304,998&nbsp;руб.</td>
                                            <td><button data-id="459882" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6p1oa4phpvc7vjl95p0/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=194058686">Смартфон Xiaomi POCO X3 PRO, 8/256GB, черный</a><br>

                                                <a class="category-link" href="/client/category?id=74527">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>


                                            </td>
                                            <td>1158395</td>
                                            <td><a href="/client/seller?id=360990">ООО "Казаньэкспресс Диджитал Ритейл" (1181690055557)</a></td>
                                            <td><a href="/client/brand?id=8782276">Xiaomi Электроника</a></td>
                                            <td>3</td>
                                            <td>6</td>
                                            <td>5</td>
                                            <td>
                                                22,980&nbsp;руб. <br>

                                                <span class="old-price-percent">(-15%)</span>
                                                <span class="old-price">26,990&nbsp;руб.</span>

                                            </td>
                                            <td>13</td>
                                            <td>298,740&nbsp;руб.</td>
                                            <td><button data-id="1158395" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">

                                            <td></td>
                                            <td><img style="width: 30px;" src="https://image-cdn.kazanexpress.ru/c6e95gniv36k2q414ftg/t_product_240_low.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=194061520">Селфи лампа кольцо светодиодная кольцевая 26 см со штативом для съемки видео и фото 210 см</a><br>

                                                <a class="category-link" href="/client/category?id=74554">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/kazan.ico">
                                                    Электроника / Фото- и видеотехника / Оборудование для фотографов
                                                </a>


                                            </td>
                                            <td>215368</td>
                                            <td><a href="/client/seller?id=359108">ИП Хусаинов Салават Рустамович (318169000184534)</a></td>
                                            <td><a href="/client/brand?id=8747015">Skyproject</a></td>
                                            <td>25</td>
                                            <td>369</td>
                                            <td>4.7</td>
                                            <td>
                                                1,110&nbsp;руб. <br>

                                                <span class="old-price-percent">(-67%)</span>
                                                <span class="old-price">3,400&nbsp;руб.</span>

                                            </td>
                                            <td>262</td>
                                            <td>290,820&nbsp;руб.</td>
                                            <td><button data-id="215368" data-mp="kazan" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (555147)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #modal .modal-dialog {
            max-width: 900px;
        }
    </style>
    <script src="/client/js/pages/products.js?id=22"></script></div>