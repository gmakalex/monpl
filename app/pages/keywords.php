<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Топ ключевых слов</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Ключевые слова</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: left;" role="tablist" data-hint-mp="1">
                        <li><a class="nav-link active" data-field="mp=wildberries&amp;q[mp][in]" data-field-value="wildberries" data-toggle="tab" href="#tab-1">Wildberries</a></li>
                        <li><a class="nav-link" data-field="mp=ozon&amp;q[mp][in]" data-field-value="ozon" data-toggle="tab" href="#tab-1">Ozon</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="show-filters" style="margin-top: -6px;">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-12"><h3 style="margin-left: -15px;">Общее</h3></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[matches][between]">
                                                    <div class="col-lg-5"><strong id="change-matches-text">Рекомендации</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_products_count][between]">
                                                    <div class="col-lg-4"><strong>Товаров</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_brands_count][between]">
                                                    <div class="col-lg-5"><strong>Брендов</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_categories_count][between]">
                                                    <div class="col-lg-5"><strong>Категорий</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">

                                            </div>
                                            <div class="col-lg-4">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12"><h3 style="margin-left: -15px;margin-top: 25px;"><span style="display: inline-block;"> Анализ первых 500-1000 товаров <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="По ключевым словам анализируются первые 500-1000 товаров в поисковой выдаче. Ключей, имеющих более 500 товаров, менее 30%, они высоко-конкурентные, поэтому анализировать весь объем не рационально. (Пример: у ключа 10.000 товаров, вы получаете данные по первым 500-1000 товарам, они составляют основную долю рынка по этому ключевому запросу)">?</span></span> </h3></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4"><strong>Всего товаров</strong></div>
                                            <div class="col-lg-4"><strong>Товаров с продажами</strong></div>
                                            <div class="col-lg-4"><strong>Товаров с продажами %</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" id="filter-analyzed_products_count" data-field="q[analyzed_products_count][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_products_with_sales][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_products_percent][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"><strong>Всего продавцов</strong></div>
                                            <div class="col-lg-4"><strong>Продавцов с продажами</strong></div>
                                            <div class="col-lg-4"><strong>Продавцов с продажами %</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" id="filter-analyzed_sellers_count" data-field="q[analyzed_sellers_count][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_sellers_with_sales_count][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_sellers_percent][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"><strong>Всего брендов</strong></div>
                                            <div class="col-lg-4"><strong>Брендов с продажами</strong></div>
                                            <div class="col-lg-4"><strong>Брендов с продажами %</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" id="filter-analyzed_brands_count" data-field="q[analyzed_brands_count][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_brands_with_sales_count][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[analyzed_brands_percent][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4"><strong>Цена</strong></div>
                                            <div class="col-lg-4"><strong>Продаж (шт.)</strong></div>
                                            <div class="col-lg-4"><strong>Оборот (руб.)</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[avg_price][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" id="filter-total_sales" data-field="q[total_sales][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" id="filter-total_sum" data-field="q[total_sum][between]">
                                                    <div class="col-lg-12">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">

                                            </div>
                                            <div class="col-lg-4">

                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Ключевое слово</th>
                                            <th class="sorting" data-sort="total_products_count">Товаров <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Среднее количество товаров в выдаче по этому ключу">?</span></th>
                                            <th class="sorting" data-state="-matches" data-sort="matches">Рекомендаций <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество вхождений этого ключевого слова в рекомендации к другим запросам">?</span></th>
                                            <th class="sorting" data-sort="total_categories_count">Категории <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество категорий, которое может назначиться на товар, по данному ключевому слову">?</span></th>
                                            <th class="sorting" data-sort="analyzed_products_count">В анализе <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="По ключевым словам анализируются первые 500-1000 товаров в поисковой выдаче. Ключей, имеющих более 500 товаров, менее 30%, они высоко-конкурентные, поэтому анализировать весь объем не рационально. (Пример: у ключа 10.000 товаров, вы получаете данные по первым 500-1000 товарам, они составляют основную долю рынка по этому ключевому запросу)">?</span></th>
                                            <th class="sorting" data-sort="analyzed_products_percent">SPP <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Процент товаров, которые участвовали в анализе, имеющих продажи">?</span></th>
                                            <th class="sorting" data-sort="avg_price">Ср. чек <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Средняя цена продажи">?</span></th>
                                            <th class="sorting" data-sort="total_sales">Продажи <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество продаж товаров, участвующих в анализе">?</span></th>
                                            <th class="sorting" data-sort="total_sum">Оборот <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Объем продаж товаров, участвующих в анализе">?</span></th>
                                            <th class="sorting" data-sort="analyzed_ttp">TTP <span class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Оборот на 1 товар">?</span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=368" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> ботинки женские</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=ботинки женские&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>91599
                                                <sup><span class="increase">⬆2.9%</span></sup>
                                            </td>
                                            <td>84647
                                                <sup><span class="decrease">⬇-28.1%</span></sup>
                                            </td>
                                            <td>2</td>
                                            <td>573</td>
                                            <td>84.25</td>
                                            <td>5,103.88
                                                <sup><span class="increase">⬆1.45%</span></sup>
                                            </td>
                                            <td>
                                                15116
                                            </td>
                                            <td>
                                                61,166,697
                                                <sup><span class="decrease">⬇-19.97%</span></sup>

                                            </td>
                                            <td>
                                                106,748
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=57" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> джинсы женские</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=джинсы женские&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>38892
                                                <sup><span class="decrease">⬇-0.18%</span></sup>
                                            </td>
                                            <td>68403
                                                <sup><span class="decrease">⬇-21%</span></sup>
                                            </td>
                                            <td>2</td>
                                            <td>561</td>
                                            <td>87.22</td>
                                            <td>2,140.96
                                                <sup><span class="decrease">⬇-5.63%</span></sup>
                                            </td>
                                            <td>
                                                33208
                                            </td>
                                            <td>
                                                60,581,256
                                                <sup><span class="decrease">⬇-13.8%</span></sup>

                                            </td>
                                            <td>
                                                107,987
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=933" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> куртка зимняя женская</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=куртка зимняя женская&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>84085
                                                <sup><span class="increase">⬆10.12%</span></sup>
                                            </td>
                                            <td>63038
                                                <sup><span class="decrease">⬇-19.46%</span></sup>
                                            </td>
                                            <td>178</td>
                                            <td>542</td>
                                            <td>82.52</td>
                                            <td>5,700.68
                                                <sup><span class="increase">⬆3.7%</span></sup>
                                            </td>
                                            <td>
                                                19022
                                            </td>
                                            <td>
                                                66,221,891
                                                <sup><span class="decrease">⬇-17.02%</span></sup>

                                            </td>
                                            <td>
                                                122,180
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=54" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> ноутбук</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=ноутбук&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>1050
                                                <sup><span class="increase">⬆2.84%</span></sup>
                                            </td>
                                            <td>47677
                                                <sup><span class="increase">⬆5.86%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>387</td>
                                            <td>8.57</td>
                                            <td>87,179.38
                                                <sup><span class="decrease">⬇-2.06%</span></sup>
                                            </td>
                                            <td>
                                                459
                                            </td>
                                            <td>
                                                17,623,504
                                                <sup><span class="decrease">⬇-72.22%</span></sup>

                                            </td>
                                            <td>
                                                45,538
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=52" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> платье</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=платье&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>321280
                                                <sup><span class="increase">⬆0.03%</span></sup>
                                            </td>
                                            <td>47439
                                                <sup><span class="increase">⬆33.71%</span></sup>
                                            </td>
                                            <td>7</td>
                                            <td>577</td>
                                            <td>94.49</td>
                                            <td>2,822.75
                                                <sup><span class="increase">⬆2.27%</span></sup>
                                            </td>
                                            <td>
                                                53906
                                            </td>
                                            <td>
                                                95,675,364
                                                <sup><span class="decrease">⬇-16.08%</span></sup>

                                            </td>
                                            <td>
                                                165,815
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=938" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> пуховик женский зимний длинный</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=пуховик женский зимний длинный&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>27877
                                                <sup><span class="decrease">⬇-6.7%</span></sup>
                                            </td>
                                            <td>44496
                                                <sup><span class="decrease">⬇-21.35%</span></sup>
                                            </td>
                                            <td>81</td>
                                            <td>565</td>
                                            <td>66.2</td>
                                            <td>10,270.74
                                                <sup><span class="decrease">⬇-0.69%</span></sup>
                                            </td>
                                            <td>
                                                4990
                                            </td>
                                            <td>
                                                38,890,661
                                                <sup><span class="decrease">⬇-37.32%</span></sup>

                                            </td>
                                            <td>
                                                68,833
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=855" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> айфон</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=айфон&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>82
                                                <sup><span class="decrease">⬇-7.87%</span></sup>
                                            </td>
                                            <td>33634
                                                <sup><span class="decrease">⬇-4.09%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>93</td>
                                            <td>41.87</td>
                                            <td>72,790.67
                                                <sup><span class="increase">⬆1.86%</span></sup>
                                            </td>
                                            <td>
                                                967
                                            </td>
                                            <td>
                                                59,486,377
                                                <sup><span class="decrease">⬇-28.81%</span></sup>

                                            </td>
                                            <td>
                                                639,638
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=11668" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> iphone</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=iphone&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>83
                                                <sup><span class="decrease">⬇-6.74%</span></sup>
                                            </td>
                                            <td>30606
                                                <sup><span class="decrease">⬇-1.18%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>94</td>
                                            <td>41.55</td>
                                            <td>72,770.02
                                                <sup><span class="increase">⬆1.78%</span></sup>
                                            </td>
                                            <td>
                                                967
                                            </td>
                                            <td>
                                                59,486,377
                                                <sup><span class="decrease">⬇-28.24%</span></sup>

                                            </td>
                                            <td>
                                                632,833
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=315" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> брюки женские</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=брюки женские&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>81254
                                                <sup><span class="decrease">⬇-0.04%</span></sup>
                                            </td>
                                            <td>29791
                                                <sup><span class="decrease">⬇-27.44%</span></sup>
                                            </td>
                                            <td>11</td>
                                            <td>581</td>
                                            <td>91.33</td>
                                            <td>2,146.42
                                                <sup><span class="decrease">⬇-1.52%</span></sup>
                                            </td>
                                            <td>
                                                41358
                                            </td>
                                            <td>
                                                59,709,713
                                                <sup><span class="decrease">⬇-21.85%</span></sup>

                                            </td>
                                            <td>
                                                102,770
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <a href="/client/keyword?id=5434" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> видеокарта</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=видеокарта&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>397
                                                <sup><span class="increase">⬆2.58%</span></sup>
                                            </td>
                                            <td>29199
                                                <sup><span class="decrease">⬇-25.2%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>313</td>
                                            <td>6.02</td>
                                            <td>91,504.47
                                                <sup><span class="decrease">⬇-15.52%</span></sup>
                                            </td>
                                            <td>
                                                247
                                            </td>
                                            <td>
                                                4,752,403
                                                <sup><span class="increase">⬆121.21%</span></sup>

                                            </td>
                                            <td>
                                                15,183
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=6285" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> смартфон</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=смартфон&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>2667
                                                <sup><span class="decrease">⬇-3.16%</span></sup>
                                            </td>
                                            <td>27789
                                                <sup><span class="decrease">⬇-16.51%</span></sup>
                                            </td>
                                            <td>2</td>
                                            <td>545</td>
                                            <td>53.73</td>
                                            <td>20,273.27
                                                <sup><span class="decrease">⬇-2.33%</span></sup>
                                            </td>
                                            <td>
                                                12409
                                            </td>
                                            <td>
                                                204,870,364
                                                <sup><span class="decrease">⬇-10.79%</span></sup>

                                            </td>
                                            <td>
                                                375,908
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=518" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> термобелье мужское</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=термобелье мужское&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>8311
                                                <sup><span class="increase">⬆0.92%</span></sup>
                                            </td>
                                            <td>27758
                                                <sup><span class="decrease">⬇-7.91%</span></sup>
                                            </td>
                                            <td>13</td>
                                            <td>536</td>
                                            <td>73.17</td>
                                            <td>1,813.34
                                                <sup><span class="increase">⬆1.53%</span></sup>
                                            </td>
                                            <td>
                                                13352
                                            </td>
                                            <td>
                                                17,815,614
                                                <sup><span class="decrease">⬇-18.69%</span></sup>

                                            </td>
                                            <td>
                                                33,238
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=7009" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> сапоги женские</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=сапоги женские&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>40735
                                                <sup><span class="increase">⬆1.36%</span></sup>
                                            </td>
                                            <td>27182
                                                <sup><span class="decrease">⬇-29.17%</span></sup>
                                            </td>
                                            <td>10</td>
                                            <td>575</td>
                                            <td>82.84</td>
                                            <td>5,207.08
                                                <sup><span class="decrease">⬇-0.15%</span></sup>
                                            </td>
                                            <td>
                                                15583
                                            </td>
                                            <td>
                                                49,021,960
                                                <sup><span class="decrease">⬇-14.38%</span></sup>

                                            </td>
                                            <td>
                                                85,255
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=11686" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> телевизор smart tv</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=телевизор smart tv&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>516
                                                <sup><span class="increase">⬆8.18%</span></sup>
                                            </td>
                                            <td>22584
                                                <sup><span class="decrease">⬇-4.35%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>490</td>
                                            <td>7.93</td>
                                            <td>72,659.41
                                                <sup><span class="increase">⬆0.15%</span></sup>
                                            </td>
                                            <td>
                                                883
                                            </td>
                                            <td>
                                                27,680,051
                                                <sup><span class="decrease">⬇-17.01%</span></sup>

                                            </td>
                                            <td>
                                                56,489
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=51" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> платье нарядное вечернее женское</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=платье нарядное вечернее женское&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>54179
                                                <sup><span class="decrease">⬇-0.33%</span></sup>
                                            </td>
                                            <td>21153
                                                <sup><span class="increase">⬆21.47%</span></sup>
                                            </td>
                                            <td>25</td>
                                            <td>579</td>
                                            <td>90.45</td>
                                            <td>3,195.71
                                                <sup><span class="decrease">⬇-4.77%</span></sup>
                                            </td>
                                            <td>
                                                32242
                                            </td>
                                            <td>
                                                78,673,468
                                                <sup><span class="decrease">⬇-21.22%</span></sup>

                                            </td>
                                            <td>
                                                135,878
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=19826" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> айфон 12</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=айфон 12&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>28
                                                <sup><span class="decrease">⬇-3.45%</span></sup>
                                            </td>
                                            <td>19351
                                                <sup><span class="increase">⬆0.45%</span></sup>
                                            </td>
                                            <td>1</td>
                                            <td>30</td>
                                            <td>53.22</td>
                                            <td>63,058.81
                                                <sup><span class="increase">⬆1.16%</span></sup>
                                            </td>
                                            <td>
                                                257
                                            </td>
                                            <td>
                                                15,956,380
                                                <sup><span class="decrease">⬇-43.81%</span></sup>

                                            </td>
                                            <td>
                                                531,879
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=161" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> кроссовки мужские</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=кроссовки мужские&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>29125
                                                <sup><span class="increase">⬆1.6%</span></sup>
                                            </td>
                                            <td>16708
                                                <sup><span class="decrease">⬇-9.29%</span></sup>
                                            </td>
                                            <td>5</td>
                                            <td>577</td>
                                            <td>89.53</td>
                                            <td>4,055.59
                                                <sup><span class="decrease">⬇-1.8%</span></sup>
                                            </td>
                                            <td>
                                                31122
                                            </td>
                                            <td>
                                                97,706,467
                                                <sup><span class="decrease">⬇-28.77%</span></sup>

                                            </td>
                                            <td>
                                                169,335
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=11539" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> наушники беспроводные</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=наушники беспроводные&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>13845
                                                <sup><span class="decrease">⬇-16.47%</span></sup>
                                            </td>
                                            <td>13306
                                                <sup><span class="decrease">⬇-7.51%</span></sup>
                                            </td>
                                            <td>4</td>
                                            <td>551</td>
                                            <td>27.91</td>
                                            <td>4,449.18
                                                <sup><span class="increase">⬆15.54%</span></sup>
                                            </td>
                                            <td>
                                                20340
                                            </td>
                                            <td>
                                                29,270,872
                                                <sup><span class="decrease">⬇-24.3%</span></sup>

                                            </td>
                                            <td>
                                                53,123
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=28765" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> шуруповерт аккумуляторный</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=шуруповерт аккумуляторный&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>1954
                                                <sup><span class="decrease">⬇-0.41%</span></sup>
                                            </td>
                                            <td>12758
                                                <sup><span class="decrease">⬇-7.46%</span></sup>
                                            </td>
                                            <td>19</td>
                                            <td>570</td>
                                            <td>11.87</td>
                                            <td>6,275.85
                                                <sup><span class="increase">⬆1.59%</span></sup>
                                            </td>
                                            <td>
                                                2973
                                            </td>
                                            <td>
                                                13,142,092
                                                <sup><span class="decrease">⬇-19.92%</span></sup>

                                            </td>
                                            <td>
                                                23,056
                                            </td>
                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">
                                            <td>
                                                <a href="/client/keyword?id=95938" target="_blank"><img style="width: 14px;margin-top: -3px;" src="/images/wildberries.ico"> iphone 13</a> &nbsp;&nbsp;
                                                <a style="color: #32b797;float: right;" href="https://www.wildberries.ru/catalog/0/search.aspx?search=iphone 13&amp;xsearch=true" target="_blank"><i class="fa fa-external-link"></i></a>
                                            </td>
                                            <td>26299
                                                <sup><span class="increase">⬆4.49%</span></sup>
                                            </td>
                                            <td>12473
                                                <sup><span class="decrease">⬇-5.66%</span></sup>
                                            </td>
                                            <td>30</td>
                                            <td>569</td>
                                            <td>13.78</td>
                                            <td>7,694.06
                                                <sup><span class="decrease">⬇-0.04%</span></sup>
                                            </td>
                                            <td>
                                                3074
                                            </td>
                                            <td>
                                                44,307,022
                                                <sup><span class="decrease">⬇-20.14%</span></sup>

                                            </td>
                                            <td>
                                                77,868
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (73870)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?=PROJECT_URL?>client/js/pages/keywords.js?id=23"></script></div>