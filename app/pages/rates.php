<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Тарифы</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">
                <a href="<?=PROJECT_URL?>client/dashboard">Главная</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Тарифы</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div id="get-extra-days" class="ibox-content" style="display: none;">
    <div class="sk-spinner sk-spinner-wave">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
    <div class="row">
        <h2>Пройдите обучение и получите +6 дней премиум доступа в подарок!</h2>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="extra-video active" data-days="1" data-lesson="40129654" data-step="0" data-video-id="lw9KLm1C0mU">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 1</p>
                    <span>Обратный поиск товаров</span>
                    <div class="extra-video-label">Урок не пройден</div>
                    <div class="extra-video-play active"></div>
                </div>
            </div>
            <div class="col">
                <div class="extra-video" data-days="1" data-lesson="40129657" data-step="1" data-video-id="pIPzI2KxCcA">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 2</p>
                    <span>Анализ ниш на маркетплейсах</span>
                    <div class="extra-video-label">Урок не пройден</div>
                    <div class="extra-video-play"></div>
                </div>
            </div>
            <div class="col">
                <div class="extra-video" data-days="1" data-lesson="40129669" data-step="2" data-video-id="gz8dw0Sfx3s">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 3</p>
                    <span>Выявление хайповых товаров на маркетплейсах</span>
                    <div class="extra-video-label">Урок не пройден</div>
                    <div class="extra-video-play"></div>
                </div>
            </div>
            <div class="col">
                <div class="extra-video" data-days="1" data-lesson="40129672" data-step="3" data-video-id="hjpdOSwQ0es">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 4</p>
                    <span>Аналитика категорий на маркетплейсах</span>
                    <div class="extra-video-label">Урок не пройден</div>
                    <div class="extra-video-play"></div>
                </div>
            </div>
            <div class="col">
                <div class="extra-video" data-days="1" data-lesson="40129675" data-step="4" data-video-id="9a9F3tqpJoo">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 5</p>
                    <span>Конкурентный анализ</span>
                    <div class="extra-video-label">Урок не пройден</div>
                    <div class="extra-video-play"></div>
                </div>
            </div>
            <div class="col">
                <div class="extra-video" data-days="6" data-lesson="40193338" data-step="5" data-video-id="ZRc2Bwwwl18">
                    <img src="/client/img/extra-days-preview.png" alt="">
                    <p>Урок 6</p>
                    <span>API кабинет и аналитика собственного магазина</span>
                    <div class="extra-video-label"><img src="/client/img/video-gift.svg" alt=""> + 6 дней после прохождения!</div>
                    <div class="extra-video-play"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="close-extra-video"><img src="/client/img/video-close.svg" alt=""></div>
</div>

<div class="wrapper wrapper-content" id="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content text-center p-md">
                    <h1>Тариф <span style="font-weight: 600;font-family: 'SuisseIntl', sans-serif;color: #35404F;font-size: 24px;">"Все включено"</span></h1>
                    <h2>Вы подбираете товар в сервисе, а мы регистрируем вас на любом маркетплейсе, ведем документацию, помогаем загрузить товар на склад, оформляем карточку товара и раскручиваем ваш магазин!</h2>
                    <button style="width: 290px;font-size: 16px;" type="button" class="btn btn-outline btn-primary">Заведите меня на маркетплейс</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 wow zoomIn">
            <div class="ibox ">
                <div class="ibox-content">
                    <h1 style="text-align: center; display: block;">Выберите тариф, <br> который подойдет лучше всего!</h1>

                    <div class="row rate-periods">
                        <div class="col"><i style="font-style: normal;display: block;margin-top: 20px;">Период оплаты</i></div>
                        <div class="col gray-bg-1 gray-bg-rounded-left"><div class="rate-days" data-days="30"><strong>Ежемесячно</strong> <!--<br /> <span>Стандартная цена</span>--></div></div>
                        <div class="col gray-bg-1"><div class="rate-days" data-days="92"><strong>За 3 месяца</strong> <!--<br /> <span style="color: #2D9CDB">Экономия 15%</span>--></div></div>
                        <div class="col gray-bg-1"><div class="rate-days" data-days="183"><strong>За 6 месяцев</strong> <!--<br /> <span style="color: #BB6BD9">Экономия 30%</span>--></div></div>
                        <div class="col gray-bg-1 gray-bg-rounded-right"><div class="rate-days active" data-days="365"><strong>За год</strong> <!--<br /> <span style="color: #EB5757">Экономия 50%</span>--></div></div>
                    </div>

                    <div class="price-wrapper">
                        <div class="price-titles">
                            <div class="price-titles-item">Пользователи</div>
                            <div class="price-titles-item">Период анализа</div>
                            <div class="price-titles-item">Новинки</div>
                            <div class="price-titles-item">Персональный аналитик</div>
                            <div class="price-titles-item">Smart анализ ниш</div>
                            <div class="price-titles-item">API аналитика</div>
                        </div>
                        <div class="price-item-blocks">
                            <div class="price-item-wrapper">
                                <div class="price-item price-item-1">
                                    <h3 class="price-item-name">Lite</h3>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">1</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value">последние 7 дней</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Первые три товара</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span id="rate-3">47 880 руб.</span><br><span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="3" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-2">
                                    <h3 class="price-item-name">Gold</h3>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">2</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value">последние 14 дней</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span id="rate-2">167 880 руб.</span><br> <span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="2" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-3">
                                    <h3 class="price-item-name">Premium</h3>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">3</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value" style="margin-top: -10px;">90 дней с ноября 2020 года</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span id="rate-1">239 880 руб.</span><br> <span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="1" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-4">
                                    <h3 class="price-item-name">Professional</h3>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">7</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value" style="margin-top: -10px;">365 дней с ноября 2020 года</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <p class="price-item-info-value">до 50 товаров</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span id="rate-4">1 079 880 руб.</span><br> <span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="8" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 wow zoomIn">
            <h1 style="margin-bottom: 40px;">Полное сравнение тарифов</h1>
            <div class="ibox1 ">
                <div class="ibox-content1">
                    <div class="price-wrapper">
                        <div class="price-titles">
                            <div class="price-titles-item"><h3>АНАЛИТИКА РЫНКА</h3></div>
                            <div class="price-titles-item">Выдача</div>
                            <div class="price-titles-item">Период анализа</div>
                            <div class="price-titles-item">Выгрузка</div>
                            <div class="price-titles-item">Запросов</div>
                            <div class="price-titles-item">Топ продаваемых товаров</div>
                            <div class="price-titles-item">Новинки</div>
                            <div class="price-titles-item">Пользователи</div>
                            <div class="price-titles-item">Персональный аналитик</div>
                            <div class="price-titles-item">Smart анализ ниш</div>
                            <div class="price-titles-item">Аналитика категорий</div>
                            <div class="price-titles-item">Аналитика продавцов</div>
                            <div class="price-titles-item">Аналитика брендов</div>
                            <div class="price-titles-item">Отслеживание</div>
                            <div class="price-titles-item" style="height: 136px;line-height: 68px;">Доступные маркетплейсы</div>
                            <div class="price-titles-item">
                                <h4 style="font-size: 14px;">АНАЛИТИКА СВОЕГО МАГАЗИНА ПО API <br> <span style="font-size: 13px;">(Подключение бесплатно!)</span></h4>
                            </div>
                            <div class="price-titles-item">ABC Анализ</div>
                            <div class="price-titles-item">XYZ Анализ</div>
                            <div class="price-titles-item">Расчет прибыльности</div>
                            <div class="price-titles-item">Расчет поставок</div>
                            <div class="price-titles-item">Финансовая аналитика</div>
                            <div class="price-titles-item">Капитализация склада</div>
                            <div class="price-titles-item">Количество магазинов</div>
                        </div>
                        <div class="price-item-blocks">
                            <div class="price-item-wrapper">
                                <div class="price-item price-item-1">
                                    <h3 class="price-item-name">Lite</h3>
                                    <div class="price-item-info">
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выдача</p>
                                        <p class="price-item-info-value">ТОП 20 результатов</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value">последние 7 дней</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выгрузка</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Запросов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Топ продаваемых товаров</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Первые три товара</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">1</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика категорий</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика продавцов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика брендов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Отслеживание</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info price-item-info-shops">
                                        <p class="price-item-info-title">Доступные маркетплейсы</p>
                                        <ul class="price-item-info-shops-list">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">ABC Анализ</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">XYZ Анализ</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет прибыльности</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет поставок</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Финансовая аналитика</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Капитализация склада</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Количество магазинов</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span>47 880 руб.</span><br> <span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="3" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-2">
                                    <h3 class="price-item-name">Gold</h3>
                                    <div class="price-item-info">
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выдача</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value">последние 14 дней</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выгрузка</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Запросов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Топ продаваемых товаров</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">2</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика категорий</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика продавцов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика брендов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Отслеживание</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info price-item-info-shops">
                                        <p class="price-item-info-title">Доступные маркетплейсы</p>
                                        <ul class="price-item-info-shops-list">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2022/01/icon-3.png" class="attachment-full size-full" alt=""> </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/ym.png" class="attachment-full size-full" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <ul class="price-item-info-shops-list" style="margin-top: -6px;">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">ABC Анализ</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">XYZ Анализ&lt;</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет прибыльности</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет поставок</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Финансовая аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Капитализация склада</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Количество магазинов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span>167 880 руб.</span><br><span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="2" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-3">
                                    <h3 class="price-item-name">Premium</h3>
                                    <div class="price-item-info">
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выдача</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value" style="margin-top: -10px;">90 дней с ноября 2020 года</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выгрузка</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Запросов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Топ продаваемых товаров</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">3</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <div class="price-item-info-cross"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика категорий</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика продавцов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика брендов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Отслеживание</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info price-item-info-shops">
                                        <p class="price-item-info-title">Доступные маркетплейсы</p>
                                        <ul class="price-item-info-shops-list">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2022/01/icon-3.png" class="attachment-full size-full" alt=""> </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/ym.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2022/01/sber-1.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/12/frame-1-1.png" class="attachment-full size-full" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <ul class="price-item-info-shops-list" style="margin-top: -6px;">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">ABC Анализ</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">XYZ Анализ</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет прибыльности</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет поставок</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Финансовая аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Капитализация склада</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Количество магазинов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span>239 880 руб.</span><br><span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="1" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                                <div class="price-item price-item-4">
                                    <h3 class="price-item-name">Professional</h3>
                                    <div class="price-item-info">
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выдача</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Период анализа</p>
                                        <p class="price-item-info-value" style="margin-top: -10px;">365 дней с ноября 2020 года</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Выгрузка</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Запросов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Топ продаваемых товаров</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Новинки</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Пользователи</p>
                                        <p class="price-item-info-value">7</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Персональный аналитик</p>
                                        <p class="price-item-info-value">до 50 товаров</p>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Анализ ниш</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика категорий</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика продавцов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Аналитика брендов</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Отслеживание</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info price-item-info-shops">
                                        <p class="price-item-info-title">Доступные маркетплейсы</p>
                                        <ul class="price-item-info-shops-list">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2022/01/icon-3.png" class="attachment-full size-full" alt=""> </li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/ym.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2022/01/sber-1.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/12/frame-1-1.png" class="attachment-full size-full" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">API аналитика</p>
                                        <ul class="price-item-info-shops-list" style="margin-top: -6px;">
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1wb.png" class="attachment-full size-full" alt=""></li>
                                            <li class="price-item-info-shops-list-item"><img width="40" height="40" src="https://moneyplace.io/wp-content/uploads/2021/11/Property-1ozon.png" class="attachment-full size-full" alt="">   </li>
                                        </ul>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">ABC Анализ</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">XYZ Анализ</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет прибыльности</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Расчет поставок</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Финансовая аналитика</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Капитализация склада</p>
                                        <div class="price-item-info-check"></div>
                                    </div>
                                    <div class="price-item-info">
                                        <p class="price-item-info-title">Количество магазинов</p>
                                        <p class="price-item-info-value">Без ограничений</p>
                                    </div>
                                    <div class="price-item-bottom">
                                        <div class="price-item-cost"><span>1 079 880 руб.</span><br><span style="font-size: 15px;color: #9299A2;">за 12 месяцев</span></div>
                                        <button class="btn btn-primary" data-id="8" data-total-days="365">Оплатить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?=PROJECT_URL?>js/libs/bootstrap/bootstrap-datepicker.js"></script>
<link href="<?=PROJECT_URL?>js/libs/bootstrap/bootstrap-datepicker.css" rel="stylesheet">
<script src="<?=PROJECT_URL?>client/js/in/rates.js?id=17"></script></div>