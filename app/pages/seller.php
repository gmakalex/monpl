<div id="page-content" style=""><div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 class="page-name">ВАЙЛДБЕРРИЗ ООО (1067746062449)</h2>
            <p style="margin-top: -10px;" class="category-path"></p>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href="/client/dashboard">Главная</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/client/sellers">Продавцы</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Продавец</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content" id="content">
        <div class="row to-toggle-loader">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Выручка</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="turnover">3,515,432,993</h1>
                        <div class="stat-percent font-bold text-navy" id="turnover-diff">3% <i class="fa fa-level-up"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Продаж</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="sales">2,799,428</h1>
                        <div class="stat-percent font-bold text-danger" id="sales-diff">5% <i class="fa fa-level-down"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Средний чек</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="bill">1,256</h1>
                        <div class="stat-percent font-bold text-navy" id="bill-diff">9% <i class="fa fa-level-up"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Остатки</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <h1 class="no-margins" id="amount">24,911,368</h1>
                        <div class="stat-percent font-bold text-danger" id="amount-diff">1% <i class="fa fa-level-down"></i></div>
                        <small>Разница со вчерашним днем:</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div data-toggle="buttons" class="btn-group btn-group-toggle date-selector" style="float: left;">
                    <span>Выберите период:</span>
                    <label class="btn btn-sm btn-white active"> <input type="radio" checked="" id="option1" name="period" value="week"> 7 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period" value="two_weeks"> 14 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period" value="month"> 30 дн. </label>
                    <label class="btn btn-sm btn-white"> <input type="radio" id="option4" name="period" value="period"> Период </label>
                </div>
                <div class="form-group" style="display: none;float: left;margin-left: 9px;margin-top: 2px;">
                    <input type="text" id="date" name="q[date][between]" class="form-control">
                </div>
            </div>
        </div>

        <diw class="row to-toggle-loader">
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Состояние склада</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 332px; height: 300px;" id="chart-amount" height="300" width="332" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Динамика продаж</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                            <canvas style="margin: 0px auto; display: block; width: 332px; height: 300px;" id="chart-price" height="300" width="332" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </diw>

        <diw class="row to-toggle-loader">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h4 class="font-bold no-margins">Распределение числа продаж от цены на товар</h4>
                    </div>
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-wave">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div>
                            <canvas style="margin: 0px auto; display: block; box-sizing: border-box; width: 733px; height: 400px;" id="chart-prices2" height="400" width="733"></canvas>
                        </div>
                        <div style="margin-left: 65px;">
                            <input type="hidden" id="price-from" value="0">
                            <input type="hidden" id="price-to" value="0">
                            <div id="slider-range" style="margin-bottom: 15px;" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span></div>
                            <p><strong>Выбранный диапазон цен:</strong> <span id="amount-range">11 руб. - 274990 руб.</span> <button id="apply-prices" type="button" class="btn btn-outline btn-primary">Показать товары</button></p>
                        </div>
                    </div>
                </div>
            </div>
        </diw>



        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" style="float: right" role="tablist">
                        <li data-placement="top" data-original-title="Товар, проданный через площадку маркетплейса">
                            <a class="nav-link active" data-field="type" data-field-value="fbo" data-toggle="tab" href="#tab-1">FBO</a>
                        </li>
                        <li data-toggle="tooltip" data-placement="top" data-original-title="Товар, проданный через склад поставщика">
                            <a class="nav-link" data-toggle="tab" data-field="type" data-field-value="fbs" href="#tab-1">FBS</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body ibox-content">
                                <div class="sk-spinner sk-spinner-wave">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
                                <div class="table-responsive">
                                    <ul class="only-new">
                                        <li><strong>Только новинки</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="is_new" class="onoffswitch-checkbox" id="example1">
                                                    <label class="onoffswitch-label" for="example1">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <ul class="show-filters">
                                        <li><strong>Фильтры и выгрузка</strong></li>
                                        <li>
                                            <div class="switch" style="">
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="" class="onoffswitch-checkbox" id="example2">
                                                    <label class="onoffswitch-label" for="example2">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <form action="" class="custom-filters" id="product-filters" style="display: none;">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[turnover][between]">
                                                    <div class="col-lg-5"><strong>Оборот</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_days_in_period][between]">
                                                    <div class="col-lg-4"><strong>Период<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих запись о товаре">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[rating][between]">
                                                    <div class="col-lg-5"><strong>Рейтинг</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[loses][between]">
                                                    <div class="col-lg-5"><strong>Упущенный оброт <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка вызванная обнулением остатков">?</span></strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[last_amount][between]">
                                                    <div class="col-lg-4"><strong>Остаток</strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[reviews][between]">
                                                    <div class="col-lg-5"><strong>Отзывы</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[total_sales][between]">
                                                    <div class="col-lg-5"><strong>Кол-во продаж</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[days_with_sales][between]">
                                                    <div class="col-lg-4"><strong>Продажи <span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Количество дней за выбранный период, имеющих продажи">?</span></strong></div>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row" data-field="q[avg_position][between]">
                                                    <div class="col-lg-5" style="line-height: 0px;"><strong>Позиция в категории</strong></div>
                                                    <div class="col-lg-7">
                                                        <input type="number" class="form-control" placeholder="От">
                                                        <input type="number" class="form-control" placeholder="До">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-warning" data-type="export" style="float: right;margin-top: 10px; margin-left: 10px;"><i class="fa fa-download"></i></button>
                                                <button class="btn btn-primary" data-type="filter" style="float: right;margin-top: 10px;">Применить</button>
                                            </div>
                                        </div>
                                    </form>
                                    <table id="model-table" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Фото</th>
                                            <th>Название</th>
                                            <th>SKU</th>
                                            <th>Бренд</th>
                                            <th>Наличие</th>
                                            <th>Цена</th>
                                            <th style="width: 65px;">ACP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Средняя позиция в категории">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="loses">LP<span style="float: none;" class="information-tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Упущенная прогнозируемая выручка, вызванная обнулением остатков">?</span></th>
                                            <th class="sorting" style="min-width: 95px;" data-sort="total_sales">Продажи</th>
                                            <th class="sorting" style="min-width: 95px;" data-state="-turnover" data-sort="turnover">Оборот</th>
                                            <th>Тренд</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>


                                            <td>
                                                <a href="" data-id="6496573" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="16023994">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/16020000/16023994-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6496573">Смартфон iPhone 11 128GB (новая комплектация)</a><br>

                                                <a class="category-link" href="/client/category?id=5630">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Android
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1114)</span>
                                                </div>
                                            </td>
                                            <td>16023994</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=90083">Apple</a>
                                            </td>
                                            <td>18</td>
                                            <td>
                                                46,741&nbsp;руб. <br>

                                                <span class="old-price-percent">(-15%)</span>
                                                <span class="old-price">54,990&nbsp;руб.</span>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>546</td>
                                            <td>26,015,496&nbsp;руб.</td>
                                            <td><span id="sparkline6496573"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="16023994" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="6551252" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="11878555">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/11870000/11878555-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6551252">Телевизор UE50TU7100UXRU,50",UHD, Smart TV, Wi-Fi, DVB-T2/C/S2</a><br>

                                                <a class="category-link" href="/client/category?id=5723">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / ТВ, Аудио, Фото, Видео техника / Телевизоры
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(130)</span>
                                                </div>
                                            </td>
                                            <td>11878555</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                41,151&nbsp;руб. <br>

                                                <span class="old-price-percent">(-16%)</span>
                                                <span class="old-price">48,990&nbsp;руб.</span>

                                            </td>
                                            <td>246</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>399</td>
                                            <td>16,419,249&nbsp;руб.</td>
                                            <td><span id="sparkline6551252"><canvas width="4" height="40" style="display: inline-block; width: 4px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="11878555" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="22290670" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="18697857">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/18690000/18697857-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=22290670">Картон</a><br>

                                                <a class="category-link" href="/client/category?id=8867">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Канцтовары / Бумажная продукция
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 0%;"></div>
                                                    </div>
                                                    <span class="comments-count">(0)</span>
                                                </div>
                                            </td>
                                            <td>18697857</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=75647">wildberries</a>
                                            </td>
                                            <td>2369</td>
                                            <td>
                                                10,668&nbsp;руб. <br>

                                            </td>
                                            <td>126</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>1159</td>
                                            <td>12,364,212&nbsp;руб.</td>
                                            <td><span id="sparkline22290670"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="18697857" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="9660370" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13615125">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13610000/13615125-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9660370">Смартфон Redmi 9A 2/32Gb: 6.53" 1600x720/IPS Helio G25 2Gb/32Gb 13Mp/5Mp 5000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5635">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Xiaomi
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(2703)</span>
                                                </div>
                                            </td>
                                            <td>13615125</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>3748</td>
                                            <td>
                                                7,120&nbsp;руб. <br>

                                                <span class="old-price-percent">(-20%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>2</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>1482</td>
                                            <td>11,057,835&nbsp;руб.</td>
                                            <td><span id="sparkline9660370"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13615125" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="5282047" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13612382">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13610000/13612382-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=5282047">Кроссовки VL COURT 2.0  CBLACK/FTWWHT/GRESIX</a><br>

                                                <a class="category-link" href="/client/category?id=3972">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Мужчинам / Обувь
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(413)</span>
                                                </div>
                                            </td>
                                            <td>13612382</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=56601">adidas</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                2,098&nbsp;руб. <br>

                                                <span class="old-price-percent">(-47%)</span>
                                                <span class="old-price">3,999&nbsp;руб.</span>

                                            </td>
                                            <td>7948</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>4904</td>
                                            <td>9,974,736&nbsp;руб.</td>
                                            <td><span id="sparkline5282047"><canvas width="24" height="40" style="display: inline-block; width: 24px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13612382" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="12754735" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="17444191">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/17440000/17444191-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=12754735">Пакет- майка ПНД 40 (+18)х65, 20мкм</a><br>

                                                <a class="category-link" href="/client/category?id=11366">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Дом / Хозяйственные товары / Бумага и пластик
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(722)</span>
                                                </div>
                                            </td>
                                            <td>17444191</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=75647">wildberries</a>
                                            </td>
                                            <td>0</td>
                                            <td>
                                                1,265&nbsp;руб. <br>

                                            </td>
                                            <td>1</td>
                                            <td>1,359,242.5&nbsp;руб.</td>
                                            <td>6447</td>
                                            <td>8,155,455&nbsp;руб.</td>
                                            <td><span id="sparkline12754735"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="17444191" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="10012705" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13615126">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13610000/13615126-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=10012705">Смартфон Redmi 9A 2/32Gb: 6.53" 1600x720/IPS Helio G25 2Gb/32Gb 13Mp/5Mp 5000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5635">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Xiaomi
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(2703)</span>
                                                </div>
                                            </td>
                                            <td>13615126</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>1080</td>
                                            <td>
                                                7,120&nbsp;руб. <br>

                                                <span class="old-price-percent">(-20%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>16</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>1093</td>
                                            <td>8,124,625&nbsp;руб.</td>
                                            <td><span id="sparkline10012705"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13615126" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="9660339" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="10054253">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/10050000/10054253-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9660339">Смартфон Nova 5T: 6.26" 2340x1080/LTPS Kirin 980 6Gb/128Gb 48+16+2+2Mp/32Mp 3750mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5630">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Android
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(290)</span>
                                                </div>
                                            </td>
                                            <td>10054253</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89506">Huawei</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                27,990&nbsp;руб. <br>

                                            </td>
                                            <td>934</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>289</td>
                                            <td>8,089,110&nbsp;руб.</td>
                                            <td><span id="sparkline9660339"><canvas width="4" height="40" style="display: inline-block; width: 4px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="10054253" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="6496191" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="14044011">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/14040000/14044011-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6496191">Смартфон Galaxy A51 128Gb: 6.5" 2400x1080/sAMOLED Exynos 9611 6Gb/128Gb 48+12+5+5Mp/32Mp 4000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5630">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Android
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(985)</span>
                                                </div>
                                            </td>
                                            <td>14044011</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                24,990&nbsp;руб. <br>

                                            </td>
                                            <td>980</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>297</td>
                                            <td>7,422,030&nbsp;руб.</td>
                                            <td><span id="sparkline6496191"><canvas width="4" height="40" style="display: inline-block; width: 4px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="14044011" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr>


                                            <td>
                                                <a href="" data-id="23172574" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="17189740">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/17180000/17189740-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=23172574">Умная колонка Яндекс.Станция Макс</a><br>

                                                <a class="category-link" href="/client/category?id=5474">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Ноутбуки и компьютеры / Периферийные аксессуары
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(413)</span>
                                                </div>
                                            </td>
                                            <td>17189740</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=81029">Yandex</a>
                                            </td>
                                            <td>909</td>
                                            <td>
                                                21,990&nbsp;руб. <br>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>334</td>
                                            <td>7,344,660&nbsp;руб.</td>
                                            <td><span id="sparkline23172574"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="17189740" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="6496144" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="14044012">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/14040000/14044012-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6496144">Смартфон Galaxy A51 64Gb: 6.5" 2400x1080/sAMOLED Exynos 9611 4Gb/64Gb 48+12+5+5Mp/32Mp 4000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5630">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Android
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1381)</span>
                                                </div>
                                            </td>
                                            <td>14044012</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                21,490&nbsp;руб. <br>

                                            </td>
                                            <td>1029</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>330</td>
                                            <td>7,091,700&nbsp;руб.</td>
                                            <td><span id="sparkline6496144"><canvas width="4" height="40" style="display: inline-block; width: 4px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="14044012" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="9660372" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="13615127">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/13610000/13615127-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9660372">Смартфон Redmi 9A 2/32Gb: 6.53" 1600x720/IPS Helio G25 2Gb/32Gb 13Mp/5Mp 5000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5635">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Xiaomi
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(2612)</span>
                                                </div>
                                            </td>
                                            <td>13615127</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>0</td>
                                            <td>
                                                7,515&nbsp;руб. <br>

                                                <span class="old-price-percent">(-16%)</span>
                                                <span class="old-price">8,990&nbsp;руб.</span>

                                            </td>
                                            <td>12</td>
                                            <td>2,735,460&nbsp;руб.</td>
                                            <td>910</td>
                                            <td>6,838,650&nbsp;руб.</td>
                                            <td><span id="sparkline9660372"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="13615127" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="9660357" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="12423144">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/12420000/12423144-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9660357">Смартфон Galaxy M31 128Gb: 6.4" 2340x1080/Super AMOLED Exynos 9166 6Gb/128Gb 64+8+5Mp/32Mp 6000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5634">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Samsung
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(799)</span>
                                                </div>
                                            </td>
                                            <td>12423144</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>0</td>
                                            <td>
                                                21,490&nbsp;руб. <br>

                                            </td>
                                            <td>1030</td>
                                            <td>6,511,470&nbsp;руб.</td>
                                            <td>303</td>
                                            <td>6,511,470&nbsp;руб.</td>
                                            <td><span id="sparkline9660357"><canvas width="9" height="40" style="display: inline-block; width: 9px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="12423144" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="7772765" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="16020241">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/16020000/16020241-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=7772765">Пакет металлоцен 40х50 (+3) 45мкм с вырубной укрепленной ручкой (INTL)</a><br>

                                                <a class="category-link" href="/client/category?id=11366">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Дом / Хозяйственные товары / Бумага и пластик
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 80%;"></div>
                                                    </div>
                                                    <span class="comments-count">(179)</span>
                                                </div>
                                            </td>
                                            <td>16020241</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=75647">wildberries</a>
                                            </td>
                                            <td>12274</td>
                                            <td>
                                                2,970&nbsp;руб. <br>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>2171</td>
                                            <td>6,447,870&nbsp;руб.</td>
                                            <td><span id="sparkline7772765"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="16020241" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="6895596" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="2147724">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/2140000/2147724-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6895596">Сменные Кассеты Fusion5 Для Мужской Бритвы, 4 шт., с 5 лезвиями, с точным триммером для контуринга</a><br>

                                                <a class="category-link" href="/client/category?id=7950">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Красота / Аптечная косметика
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(3995)</span>
                                                </div>
                                            </td>
                                            <td>2147724</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=92651">GILLETTE</a>
                                            </td>
                                            <td>0</td>
                                            <td>
                                                1,264&nbsp;руб. <br>

                                            </td>
                                            <td>null</td>
                                            <td>1,054,597.33&nbsp;руб.</td>
                                            <td>5006</td>
                                            <td>6,327,584&nbsp;руб.</td>
                                            <td><span id="sparkline6895596"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="2147724" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="6546124" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="9434636">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/9430000/9434636-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6546124">Смарт-часы Galaxy Watch Active2 Алюминий 40 мм</a><br>

                                                <a class="category-link" href="/client/category?id=5651">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смарт-часы и браслеты
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(969)</span>
                                                </div>
                                            </td>
                                            <td>9434636</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>1</td>
                                            <td>
                                                19,990&nbsp;руб. <br>

                                            </td>
                                            <td>2485</td>
                                            <td>12,233,880&nbsp;руб.</td>
                                            <td>306</td>
                                            <td>6,116,940&nbsp;руб.</td>
                                            <td><span id="sparkline6546124"><canvas width="29" height="40" style="display: inline-block; width: 29px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="9434636" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="24334916" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="21264435">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/21260000/21264435-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=24334916">Смартфон Galaxy A32 128Gb: 6.4" 2400x1080/sAMOLED MT G80 4Gb/128Gb 64+8+5+5Mp/20Mp 5000mAh</a><br>

                                                <a class="category-link" href="/client/category?id=5634">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны / Смартфоны Samsung
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(1127)</span>
                                                </div>
                                            </td>
                                            <td>21264435</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89468">Samsung</a>
                                            </td>
                                            <td>354</td>
                                            <td>
                                                19,791&nbsp;руб. <br>

                                                <span class="old-price-percent">(-10%)</span>
                                                <span class="old-price">21,990&nbsp;руб.</span>

                                            </td>
                                            <td>4</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>283</td>
                                            <td>5,845,794&nbsp;руб.</td>
                                            <td><span id="sparkline24334916"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="21264435" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="9495256" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="7961412">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/7960000/7961412-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=9495256">Миксер планетарный SPM7167 1600Вт</a><br>

                                                <a class="category-link" href="/client/category?id=6344">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Бытовая техника / Техника для кухни / Измельчение и смешивание
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(2535)</span>
                                                </div>
                                            </td>
                                            <td>7961412</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=92587">StarWind</a>
                                            </td>
                                            <td>6418</td>
                                            <td>
                                                6,577&nbsp;руб. <br>

                                                <span class="old-price-percent">(-43%)</span>
                                                <span class="old-price">11,540&nbsp;руб.</span>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>877</td>
                                            <td>5,817,463&nbsp;руб.</td>
                                            <td><span id="sparkline9495256"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="7961412" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="6569331" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="9181767">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/9180000/9181767-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=6569331">Весы Mi Smart Scale 2</a><br>

                                                <a class="category-link" href="/client/category?id=6085">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Бытовая техника / Красота и здоровье / Весы напольные
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(8233)</span>
                                                </div>
                                            </td>
                                            <td>9181767</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>14425</td>
                                            <td>
                                                1,049&nbsp;руб. <br>

                                                <span class="old-price-percent">(-34%)</span>
                                                <span class="old-price">1,590&nbsp;руб.</span>

                                            </td>
                                            <td>1</td>
                                            <td>0&nbsp;руб.</td>
                                            <td>5343</td>
                                            <td>5,717,847&nbsp;руб.</td>
                                            <td><span id="sparkline6569331"><canvas width="34" height="40" style="display: inline-block; width: 34px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="9181767" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>

                                        <tr style="-webkit-filter: blur(4px);">


                                            <td>
                                                <a href="" data-id="68593785" class="get-positions"><i class="fa fa-key"></i></a>
                                                <input data-mp="wildberries" style="margin-top: 10px;" type="checkbox" value="48077005">
                                            </td>
                                            <td><img style="width: 30px;" src="https://images.wbstatic.net/c246x328/new/48070000/48077005-1.jpg"></td>
                                            <td>
                                                <a href="/client/product?id=68593785">Смартфон Xiaomi 11 Lite 5G NE 8 Гб/128 Гб: 6.55" 1080 x 2400 / Snapdragon 778G 8/128 Гб 4250 мАч</a><br>

                                                <a class="category-link" href="/client/category?id=5629">
                                                    <img style="width: 12px;margin-top: -2px;" src="/images/wildberries.ico">
                                                    Электроника / Смартфоны и телефоны / Смартфоны
                                                </a>

                                                <div>
                                                    <div class="marks-wrapper">
                                                        <div style="width: 100%;"></div>
                                                    </div>
                                                    <span class="comments-count">(168)</span>
                                                </div>
                                            </td>
                                            <td>48077005</td>
                                            <td>
                                                <img style="width: 12px;margin-top: -4px;" src="/images/wildberries.ico">
                                                <a href="/client/brand?id=89493">Xiaomi</a>
                                            </td>
                                            <td>0</td>
                                            <td>
                                                23,600&nbsp;руб. <br>

                                                <span class="old-price-percent">(-32%)</span>
                                                <span class="old-price">34,990&nbsp;руб.</span>

                                            </td>
                                            <td>29</td>
                                            <td>21,523,200&nbsp;руб.</td>
                                            <td>228</td>
                                            <td>5,380,800&nbsp;руб.</td>
                                            <td><span id="sparkline68593785"><canvas width="24" height="40" style="display: inline-block; width: 24px; height: 40px; vertical-align: top;"></canvas></span></td>
                                            <td><button data-id="48077005" data-mp="wildberries" type="product" class="add-to-my-list" title="Отслеживать"><i class="fa fa-plus-square-o"></i></button></td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <a style="display: none;" class="btn btn-primary btn-rounded btn-block" id="show-more" href="#" next-page="2">Показать еще (656982)</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #modal .modal-dialog {
            max-width: 900px;
        }
    </style>
    <script src="<?=PROJECT_URL?>client/js/pages/seller.js?id=9"></script></div>