<?php if(empty($javascriptActivatedSwitches)) {die();} header('Content-Type: application/javascript');  
?>

RouterController.loadHandler(true);

var RatesController = {
    init: function() {

        if (!settings.allGetParams().pay) {

            //HintsController.modal();
        }

        this.eventListener();

        //HintsController.extraDays();

        if (settings.allGetParams().paid) {

            settings.responseHandler(true, 'Оплата прошла успешно, в скором времени мы свяжемся с вами!');

            gtag('event', 'send_form', {'event_category': 'finish_payment_podbor'}); ym(6994312611111111111111111111111111111111, 'reachGoal', 'finish_payment_podbor_send_form');
        }
    },
    eventListener: function() {

        $('#content button').click(function() {

            if ($(this).attr('data-id')) {

                RatesController.createTransaction($(this).attr('data-id'), $(this).attr('data-total-days'));
            }
            else {

                RatesController.modal();
            }
        });

       // if (CommonController.user.id_ref == 78678 && !CommonController.user.got_extra_bonus) {

            $('button[data-id="1"]').each(function() {

                $(this).parent().find('span').eq(0).text('1 000 руб.');
            });
      //  }

        $('.rate-periods .rate-days').click(function() {

            $('.rate-periods .rate-days').removeClass('active');

            $(this).addClass('active');

            /*var days = $(this).attr('data-days'),
                schema = {
                    30: {
                        3: {
                            price: '3 990',
                            discount: 'в месяц',
                        },
                        2: {
                            price: '13 990',
                            discount: 'в месяц',
                        },
                        1: {
                            price: '19 990',
                            discount: ' в месяц',
                        },
                        8: {
                            price: '89 990',
                            discount: ' в месяц',
                        }
                    },
                    92: {
                        3: {
                            price: '10 175',
                            discount: '11 970 руб.',
                        },
                        2: {
                            price: '35 675',
                            discount: '41 970 руб.',
                        },
                        1: {
                            price: '50 975',
                            discount: '59 970 руб.',
                        },
                        8: {
                            price: '229 245',
                            discount: '269 970 руб.',
                        }
                    },
                    183: {
                        3: {
                            price: '16 758',
                            discount: '23 940 руб.',
                        },
                        2: {
                            price: '58 758',
                            discount: '83 940 руб.',
                        },
                        1: {
                            price: '83 958',
                            discount: '119 940 руб.',
                        },
                        8: {
                            price: '377 580',
                            discount: '539 940 руб.',
                        }
                    },
                    365: {
                        3: {
                            price: '23 940',
                            discount: '47 880 руб.',
                        },
                        2: {
                            price: '83 940',
                            discount: '167 880 руб.',
                        },
                        1: {
                            price: '119 940',
                            discount: '239 880 руб.',
                        },
                        8: {
                            price: '539 940',
                            discount: '1 079 880 руб.',
                        }
                    },
                };

                for (var i in schema[days]) {

                    $(`button[data-id="${i}"]`).each(function() {

                        var wrapper = $(this).parent().find('.price-item-cost');

                        wrapper.find('span').eq(0).text(`${schema[days][i].price} руб.`);
                        wrapper.find('span').eq(1).text(schema[days][i].discount);

                        if (days != 30) {

                            wrapper.find('span').eq(1).addClass('trough-lign');

                            $(this).attr('data-total-days', days);
                        }
                        else {

                            wrapper.find('span').eq(1).removeClass('trough-lign');

                            $(this).removeAttr('data-total-days');
                        }
                    });
                }*/
            var days = $(this).attr('data-days'),
                schema = {
                    3: {
                        text: '3 990',
                        price: 3990,
                    },
                    2: {
                        text: '13 990',
                        price: 13990,
                    },
                    1: {
                        text: '19 990',
                        price: 19990,
                    },
                    8: {
                        text: '89 990',
                        price: 89990,
                    }
                },
                multiplier = 1;

            switch (days) {
                case '30':

                    multiplier = 1;
                    break;
                case '92':

                    multiplier = 3;
                    break;
                case '183':

                    multiplier = 6;
                    break;
                case '365':

                    multiplier = 12;
                    break;
            }

            for (var i in schema) {

                $(`button[data-id="${i}"]`).each(function() {

                    var wrapper = $(this).parent().find('.price-item-cost');

                    wrapper.find('span').eq(0).text(`${settings.intToMoney(schema[i].price * multiplier).replace(/,/gi, ' ')} руб.`);
                    wrapper.find('span').eq(1).text(`за ${multiplier} ${settings.declOfNum(multiplier, ['месяц', 'месяца', 'месяцев'])}`);

                    if (days != 30) {

                        $(this).attr('data-total-days', days);
                    }
                    else {

                        $(this).removeAttr('data-total-days');
                    }
                });
            }


        });
    },
    createTransaction: function(id, days) {

        switch (parseInt(id)) {
            case 3:
                console.log('s');
                gtag('event', 'click_button', {'event_category': 'select_tarif', 'event_label': 'lk_standart'}); ym(69943126111, 'reachGoal', 'select_tarif_click_button_lk_standart');
                break;
            case 2:
                gtag('event', 'click_button', {'event_category': 'select_tarif', 'event_label': 'lk_gold'}); ym(69943126111, 'reachGoal', 'select_tarif_click_button_lk_gold');
                break;
            case 1:
                gtag('event', 'click_button', {'event_category': 'select_tarif', 'event_label': 'lk_premium'}); ym(6994312611, 'reachGoal', 'select_tarif_click_button_lk_premium');
                break;
            case 7:
                gtag('event', 'click_button', {'event_category': 'select_tarif', 'event_label': 'lk_all_inclusive'}); ym(69943126111, 'reachGoal', 'select_tarif_click_button_lk_all_inclusive');
                break;
        }

        var data = {
            id_rate: id,
            days: days
        }

        settings.ajax({
            method: 'POST',
            url: `/v1/transaction`,
            data: data,
            callback: function(result) {

                setTimeout(function () {

                    location.replace(`/client/payment?id=${result.id}`);
                }, 200);
            },
            error: function() {

            },
            headers: function() {

            }
        }, true);
    },
    modal: function() {

        var modal = $('#modal');

        $('.modal-title', modal).text('Тариф "Все включено"');
        $('.modal-footer', modal).html('');
        $('.modal-body', modal).html(`
            <table class="table">
                <tr>
                    <td>Создание личного кабинета поставщика, загрузка документов</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Рекомендации по количеству к первой поставке</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Расчёт розничной цены</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <!--
                <tr>
                    <td>Создание карточек товаров</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                -->
                <tr>
                    <td>Вывод до 5 товарных карточек</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Обработка фото под требования МП</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Загрузка рекомендаций к карточке</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Загрузка логотипа бренда</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Генерация штрих-кодов</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Формирование поставки на портале МП</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Формирование задания по упаковке для склада</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Подготовка документов, заказ пропуска</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Поставка на склад МП</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Рекомендации по бухгалтерскому оформлению продаж</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Месяц сопровождения продаж</td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td>Месяц пользования тарифом <strong>"Gold"</strong></td>
                    <td><div class="price-item-info-check"></div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong>Цена:</strong> 31.490 руб/мес (За стоимость одного сотрудника вы получаете команду профессионалов и лучший сервис аналитики на рынке) <br />
                        <strong>Важно:</strong> купоны на данный тариф не распространяются
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <button class="btn btn-primary" data-id="7">Оплатить</button>
                    </td>
                </tr>
            </table>
        `);

        modal.modal('show');

        $('.btn-primary', modal).click(function() {

            RatesController.createTransaction($(this).attr('data-id'));
        });
    }
}

$(function() {

/*    if (CommonController.user.id > 9391 && CommonController.user.id < 9500) {

        $('#rate-3').text('1 000');
        $('#rate-2').text('2 000');
        $('#rate-1').text('3 000');
    }*/

    RatesController.init();
});