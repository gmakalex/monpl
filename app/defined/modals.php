
<!-- modal -->

<div class="modal inmodal" id="modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="save-data">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<!-- modal special -->

<div class="modal inmodal" id="modal-special" data-keyboard="false" data-backdrop="static" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h4 class="modal-title">Запишитесь на экскурсию</h4>
            </div>
            <div class="modal-body">
                <p class="text-center">Вы почти у цели! Для завершения регистрации в сервисе, запишитесь на онлайн-экскурсию по сервису, которая идет не более 30 минут </p>
                <table style="width: 100%;">
                    <tr style="display: none;">
                        <td><strong>Выберите удобную дату:</strong></td>
                        <td><input type="text" style="display: inline-block;width: auto;" id="schedule-date" name="date" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Выберите любое удобное время на <span id="schedule-late">сегодня</span>: </td>
                    </tr>
                    <tr>
                        <td><strong>Время по Москве:</strong></td>
                        <td style="padding-top: 15px;">
                            <label for="date-9">09:00 <input type="radio" id="date-9" value="9" name="time"></label> <br />
                            <label for="date-11">11:00 <input type="radio" id="date-11" value="11" name="time"></label> <br />
                            <label for="date-13">13:00 <input type="radio" id="date-13" value="13" name="time"></label> <br />
                            <label for="date-17">17:00 <input type="radio" id="date-17" value="17" name="time"></label> <br />
                            <label for="date-21">21:00 <input type="radio" id="date-21" value="21" name="time"></label> <br />
                        </td>
                    </tr>
                </table>
                <p class="text-center">Бесплатный доступ на 2 дня к полному функционалу сервиса откроется только после прохождения экскурсии.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-schedule">Записаться</button>
            </div>
        </div>
    </div>
</div>

<!-- modal after schedule -->

<div class="modal inmodal" id="modal-after-schedule" data-keyboard="false" data-backdrop="static" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <h4 class="modal-title">Завершение записи</h4>
            </div>
            <div class="modal-body">
                <p>Выберите мессенджер, куда вам прислать ссылку на экскурсию:</p>
                <script src="<?=PROJECT_URL?>@textback/notification-widget@latest/build/index.js"></script>
                <tb-notification-widget id="widget_id" api-path="https://api.textback.io/api" widget-id="128112ad-9141-367f-8f5a-0177dd47fc06">
                </tb-notification-widget>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- modal about video -->

<div class="modal inmodal" id="modal-about-viedo" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добро пожаловать в Moneyplace!</h4>
            </div>
            <div class="modal-body">
                <p>Мы уже открыли вам 1 день доступа к тарифу Demo Premium после регистрации и подтверждения номера.</p>
                <p>В демо режиме вам доступны по 3 поиска в каждом из разделов и по 3 применения фильтра ежедневно. Данное ограничение снимается после оплаты.</p>
                <p>Чтобы получить еще +6 дней бесплатного доступа, переходите в раздел <a href="<?=PROJECT_URL?>client/bonus">Бонусы</a>.</p>
                <p>Заполните короткую анкету +1 день доступа <br /> Посмотрите ВСЕ обучающие видео +5 дней доступа</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="modal-demo-request" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">На сегодня у вас есть еще 1 пробный запрос в данной категории!</h4>
            </div>
            <div class="modal-body">
                <p>Получите неограниченный доступ к самой большой базе данных
                    продуктов на маркетплейсах (> 100 млн. товаров) для поиска самых
                    прибыльных направлений и потенциальных трендов!
                </p>
                <p>Пользуйтесь уникальными инструментами, чтобы ежедневно находить десятки лучших товаров для торговли и увеличивать продажи существующих.</p>
                <p><a href="<?=PROJECT_URL?>client/rates" style="width: 100%" class="btn btn-primary" target="_blank">Получить полный доступ к Moneyplace</a></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal inmodal" id="modal-promo" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Скидка</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Standart</th>
                            <th>Gold</th>
                            <th>Premium</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>3 месяца</td>
                            <td>
                                <strong>10 174 руб.</strong> <br>
                                <span class="old-price-percent">(-15%)</span>
                                <span class="old-price">11 970&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>35 675 руб.</strong> <br>
                                <span class="old-price-percent">(-15%)</span>
                                <span class="old-price">41 970&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>50 974 руб.</strong> <br>
                                <span class="old-price-percent">(-15%)</span>
                                <span class="old-price">59 970&nbsp;руб.</span>
                            </td>
                        </tr>
                        <tr>
                            <td>6 месяцев</td>
                            <td>
                                <strong>16 758 руб.</strong> <br>
                                <span class="old-price-percent">(-30%)</span>
                                <span class="old-price">23 940&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>58 750 руб.</strong> <br>
                                <span class="old-price-percent">(-30%)</span>
                                <span class="old-price">83 940&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>83 960 руб.</strong> <br>
                                <span class="old-price-percent">(-30%)</span>
                                <span class="old-price">119 940&nbsp;руб.</span>
                            </td>
                        </tr>
                        <tr>
                            <td>12 месяцев</td>
                            <td>
                                <strong>23 940 руб.</strong> <br>
                                <span class="old-price-percent">(-50%)</span>
                                <span class="old-price">47 800&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>83 940 руб.</strong> <br>
                                <span class="old-price-percent">(-50%)</span>
                                <span class="old-price">167 880&nbsp;руб.</span>
                            </td>
                            <td>
                                <strong>119 940 руб.</strong> <br>
                                <span class="old-price-percent">(-50%)</span>
                                <span class="old-price">239 880&nbsp;руб.</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="apply-for-promo">Оставить заявку на скидку</button>
            </div>
        </div>
    </div>
</div>

<style>
    #modal-promo > .modal-dialog {
        max-width: 700px;
    }
    #show-compare {
        position: fixed;
        bottom: 0;
        left: 0;
        background: #fff;
        width: 100%;
        z-index: 9999;
        height: 50px;
        border-top: 1px solid #e7eaec;
    }
</style>


<div id="show-compare" style="display: none">
    <button style="margin: 8px 0 0 20px;" id="compare" class="btn btn-primary">Сравнить</button>
    <button style="margin: 8px 0 0 20px;" id="compare-seo" class="btn btn-warning">Сравнить SEO</button>
</div>

<div style="" id="custom-video" class="popup-wnd">
    <div class="popup-content popup-video" style="top: 10%;left: 10%;-webkit-transform: translate(-50%,-50%);-moz-transform: translate(-50%,-50%);transform: translate(-10%,-10%);">
        <h2 style="margin-bottom: 40px;font-weight: bold;">Изучите урок полностью, чтобы вам добавились бонусные дни!</h2>
        <div class="video-wnd">
            <div class="video">

            </div>
        </div>
    </div>
    <div class="close-btn js-close"></div>
</div>
