
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a href="<?=PROJECT_URL?>client/dashboard">
                            <img src="<?=PROJECT_URL?>images/1-3.png" style="width: 150px;" />
                        </a>
                        <span data-toggle="dropdown" class="dropdown-toggle">
                            <a class="block m-t-xs font-bold" id="name" href="<?=PROJECT_URL?>client/user" style="color: #fff;">Игрок</a>
                            <a class="text-muted text-xs block" href="/client/rates" id="rate">Тариф: <span style="color: #fff;">Premium</span></a>
                            <a class="text-muted text-xs block" href="/client/rates" id="expires">До: <span style="color: #fff;">24.02.2022 в 18:41</span></a>
                        </span>
                    </div>
                    <div class="logo-element">
                        MP
                    </div>
                </li>

                <li <?=$aLink['bonus']?>>
                    <a href="/client/bonus" class="bonus-menu-item"><i class="fa fa-graduation-cap"></i><span class="nav-label">Бонусы</span></a>
                </li>

                <li <?=$aLink['dashboard']?>>
                    <a href="/client/dashboard"><i class="fa fa-th-large"></i><span class="nav-label">Главная</span></a>
                </li>

                <li <?=$aLink['products']?>>
                    <a href="/client/products"><i class="fa fa-diamond"></i><span class="nav-label">Товары</span></a>
                </li>

                <li <?=$aLink['categories']?>>
                    <a href="/client/categories"><i class="fa fa-sitemap"></i><span class="nav-label">Категории</span></a>
                </li>

                <li <?=$aLink['sellers']?>>
                    <a href="/client/sellers"><i class="fa fa-users"></i><span class="nav-label">Продавцы</span></a>
                </li>

                <li <?=$aLink['brands']?>>
                    <a href="/client/brands"><i class="fa fa-bar-chart-o"></i><span class="nav-label">Бренды</span></a>
                </li>

                <li <?=$aLink['keywords']?>>
                    <a href="/client/keywords"><i class="fa fa-key"></i><span class="nav-label">Ключевики</span><span class="label label-info float-right">NEW</span></a>
                </li>

                <li <?=$aLink['analytic']?>>
                    <a href="/client/analytic"><i class="fa fa-line-chart"></i><span class="nav-label">Анализ ниш</span></a>
                </li>

                <!--<li>
                    <a href="/client/market"><i class="fa fa-pie-chart"></i><span class="nav-label">Анализ рынка</span></a>
                </li>-->

                <li style="margin-top: 60px;" <?=$aLink['lists']?>>
                    <a href="/client/lists"><i class="fa fa-list"></i><span class="nav-label">Отслеживание</span></a>
                </li>

                <li <?=$aLink['faq']?>>
                    <a href="/client/faq"><i class="fa fa-question-circle"></i><span class="nav-label">Обучение</span></a>
                </li>

            </ul>
        </div>
    </nav>
