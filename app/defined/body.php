<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <div id="mobile-mesage">На данный момент наш сервис не адаптирован под мобильные устройства.</div>
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">

                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="<?=PROJECT_URL?>#"><i class="fa fa-bars"></i> </a>

                <form id="search-form" class="input-group m-b">
                    <div class="input-group-prepend">
                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button" aria-expanded="false">
                            <img style="width: 14px;margin-top: -3px;" src="<?=PROJECT_URL?>images/wildberries.ico">
                            <img style="width: 14px;margin-top: -3px;" src="<?=PROJECT_URL?>images/ozon.ico">
                            <img style="width: 14px;margin-top: -3px;" src="<?=PROJECT_URL?>images/ali.ico">
                            <strong>Поиск по:</strong> <span id="search-helper">товарам</span>
                        </button>
                        <ul class="dropdown-menu" id="search-selector" x-placement="bottom-start" style="position: absolute;width: 190px; top: 35px; left: 0px; will-change: top, left;">
                            <li class="search-selector" data-link="product" data-search-url="products" data-search-param="q[name][like]">Товарам (название)</li>
                            <li class="search-selector" data-link="product" data-search-url="products" data-search-param="q[sku][equal]">Товарам (артикул)</li>
                            <li class="search-selector" data-link="product-type" data-search-url="product-type" data-search-param="q[name][like]">Товарам (тип)</li>
                            <li class="search-selector" data-link="category" data-search-url="categories" data-search-param="q[name][like]">Категориям</li>
                            <li class="search-selector" data-link="seller" data-search-url="sellers" data-search-param="q[name][like]">Продавцам</li>
                            <li class="search-selector" data-link="brand" data-search-url="brands" data-search-param="q[name][like]">Брендам</li>
                            <li class="search-selector" data-link="keyword" data-search-url="keywords" data-search-param="q[name][like]">Ключевым словам</li>
                        </ul>
                    </div>
                    <input type="text" placeholder="Поиск, минимум 3 символа" class="form-control" name="top-search" id="top-search">

                    <div class="information-tooltip main-search" data-toggle="tooltip" data-placement="bottom" data-original-title='Порядок слов неважен при поиске. Например, если товар называется "Силиконовый чехол-накладка для Samsung Galaxy S10", то мы его найдем даже по запросу "для samsung чехол s10"'>?</div>
                </form>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a id="logout" href="<?=PROJECT_URL?>login.html">
                        <i class="fa fa-sign-out"></i> Выход
                    </a>
                </li>
            </ul>

        </nav>
    </div>



    <!--
        <div id="get-course" style="">
            <div class="alert alert-warning">
                Реалити-шоу «Битва селлеров» уже на канале! Не пропустите первую серию <a href="<?=PROJECT_URL?>https://youtu.be/vgGICiwUuZg" target="_blank">Смотреть</a>
            </div>
        </div>

        <div id="watch-about" style="display: none;">
            <div class="alert alert-info">
                <p>
                    Узнайте, как умножить свои продажи при помощи Moneyplace! <a href="<?=PROJECT_URL?>https://moneyplace.getcourse.ru/mp_reg" target="_blank">Посмотрите запись экскурсии</a>
                </p>
            </div>
        </div>
        <div id="get-discount" style="display: none;">
            <div class="alert alert-warning">
                <p>
                    Ваша скидка на 15-30-50% при покупке на 3-6-12 мес сгорит через: <strong>13:02:23</strong> &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-primary btn-xs">Оставить заявку</button>
                </p>
            </div>
        </div>
        -->

    <div class="demo-warning-banner" style="display: none;">
        <div class="alert alert-danger">
            <p><strong>Внимание!</strong> У Вас закончился оплаченный период. Для работы сервиса необходимо оплатить тариф.
                Если вы только зарегистрировались - просмотрите обучающие уроки для получения демо-доступа.
            </p>
            <!--
                <p>
                    <strong>Внимание!</strong> Вы находитесь в демо-режиме, который создан в ознакомительных целях с нашим сервисом.
                    Абсолютно все аналитические данные
                    о товарах, категориях, брендах и продавцах - <strong>случайны</strong>. Также стоить отметить, что количество товаров в данном режиме
                    составляет <strong>~1%</strong> от истинного, но функция поиска над данным сообщением - содержит полностью актуальные данные, чтобы вы смогли убедиться
                    в наличии последних. Соответсвенно, <strong>найденные товары во всплывающем окне могут не вывестись в аналитических результутах</strong>.
                </p>
                <p>
                    Для наглядной разницы между функционалом тарифов - вы можете активировать любой из них.
                    <strong>Попробовать: </strong>
                    <button type="button" data-id="6" class="btn btn-primary btn-xs">Standart</button>
                    <button type="button" data-id="5" class="btn btn-primary btn-xs">Gold</button>
                    <button type="button" data-id="4" class="btn btn-primary btn-xs">Premium</button>
                </p>
                На странице <strong><a href="<?=PROJECT_URL?>client/rates">тарифы</a></strong> вы можете произвести оплату онлайн и в течение 5 минут
                получить полностью рабочий функционал в соответсвии с выбранным тарифом!
                -->
        </div>
    </div>
    <div id="page-content" style="display: none;">


