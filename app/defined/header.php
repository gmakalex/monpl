<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1440, initial-scale=1.0">
    <title><?=PROJECT_NAME?> | Главная</title>

    <link href="<?=PROJECT_URL?>css/libs/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>css/libs/font-awesome/font-awesome.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>js/libs/jquery/jquery-ui.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>js/libs/select2/select2.min.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>css/libs/animate/animate.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>js/libs/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>js/libs/intro/introjs.min.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>js/libs/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>css/style.css" rel="stylesheet">
    <link href="<?=PROJECT_URL?>client/css/client.css" rel="stylesheet">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <link rel="icon" type="image/png" href="<?=PROJECT_URL?>favicon.png">
</head>
<body>