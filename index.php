<?php
$thisDir = __DIR__;
if(empty($requestUri)){
    $requestUri = $_SERVER["REQUEST_URI"];
}

$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
define("CURRENT_URL", $actualLink);

include $thisDir . '/app/data/configurations.php';

$ajaxPosition = 1;
include $thisDir . '/app/engine/functions.php';
include $thisDir . '/app/engine/specials.php';




/**/
if (str_contains($requestUri, "/register.html")){
    if(file_exists($thisDir . '/app/authorization/register.php')) {
        include $thisDir . '/app/authorization/register.php';
        exit;
    }
}elseif(str_contains($requestUri, "/login.html")){
    if(file_exists($thisDir . '/app/authorization/login.php')) {
        include $thisDir . '/app/authorization/login.php';
        exit;
    }
}


include $thisDir . '/app/authorization/login.php';
die();






if(UNDER_RECONSTRUCTION) {
    if (!in_array($_SERVER['REMOTE_ADDR'], UNDER_RECONSTRUCTION, true)) {
        die(UNDER_RECONSTRUCTION_TEXT);
    }
}


include $thisDir . '/app/defined/header.php';
echo '<div id="wrapper">';
echo "<script> document.title = ' ".PROJECT_NAME." | ".$nameTitle."'; </script>";
include $thisDir . '/app/defined/menu.php';
include $thisDir . '/app/defined/body.php';
echo '</div>';
include $thisDir . '/app/defined/modals.php';


//CONTENT DATA
if (str_contains($requestUri, "client/".$nameUrl)){
    if(file_exists($thisDir . '/app/pages/'.$nameUrl.'.php')) {
        include $thisDir . '/app/pages/' . $nameUrl . '.php';
    }else{
        echo '';
    }
}else{
    include $thisDir . '/app/main.php';
}

include $thisDir . '/app/defined/footer.php';
 
?>